/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const AUTENTICACAO_CONTROLLER = require('../controllers/autenticacao')

/* Methods */

router.get('/', AUTENTICACAO_CONTROLLER.renderEntrar)
router.get('/entrar', AUTENTICACAO_CONTROLLER.renderEntrar)
router.get('/registrar', AUTENTICACAO_CONTROLLER.renderRegistrar)
router.get('/nova-senha/:id_empresa/:id_usuario/:token', AUTENTICACAO_CONTROLLER.renderNovaSenha)

module.exports = router
