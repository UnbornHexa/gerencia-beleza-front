/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const ERRO_CONTROLLER = require('../controllers/erro')

/* Methods */

router.use('/', ERRO_CONTROLLER.render)

module.exports = router
