/* Requires */

const express = require('express')
const router = express.Router()

/* Controllers */

const BEMVINDO_CONTROLLER = require('../controllers/bem-vindo')

const AGENDA_CONTROLLER = require('../controllers/agenda')
const CLIENTES_CONTROLLER = require('../controllers/clientes')
const COMANDAS_CONTROLLER = require('../controllers/comandas')

const SERVICOS_CONTROLLER = require('../controllers/servicos')
const PRODUTOS_CONTROLLER = require('../controllers/produtos')
const MATERIAIS_CONTROLLER = require('../controllers/materiais')

const FINANCEIRO_CONTROLLER = require('../controllers/financeiro')
const PERMISSOES_CONTROLLER = require('../controllers/permissoes')
const FUNCIONARIOS_CONTROLLER = require('../controllers/funcionarios')

const NOTIFICACOES_CONTROLLER = require('../controllers/notificacoes')
const PERFIL_CONTROLLER = require('../controllers/perfil')
const EMPRESA_CONTROLLER = require('../controllers/empresa')
const AJUDA_CONTROLLER = require('../controllers/ajuda')

/* Methods */

router.get('/bem-vindo', BEMVINDO_CONTROLLER.render)

router.get('/agenda', AGENDA_CONTROLLER.render)
router.get('/clientes', CLIENTES_CONTROLLER.render)
router.get('/comandas', COMANDAS_CONTROLLER.render)

router.get('/servicos', SERVICOS_CONTROLLER.render)
router.get('/produtos', PRODUTOS_CONTROLLER.render)
router.get('/materiais', MATERIAIS_CONTROLLER.render)

router.get('/financeiro', FINANCEIRO_CONTROLLER.renderFinanceiro)
router.get('/financeiro/receitas', FINANCEIRO_CONTROLLER.renderReceitas)
router.get('/financeiro/despesas', FINANCEIRO_CONTROLLER.renderDespesas)
router.get('/financeiro/comissoes', FINANCEIRO_CONTROLLER.renderComissoes)
router.get('/financeiro/gorjetas', FINANCEIRO_CONTROLLER.renderGorjetas)

router.get('/permissoes', PERMISSOES_CONTROLLER.render)
router.get('/funcionarios', FUNCIONARIOS_CONTROLLER.render)

router.get('/notificacoes', NOTIFICACOES_CONTROLLER.render)
router.get('/perfil', PERFIL_CONTROLLER.render)
router.get('/empresa', EMPRESA_CONTROLLER.render)
router.get('/ajuda', AJUDA_CONTROLLER.render)

module.exports = router
