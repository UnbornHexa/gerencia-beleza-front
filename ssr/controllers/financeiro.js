/* Requires */

const file = require('../helpers/read-file-async')
const path = require('path')

/* Internal Variables */

const PATH_HTML = path.join(__dirname, '../../build/html/pages')

/* Methods */

exports.renderFinanceiro = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/financeiro.html`)
  res.status(200).send(html)
}

exports.renderReceitas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/receitas.html`)
  res.status(200).send(html)
}

exports.renderDespesas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/despesas.html`)
  res.status(200).send(html)
}

exports.renderComissoes = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/comissoes.html`)
  res.status(200).send(html)
}

exports.renderGorjetas = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/gorjetas.html`)
  res.status(200).send(html)
}
