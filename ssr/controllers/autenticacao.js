/* Requires */

const file = require('../helpers/read-file-async')
const path = require('path')

/* Internal Variables */

const PATH_HTML = path.join(__dirname, '../../build/html/pages')

/* Methods */

exports.renderEntrar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/entrar.html`)
  res.status(200).send(html)
}

exports.renderRegistrar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/registrar.html`)
  res.status(200).send(html)
}

exports.renderNovaSenha = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/nova-senha.html`)
  res.status(200).send(html)
}
