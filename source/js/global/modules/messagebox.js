const Module = () => {
  const methods = {}

  // Methods

  methods.alert = (_text, _theme) => {
    const messagebox = document.createElement('messagebox')
    messagebox.classList.add('show')

    let customMessage
    if (!_theme || _theme === 'info') customMessage = methods._buildInfoTheme(messagebox, _text)
    if (_theme === 'success') customMessage = methods._buildSuccessTheme(messagebox, _text)
    if (_theme === 'error') customMessage = methods._buildErrorTheme(messagebox, _text)

    document.body.appendChild(customMessage)
    setTimeout(() => {
      try {
        document.body.removeChild(customMessage)
      } catch (e) {}
    }, 5000)
  }

  // Theme

  methods._buildErrorTheme = (_messagebox, _text) => {
    _messagebox.id = 'message__error'
    _messagebox.innerHTML = `
    <div class="icon">
      <i class="icon-times"></i>
    </div>
    <div class="text">
      <p>Ops... Ocorreu um Erro!</p>
      <h2>${_text}</h2>
    </div>
    `
    return _messagebox
  }

  methods._buildSuccessTheme = (_messagebox, _text) => {
    _messagebox.id = 'message__success'
    _messagebox.innerHTML = `
      <div class="icon">
        <i class="icon-check"></i>
      </div>
      <div class="text">
        <p>Deu tudo certo!</p>
        <h2>${_text}</h2>
      </div>
    `
    return _messagebox
  }

  methods._buildInfoTheme = (_messagebox, _text) => {
    _messagebox.id = 'message__info'
    _messagebox.innerHTML = `
      <div class="icon">
        <i class="icon-ban"></i>
      </div>
      <div class="text">
        <p>Espere, algo aconteceu!</p>
        <h2>${_text}</h2>
      </div>
    `
    return _messagebox
  }

  return methods
}

module.exports = Module
