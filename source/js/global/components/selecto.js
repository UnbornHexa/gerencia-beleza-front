/* Requires */

const ACCENT = require('../../global/helpers/accent')

/* Class */

class Selecto {
  constructor (_id) {
    this.__component = document.querySelector(_id)
    this.__state = {
      options: [],
      selected: {},
      clearTimeoutSearch: {},
      limitResults: 30
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () {
    this.__component.innerHTML = `
    <div class="selected"></div>
    <div class="dropdown">
      <div class="dropdown__busca input">
        <input type="text" placeholder="Busque...">
      </div>
      <div class="dropdown__opcoes"></div>
      <div class="alert__empty"></div>
    </div>
    `
  }

  _alertEmpty (_html) {
    if (this.__state.options.length > 0) return
    this.__component.querySelector('.dropdown .dropdown__busca').style.display = 'none'
    this.__component.querySelector('.dropdown .dropdown__opcoes').style.display = 'none'
    this.__component.querySelector('.dropdown .alert__empty').innerHTML = _html
  }

  // Dropdown

  _showDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    dropdownDiv.classList.add('show')
  }

  _hideDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    dropdownDiv.classList.remove('show')
  }

  _toggleDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    if (dropdownDiv.style.display === 'block') {
      this._hideDropdown()
    } else {
      this._showDropdown()
    }
  }

  _resetSearchbar () {
    const searchDiv = this.__component.querySelector('.dropdown .dropdown__busca input')
    searchDiv.value = ''
  }

  // Options

  importOptions (_options) {
    this.__state.options = _options
    this._loadFirstOptions()
  }

  _loadFirstOptions () {
    this._cleanOptions()
    const optionsToShow = this.__state.options.slice(0, this.__state.limitResults)

    optionsToShow.forEach(_option => {
      const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
      const div = `<option value="${_option.value}">${_option.text}</option>`
      optionsDiv.innerHTML += div
    })
  }

  filterOptions (_pesquisa) {
    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    this._cleanOptions()
    let countResults = 0

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      const optionText = ACCENT().remove(currentOption.text.toUpperCase())
      _pesquisa = ACCENT().remove(_pesquisa.toUpperCase())

      if (optionText.includes(_pesquisa)) {
        countResults++
        if (this.__state.limitResults === countResults) break

        const div = `<option value="${currentOption.value}">${currentOption.text}</option>`
        optionsDiv.innerHTML += div
      }
    }
  }

  _cleanOptions () {
    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    optionsDiv.innerHTML = ''
  }

  // Selected

  value () {
    return this.__state.selected.value
  }

  text () {
    return this.__state.selected.text
  }

  isEmpty () {
    if (Object.entries(this.__state.selected).length === 0) return true
    return false
  }

  selectOption (_value) {
    const selectedDiv = this.__component.querySelector('.selected')

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      if (currentOption.value === _value) {
        selectedDiv.innerText = currentOption.text
        this.__state.selected = {
          value: currentOption.value,
          text: currentOption.text
        }
        break
      }
    }
  }

  resetSelected () {
    this.__state.selected = {}
    this.__component.querySelector('.selected').innerHTML = ''
  }

  // Utils

  disable () {
    this.__component.setAttribute('disabled', true)
  }

  // Events

  activeClickOnSelecto () {
    document.addEventListener('click', (e) => {
      if (e.target !== this.__component) return

      this._toggleDropdown()
    })
  }

  activeHiddenOnLostFocus () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__busca input')
      if (e.target === this.__component || e.target === matchElement) return

      this._hideDropdown()
    })
  }

  activeClickOnOption () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__opcoes')
      if (e.target.parentElement !== matchElement) return

      this.selectOption(e.target.value)
      this._resetSearchbar()
      this._loadFirstOptions()
    })
  }

  activeKeyupOnSearchbar () {
    document.addEventListener('keyup', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__busca input')
      if (e.target !== matchElement) return

      clearTimeout(this.__state.clearTimeoutSearch)
      if (e.target.value.length < 1) {
        this._loadFirstOptions()
        return
      }

      this.__state.clearTimeoutSearch = setTimeout(() => {
        const pesquisa = e.target.value
        this.filterOptions(pesquisa)
      }, 500)
    })
  }

  // Preset Options

  setOptionsWithEstados () {
    this.__state.options = [
      { value: 'AC', text: 'Acre' },
      { value: 'AL', text: 'Alagoas' },
      { value: 'AP', text: 'Amapá' },
      { value: 'AM', text: 'Amazonas' },
      { value: 'BA', text: 'Bahia' },
      { value: 'CE', text: 'Ceará' },
      { value: 'DF', text: 'Distrito Federal' },
      { value: 'ES', text: 'Espírito Santo' },
      { value: 'GO', text: 'Goiás' },
      { value: 'MA', text: 'Maranhão' },
      { value: 'MT', text: 'Mato Grosso' },
      { value: 'MS', text: 'Mato Grosso do Sul' },
      { value: 'MG', text: 'Minas Gerais' },
      { value: 'PA', text: 'Pará' },
      { value: 'PB', text: 'Paraíba' },
      { value: 'PR', text: 'Paraná' },
      { value: 'PE', text: 'Pernambuco' },
      { value: 'PI', text: 'Piauí' },
      { value: 'RJ', text: 'Rio de Janeiro' },
      { value: 'RN', text: 'Rio Grande do Norte' },
      { value: 'RS', text: 'Rio Grande do Sul' },
      { value: 'RO', text: 'Rondônia' },
      { value: 'RR', text: 'Roraima' },
      { value: 'SC', text: 'Santa Catarina' },
      { value: 'SP', text: 'São Paulo' },
      { value: 'SE', text: 'Sergipe' },
      { value: 'TO', text: 'Tocantins' }
    ]

    this._loadFirstOptions()
  }

  setOptionsWithFormaPagamento () {
    this.__state.options = [
      { value: 'dinheiro', text: 'Dinheiro' },
      { value: 'credito', text: 'Crédito' },
      { value: 'debito', text: 'Débito' }
    ]

    this._loadFirstOptions()
  }
}

module.exports = Selecto
