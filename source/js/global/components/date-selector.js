class DateSelector {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      selectedDate: { year: 0, month: 0 },
      nowDate: { year: 0, month: 0 }
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  init () {
    const date = this._getCurrentDate()

    this.getState().selectedDate.month = date.month
    this.getState().nowDate.month = date.month

    this.getState().selectedDate.year = date.year
    this.getState().nowDate.year = date.year
  }

  render () {
    const year = this.__state.selectedDate.year
    const month = this.__state.selectedDate.month
    const monthName = this._getMonthName(month)

    const html =
    `
    <div class="seta__voltar">
      <button class="voltar">
        <i class="icon-arrow-thin-left"></i>
      </button>
    </div>
    <div class="data">
      <p class="data__mes">${monthName}</p>
      <p class="data__ano">${year}</p>
    </div>
    <div class="seta__avancar">
      <button class="avancar">
        <i class="icon-arrow-thin-right"></i>
      </button>
    </div>
    `

    this.__component.innerHTML = html
  }

  _reloadComponent () {
    this.__component.innerHTML = ''
    this.render()
  }

  // Date Functions

  _getMonthName (_month) {
    if (!_month) return

    const monthsName = [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro'
    ]

    return monthsName[_month - 1]
  }

  _getCurrentDate () {
    return {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear()
    }
  }

  // Control

  value () {
    return this.getState().selectedDate
  }

  _nextMonth () {
    const date = this.getState().selectedDate

    if (date.month < 12) {
      this.getState().selectedDate.month++
    } else {
      this.getState().selectedDate.month = 1
      this.getState().selectedDate.year++
    }

    this._reloadComponent()
  }

  _previousMonth () {
    const date = this.getState().selectedDate

    if (date.month > 1) {
      this.getState().selectedDate.month--
    } else {
      this.getState().selectedDate.month = 12
      this.getState().selectedDate.year--
    }

    this._reloadComponent()
  }
}

module.exports = DateSelector
