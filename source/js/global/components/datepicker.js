class DatePicker {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      weekdaysName: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
      monthsName: [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
      ],
      selectedDate: { year: 0, month: 0 },
      nowDate: { year: 0, month: 0, day: 0 },
      value: ''
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  init () {
    const date = this._getCurrentDate()

    this.getState().selectedDate.month = date.month
    this.getState().nowDate.month = date.month

    this.getState().selectedDate.year = date.year
    this.getState().nowDate.year = date.year

    this.getState().nowDate.day = date.day
  }

  render () {
    const html =
    `
    <input class="datepicker__value" type="text" readonly="true">
    <div class="datepicker__calendar">
      <div class="calendar__controls">
        <div class="calendar__controls__previous">
          <i class="icon-arrow-thin-left"></i>
        </div>
        <div class="calendar__controls__date">
          <span class="date__month"></span>
          <span class="date__year"></span>
        </div>
        <div class="calendar__controls__next">
          <i class="icon-arrow-thin-right"></i>
        </div>
      </div>
      <div class="calendar__weekdays"></div>
      <div class="calendar__days"></div>
    </div>
    `

    this.__component.innerHTML = html
    this._reloadStateComponent()
  }

  _renderDays () {
    const DaysDiv = this.__component.querySelector('.calendar__days')
    DaysDiv.innerHTML = ''

    const selectedDate = this.getState().selectedDate
    const indexOfWeekDay = this._getIndexOfWeekDay(selectedDate.year, selectedDate.month)
    const totalOfMonthDays = this._getNumberOfDaysFromMonth(selectedDate.year, selectedDate.month)

    // step 1 - insert blank days until start day 1 of the month
    for (let index = 0; index < indexOfWeekDay; index++) {
      const span = '<span class="none"></span>'
      DaysDiv.innerHTML += span
    }

    // step 2 - insert days of the month in calendar
    for (let index = 1; index <= totalOfMonthDays; index++) {
      const isToday = this._checkSelectedDayIsnowDate(index)
      const todayClass = (isToday) ? 'class="today"' : ''

      const span = `<span data-day="${index}" ${todayClass}>${index}</span>`
      DaysDiv.innerHTML += span
    }
  }

  _renderWeekdays () {
    const WeekdaysDiv = this.__component.querySelector('.calendar__weekdays')

    WeekdaysDiv.innerHTML = ''
    for (const weekday of this.getState().weekdaysName) {
      const span = `<span>${weekday}</span>`
      WeekdaysDiv.innerHTML += span
    }
  }

  _renderDataLabels () {
    const year = this.getState().selectedDate.year
    const month = this.getState().selectedDate.month
    const monthName = this._getMonthName(month)

    const monthNameSpan = this.__component.querySelector('.calendar__controls__date .date__month')
    const yearSpan = this.__component.querySelector('.calendar__controls__date .date__year')

    monthNameSpan.innerText = monthName
    yearSpan.innerText = year
  }

  _reloadStateComponent () {
    this._renderWeekdays()
    this._renderDays()
    this._renderDataLabels()
  }

  // Calendar

  _showCalendar () {
    const calendarDiv = this.__component.querySelector('.datepicker__calendar')
    calendarDiv.classList.add('show')
  }

  _hideCalendar () {
    const calendarDiv = this.__component.querySelector('.datepicker__calendar')
    calendarDiv.classList.remove('show')
  }

  // Control

  value () {
    return this.getState().value
  }

  selectDate (_dateUs) {
    const newDate = this._splitDateUS(_dateUs)
    this.getState().selectedDate = { year: newDate.year, month: newDate.month }
    this.getState().value = _dateUs
    this.__component.querySelector('input').value = this._formatDate(newDate.year, newDate.month, newDate.day).br
    this._reloadStateComponent()
  }

  clear () {
    this.getState().value = ''
    this.getState().selectedDate.year = this.getState().nowDate.year
    this.getState().selectedDate.month = this.getState().nowDate.month
    this._reloadStateComponent()
  }

  _nextMonth () {
    const date = this.getState().selectedDate

    if (date.month < 12) {
      this.getState().selectedDate.month++
    } else {
      this.getState().selectedDate.month = 1
      this.getState().selectedDate.year++
    }

    this._reloadStateComponent()
  }

  _previousMonth () {
    const date = this.getState().selectedDate

    if (date.month > 1) {
      this.getState().selectedDate.month--
    } else {
      this.getState().selectedDate.month = 12
      this.getState().selectedDate.year--
    }

    this._reloadStateComponent()
  }

  // Aux Functions

  _getCurrentDate () {
    return {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      day: new Date().getDate()
    }
  }

  _getIndexOfWeekDay (year, month, day = 1) {
    const newDate = new Date(year, (month - 1), day)
    return newDate.getDay()
  }

  _getNumberOfDaysFromMonth (year, month) {
    const newDate = new Date(year, (month), 0)
    return newDate.getDate()
  }

  _getMonthName (_month) {
    if (!_month) return
    return this.getState().monthsName[_month - 1]
  }

  _formatDate (_year, _month, _day) {
    const day = _day.toString().padStart(2, '0')
    const month = _month.toString().padStart(2, '0')
    const year = _year.toString().padStart(4, '0')

    return {
      br: `${day}/${month}/${year}`,
      us: `${year}-${month}-${day}`
    }
  }

  _splitDateUS (_dateUs) {
    const split = _dateUs.split('-')
    return {
      year: split[0],
      month: split[1],
      day: split[2]
    }
  }

  _checkSelectedDayIsnowDate (_day) {
    if (Number(this.getState().selectedDate.year) !== Number(this.getState().nowDate.year)) return false
    if (Number(this.getState().selectedDate.month) !== Number(this.getState().nowDate.month)) return false
    if (Number(_day) !== Number(this.getState().nowDate.day)) return false

    return true
  }

  // Events

  activeCalendarOnInputClick () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('input')
      if (e.target !== match) return
      this._showCalendar()
    })
  }

  activeClickOnNextArrow () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.datepicker__calendar .calendar__controls__next')
      if (e.target !== match) return

      this._nextMonth()
    })
  }

  activeClickOnPreviousArrow () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.datepicker__calendar .calendar__controls__previous')
      if (e.target !== match) return

      this._previousMonth()
    })
  }

  activeClickOutOfComponent () {
    document.addEventListener('click', (e) => {
      const match = `${this.__id}`
      if (e.target.closest(match)) return

      this._hideCalendar()
    })
  }

  activeClickOnDay () {
    document.addEventListener('click', (e) => {
      const match = `${this.__id} .datepicker__calendar .calendar__days span`
      if (!e.target.matches(match)) return

      const daySelected = e.target.getAttribute('data-day')
      const selectedDate = this.getState().selectedDate
      const formatedDate = this._formatDate(selectedDate.year, selectedDate.month, daySelected)
      this.selectDate(formatedDate.us)
    })
  }
}

module.exports = DatePicker
