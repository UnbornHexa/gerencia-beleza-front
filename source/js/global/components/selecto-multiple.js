/* Requires */

const ACCENT = require('../helpers/accent')

/* Class */

class SelectoMultiple {
  constructor (_id) {
    this.__component = document.querySelector(_id)
    this.__state = {
      options: [],
      selected: [],
      clearTimeoutSearch: {}
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () {
    this.__component.innerHTML = `
    <div class="selected"></div>
    <div class="dropdown">
      <div class="dropdown__busca input">
        <input type="text" placeholder="Busque...">
      </div>
      <div class="dropdown__opcoes"></div>
    </div>
    `
  }

  // Dropdown

  _showDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    dropdownDiv.classList.add('show')
  }

  _hideDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    dropdownDiv.classList.remove('show')
  }

  _toggleDropdown () {
    const dropdownDiv = this.__component.querySelector('.dropdown')
    if (dropdownDiv.style.display === 'block') {
      this._hideDropdown()
    } else {
      this._showDropdown()
    }
  }

  // Searchbar

  _resetSearchbar () {
    const searchDiv = this.__component.querySelector('.dropdown .dropdown__busca input')
    searchDiv.value = ''
  }

  // Options

  importOptions (_options) {
    this.__state.options = _options
    this._loadOptions()
  }

  _loadOptions () {
    this._cleanOptions()

    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    const selectedDiv = this.__component.querySelector('.selected')
    let options = ''
    let selecteds = ''

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      const checkedClass = (currentOption.checked) ? 'checked' : ''

      options += `<option class="${checkedClass}" value="${currentOption.value}">${currentOption.text}</option>`

      if (checkedClass) selecteds += `<span>${currentOption.text}</span>`
    }

    optionsDiv.innerHTML = options
    selectedDiv.innerHTML = selecteds
  }

  filterOptions (_phrase) {
    this._cleanOptions()

    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    let optionsFiltered = ''

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      const checkedClass = (currentOption.checked) ? 'checked' : ''

      const optionText = ACCENT().remove(currentOption.text.toUpperCase())
      _phrase = ACCENT().remove(_phrase.toUpperCase())

      if (optionText.includes(_phrase)) {
        optionsFiltered += `<option class="${checkedClass}" value="${currentOption.value}">${currentOption.text}</option>`
      }
    }

    optionsDiv.innerHTML += optionsFiltered
  }

  _cleanOptions () {
    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    const selectedDiv = this.__component.querySelector('.selected')

    optionsDiv.innerHTML = ''
    selectedDiv.innerHTML = ''
  }

  // Selected

  selectOption (_value) {
    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]

      if (currentOption.value === _value) {
        if (currentOption.checked !== true) { this._addOnSelected(currentOption) } else { this._delFromSelected(index) }

        this._loadOptions()
        break
      }
    }
  }

  _addOnSelected (_option) {
    this.__state.selected.push({
      value: _option.value,
      text: _option.text
    })

    _option.checked = true
  }

  _delFromSelected (_index) {
    const currentOption = this.__state.options[_index]
    delete currentOption.checked

    this.__state.selected = this.__state.selected.filter(_optionSelected => {
      return _optionSelected.value !== currentOption.value
    })
  }

  resetSelected () {
    this.__state.selected = []
    this.__state.options = this.__state.options.map(_option => {
      return { value: _option.value, text: _option.text }
    })
  }

  // Utils

  disable () {
    this.__component.setAttribute('disabled', true)
  }

  value () {
    return this.__state.selected
  }

  isEmpty () {
    if (this.__state.selected.length === 0) return true
    return false
  }

  // Events

  activeClickOnSelecto () {
    document.addEventListener('click', (e) => {
      if (e.target !== this.__component) return

      this._toggleDropdown()
    })
  }

  activeHiddenOnLostFocus () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__busca input')
      if (e.target === this.__component || e.target === matchElement) return

      this._hideDropdown()
    })
  }

  activeClickOnOption () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__opcoes')
      if (e.target.parentElement !== matchElement) return

      this.selectOption(e.target.value)
      this._resetSearchbar()
      this._loadOptions()
    })
  }

  activeKeyupOnSearchbar () {
    document.addEventListener('keyup', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__busca input')
      if (e.target !== matchElement) return

      clearTimeout(this.__state.clearTimeoutSearch)
      if (e.target.value.length === 0) {
        this._loadOptions()
        return
      }

      this.__state.clearTimeoutSearch = setTimeout(() => {
        const pesquisa = e.target.value
        this.filterOptions(pesquisa)
      }, 500)
    })
  }
}

module.exports = SelectoMultiple
