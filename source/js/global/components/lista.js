class Lista {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      items: [],
      filteredItems: [],
      itemsPerPage: 19,
      indexLastPage: 0,
      indexFirstPage: 1,
      indexCurrentPage: 1
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () {
    this.__component.innerHTML = `
    <div class="lista__titulo"></div>
    <ul class="lista__itens"></ul>
    <div class="lista__paginacao"></div>
    `

    this._calcPagination()
  }

  // Items

  _loadItemsFromPage (_index) {
    this._resetItems()
    const startOfRange = (_index - 1) * this.__state.itemsPerPage
    const endOfRange = (_index * this.__state.itemsPerPage)
    const rangeOfItems = this.__state.filteredItems.slice(startOfRange, endOfRange)

    for (const index in rangeOfItems) {
      this._addItem(rangeOfItems[index])
    }

    this._calcPagination()
  }

  _addItem () {}

  importItems (_items) {
    this._resetItems()
    this.__state.items = _items
    this.__state.filteredItems = _items
    this._loadItemsFromPage(1)
    this._goFirstPage(1)
  }

  // Pagination

  _calcPagination () {
    this.__state.indexLastPage = Math.ceil(this.__state.filteredItems.length / this.__state.itemsPerPage)
    this._renderPagination()
  }

  _renderPagination () {
    const paginationDiv = this.__component.querySelector('.lista__paginacao')
    paginationDiv.innerHTML = ''

    if ((this.__state.indexCurrentPage - 2) >= this.__state.indexFirstPage) {
      paginationDiv.innerHTML += `<span class="lista__paginacao__primeira">${this.__state.indexFirstPage}</span>`
      paginationDiv.innerHTML += '<span class="lista__paginacao__divisor">...</span>'
    }

    if ((this.__state.indexCurrentPage - 1) >= this.__state.indexFirstPage) {
      paginationDiv.innerHTML += `<span class="lista__paginacao__voltar">${this.__state.indexCurrentPage - 1}</span>`
    }

    paginationDiv.innerHTML += `<span class="lista__paginacao__atual">${this.__state.indexCurrentPage}</span>`

    if ((this.__state.indexCurrentPage + 1) <= this.__state.indexLastPage) {
      paginationDiv.innerHTML += `<span class="lista__paginacao__avancar">${this.__state.indexCurrentPage + 1}</span>`
    }

    if ((this.__state.indexCurrentPage + 2) <= this.__state.indexLastPage) {
      paginationDiv.innerHTML += '<span class="lista__paginacao__divisor"> ... </span>'
      paginationDiv.innerHTML += `<span class="lista__paginacao__ultima">${this.__state.indexLastPage}</span>`
    }
  }

  _goFirstPage () {
    this.__state.indexCurrentPage = this.__state.indexFirstPage
    this._resetItems()
    this._loadItemsFromPage(this.__state.indexCurrentPage)
  }

  _goPreviousPage () {
    if (this.__state.indexCurrentPage <= this.__state.indexFirstPage) return
    this.__state.indexCurrentPage--
    this._resetItems()
    this._loadItemsFromPage(this.__state.indexCurrentPage)
  }

  _goNextPage () {
    if (this.__state.indexCurrentPage >= this.__state.indexLastPage) return
    this.__state.indexCurrentPage++
    this._resetItems()
    this._loadItemsFromPage(this.__state.indexCurrentPage)
  }

  _goLastPage () {
    this.__state.indexCurrentPage = this.__state.indexLastPage
    this._resetItems()
    this._loadItemsFromPage(this.__state.indexCurrentPage)
  }

  _resetItems () {
    const itemsDiv = this.__component.querySelector('.lista__itens')
    itemsDiv.innerHTML = ''
  }

  // Options Menu

  _hideOptionsMenu () {
    const itemsLi = this.__component.querySelectorAll('ul li')

    itemsLi.forEach(_li => {
      _li.querySelector('span .lista__opcoes').classList.remove('show')
    })
  }

  _showOptionsMenu (li) {
    this._hideOptionsMenu()
    const optionsMenu = li.querySelector('.lista__opcoes')
    optionsMenu.classList.add('show')
  }

  _activeLi (li) {
    this._desactiveLi()
    li.classList.add('active')
  }

  _desactiveLi () {
    const itemsLi = this.__component.querySelectorAll('ul li')
    itemsLi.forEach(_li => {
      _li.classList.remove('active')
    })
  }

  // Events

  activeClickOnFirsrPage () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.lista__paginacao .lista__paginacao__primeira')
      if (e.target !== match) return
      this._goFirstPage()
    })
  }

  activeClickOnPreviousPage () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.lista__paginacao .lista__paginacao__voltar')
      if (e.target !== match) return
      this._goPreviousPage()
    })
  }

  activeClickOnNextPage () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.lista__paginacao .lista__paginacao__avancar')
      if (e.target !== match) return
      this._goNextPage()
    })
  }

  activeClickOnLastPage () {
    document.addEventListener('click', (e) => {
      const match = this.__component.querySelector('.lista__paginacao .lista__paginacao__ultima')
      if (e.target !== match) return
      this._goLastPage()
    })
  }

  // Li

  activeClickOnLi () {
    const match = `${this.__id} ul li`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      this._showOptionsMenu(e.target)
      this._activeLi(e.target)
    })
  }

  activeClickOutOfLi () {
    const match = `${this.__id} ul li`
    document.addEventListener('click', (e) => {
      if (e.target.matches(match)) return

      this._desactiveLi()
      this._hideOptionsMenu()
    })
  }
}

module.exports = Lista
