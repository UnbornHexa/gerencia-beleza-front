const Module = () => {
  const methods = {}

  methods.activePreventSubmit = () => {
    document.addEventListener('submit', (e) => {
      if (!e.target.matches('form')) return
      e.preventDefault()
    })
  }

  return methods
}

module.exports = Module
