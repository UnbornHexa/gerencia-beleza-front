/* Requires */

const TOKEN = require('../helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  let __isIdle = false

  // Methods

  methods.activePreventDragAndDrop = () => {
    document.addEventListener('dragstart', (e) => {
      e.preventDefault()
    }, false)

    document.addEventListener('drop', (e) => {
      e.preventDefault()
    }, false)
  }

  methods.activePreventBackButton = () => {
    window.history.pushState(null, null, window.location.href)
    window.onpopstate = () => { window.history.go(1) }
  }

  methods.checkIdle = () => {
    window.addEventListener('focus', () => {
      if (__isIdle === false) return

      TOKEN().redirectIfExpiredToken()
      __isIdle = false
    })

    window.addEventListener('blur', () => {
      __isIdle = true
    })
  }

  return methods
}

module.exports = Module
