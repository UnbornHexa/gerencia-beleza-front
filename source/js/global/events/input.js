/* Requires */

const MASK = require('../helpers/mask.js')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeBlockLetterOnTypeTel = () => {
    document.addEventListener('keypress', (e) => { handler(e) })
    document.addEventListener('keyup', (e) => { handler(e) })

    function handler (e) {
      if (!e.target.matches('input[type="tel"]')) return

      const acceptedChars = e.target.value.replace(/(?![R])[a-z|A-Z]/g, '')
      e.target.value = acceptedChars
    }
  }

  // Generic

  methods.activeGenericMask = () => {
    document.addEventListener('keypress', (e) => { handler(e) })
    document.addEventListener('keyup', (e) => { handler(e) })

    function handler (e) {
      if (!e.target.matches('input')) return
      if (!e.target.getAttribute('data-mask')) return

      const _pattern = e.target.getAttribute('data-mask')
      if (e.target.value.length >= _pattern.length) e.preventDefault()
      e.target.value = MASK().generic(e.target.value, _pattern)
    }
  }

  // Phone

  methods.activePhoneMask = () => {
    document.addEventListener('keypress', (e) => { handler(e) })
    document.addEventListener('keyup', (e) => { handler(e) })

    function handler (e) {
      if (!e.target.matches('input')) return
      if (!e.target.getAttribute('data-mask-phone')) return
      if (e.target.value.length >= 16) e.preventDefault()

      let pattern = '(##) ####-#####'
      const phone = e.target.value.replace(/[^0-9]/g, '')
      if (phone.length > 10) pattern = '(##) # ####-####'

      e.target.value = MASK().generic(e.target.value, pattern)
    }
  }

  // Money

  methods.activeMoneyMask = () => {
    document.addEventListener('keyup', (e) => {
      if (!e.target.matches('input')) return
      if (!e.target.getAttribute('data-mask-money')) return

      e.target.value = MASK().moneyMask(e.target.value)
    })
  }

  return methods
}

module.exports = Module
