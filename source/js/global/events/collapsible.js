const Module = () => {
  const methods = {}

  methods.activeClickOnTitle = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.closest('.collapsible .title')) return

      const collapsibleDiv = e.target.parentElement
      const statusCollapsible = collapsibleDiv.classList.contains('active')
      methods._removeAllActives()

      if (statusCollapsible) {
        collapsibleDiv.classList.remove('active')
      } else {
        collapsibleDiv.classList.add('active')
      }
    })
  }

  methods._removeAllActives = () => {
    const collapsibles = document.querySelectorAll('.collapsible')

    for (const collapsible of collapsibles) {
      collapsible.classList.remove('active')
    }
  }

  return methods
}

module.exports = Module
