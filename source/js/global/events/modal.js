/* Requires */

const MAIN = require('../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeClickOnButtonClose = () => {
    document.addEventListener('click', (e) => {
      if (e.target.getAttribute('data-close') !== 'true') return

      const modals = document.querySelectorAll('.modal')
      modals.forEach(_modal => {
        _modal.style.display = 'none'
      })

      MAIN().enableMain()
    })
  }

  return methods
}

module.exports = Module
