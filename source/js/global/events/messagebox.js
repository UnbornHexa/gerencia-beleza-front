const Module = () => {
  const methods = {}

  methods.activeClickToClose = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('messagebox')) return
      document.body.removeChild(e.target)
    })
  }

  return methods
}

module.exports = Module
