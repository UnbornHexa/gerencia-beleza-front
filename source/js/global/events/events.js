/* Requires */

const COLLAPSIBLE = require('./collapsible')
const INPUT = require('./input')
const MESSAGEBOX = require('./messagebox')
const MODAL = require('./modal')
const FORM = require('./form')
const WINDOW = require('./window')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    COLLAPSIBLE().activeClickOnTitle()

    INPUT().activeGenericMask()
    INPUT().activeMoneyMask()
    INPUT().activePhoneMask()
    INPUT().activeBlockLetterOnTypeTel()

    FORM().activePreventSubmit()

    MESSAGEBOX().activeClickToClose()

    MODAL().activeClickOnButtonClose()

    WINDOW().activePreventDragAndDrop()
    WINDOW().activePreventBackButton()
  }

  return methods
}

module.exports = Module
