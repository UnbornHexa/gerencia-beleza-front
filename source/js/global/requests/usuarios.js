/* Requires */

const HTTP = require('../helpers/http')
const TOKEN = require('../helpers/token')
const MESSAGEBOX = require('../modules/messagebox')
const HOSTS = require('../data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __idUsuario = TOKEN().getIdUsuarioFromToken()
  const __url = `${HOSTS().url.api_plataforma}/usuarios`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getImagem = async () => {
    const url = `${__url}/image/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
