/* Requires */

const HTTP = require('../helpers/http')

/* Module */

const Module = () => {
  const methods = {}

  methods.getAddress = async _cep => {
    const url = `https://viacep.com.br/ws/${_cep}/json/`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, null, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
