const Module = () => {
  const methods = {}

  // Internal Variables

  const __protocol = window.location.protocol
  const __host = window.location.host

  methods.url = {}

  // Localhost
  methods.url.app_plataforma = `${__protocol}//${__host}`

  // API
  // methods.url.api_plataforma = 'http://localhost:53010'
  methods.url.api_plataforma = 'https://api-app.gerenciabeleza.com.br'

  return methods
}

module.exports = Module
