const Module = () => {
  const methods = {}

  methods.money = (_valor) => {
    if (!_valor && _valor !== 0) return 'R$ 00,00'

    const valor = Number(_valor)
    const pais = 'pt-BR'
    const parametros = {
      style: 'currency',
      currency: 'BRL'
    }

    const valorFormatado = valor.toLocaleString(pais, parametros)
    return valorFormatado
  }

  methods.unformatMoney = (_valor) => {
    let valor
    valor = _valor.replace(/\u00A0/, ' ')
    valor = valor.replace('R$ ', '')
    valor = valor.replace(/\./g, '')
    valor = valor.replace(',', '.')
    valor = Number(valor)
    return valor
  }

  methods.number = (_numero) => {
    return _numero.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }

  methods.onlyNumber = (_numero) => {
    return _numero.toString().replace(/[^0-9]/g, '')
  }

  return methods
}

module.exports = Module
