const Module = () => {
  const methods = {}

  methods.today = () => {
    const newDate = new Date()
    const day = (newDate.getDate()).toString().padStart(2, '0')
    const month = (newDate.getMonth() + 1).toString().padStart(2, '0')
    const year = newDate.getFullYear()
    return `${year}-${month}-${day}`
  }

  methods.yearAndMonth = () => {
    const newDate = new Date()
    const month = (newDate.getMonth() + 1).toString().padStart(2, '0')
    const year = newDate.getFullYear()
    return `${year}-${month}`
  }

  methods.getMonthName = (_month) => {
    _month = _month.toString()
    _month = _month.padStart(2, '0')

    if (_month === '01') return 'Janeiro'
    if (_month === '02') return 'Fevereiro'
    if (_month === '03') return 'Março'
    if (_month === '04') return 'Abril'
    if (_month === '05') return 'Maio'
    if (_month === '06') return 'Junho'
    if (_month === '07') return 'Julho'
    if (_month === '08') return 'Agosto'
    if (_month === '09') return 'Setembro'
    if (_month === '10') return 'Outubro'
    if (_month === '11') return 'Novembro'
    if (_month === '12') return 'Dezembro'
  }

  return methods
}

module.exports = Module
