const Module = () => {
  const methods = {}

  methods.subscribe = (_key, _obj) => {
    const symbol = Symbol.for(_key)
    window[symbol] = _obj
  }

  methods.retrieve = (_key) => {
    const globalSymbols = Object.getOwnPropertySymbols(window)
    const indexSymbol = globalSymbols.indexOf(Symbol.for(_key))
    const componentSymbol = globalSymbols[indexSymbol]
    return window[componentSymbol]
  }

  return methods
}

module.exports = Module
