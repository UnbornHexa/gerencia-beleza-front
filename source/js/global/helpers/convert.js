const Module = () => {
  const methods = {}

  methods.dateBR = (_data) => {
    if (!_data) return ''

    try {
      const data = _data.split('-')
      return data[2] + '/' + data[1] + '/' + data[0]
    } catch (e) {
      return ''
    }
  }

  methods.dateUS = (_data) => {
    if (!_data) return ''

    try {
      const data = _data.split('/')
      return data[2] + '-' + data[1] + '-' + data[0]
    } catch (e) {
      return ''
    }
  }

  return methods
}

module.exports = Module
