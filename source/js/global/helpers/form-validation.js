const Module = () => {
  const methods = {}

  methods.date = (_date) => {
    const pattern = new RegExp(/[0-9]{4}-[0-9]{2}-[0-9]{2}$/)
    const match = pattern.test(_date)
    if (!match) return false

    const dateSplited = _date.split('-')
    const day = Number(dateSplited[2])
    const month = Number(dateSplited[1])
    const year = Number(dateSplited[0])

    if (day < 1 || day > 31) return false
    if (month < 1 || month > 12) return false
    if (year < 1900 || year > 2100) return false

    return true
  }

  methods.time = (_time) => {
    const pattern = new RegExp(/[0-9]{2}:[0-9]{2}$/)
    const match = pattern.test(_time)
    if (!match) return false

    const timeSplited = _time.split(':')
    const hour = Number(timeSplited[0])
    const minute = Number(timeSplited[1])

    if (hour < 0 || hour > 23) return false
    if (minute < 0 || minute > 59) return false

    return true
  }

  methods.phone = (_phone) => {
    const phone = _phone.replace(/[^0-9]/g, '')
    const digits = phone.length
    return (digits === 11 || digits === 10)
  }

  methods.cep = (_cep) => {
    const cep = _cep.replace(/[^0-9]/g, '')
    return (cep.length === 8)
  }

  methods.email = (_email) => {
    const pattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    return pattern.test(_email)
  }

  methods.year = (_year) => {
    const year = Number(_year)
    return (year >= 1900 && year <= 2100)
  }

  return methods
}

module.exports = Module
