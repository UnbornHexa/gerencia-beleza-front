const Module = () => {
  const methods = {}

  methods.load = () => {
    if (!window.localStorage.getItem('dark-theme')) return
    methods.activeDarkTheme()
  }

  methods.activeDarkTheme = () => {
    window.localStorage.setItem('dark-theme', true)
    document.body.setAttribute('data-theme', 'dark')
  }

  methods.removeDarkTheme = () => {
    window.localStorage.removeItem('dark-theme')
    document.body.removeAttribute('data-theme')
  }

  return methods
}

module.exports = Module
