/* Requires */

const HOSTS = require('../data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  methods.getToken = () => {
    return window.localStorage.getItem('token-usuario') || false
  }

  methods.setToken = _data => {
    window.localStorage.setItem('token-usuario', _data)
  }

  methods.delToken = () => {
    window.localStorage.removeItem('token-usuario')
  }

  // Get Info From Token

  methods.getTimestampFromToken = () => {
    return readToken(methods.getToken()).exp || 0
  }

  methods.getIdUsuarioFromToken = () => {
    return readToken(methods.getToken()).data.id || 0
  }

  methods.getNameUsuarioFromToken = () => {
    return readToken(methods.getToken()).data.nome || ''
  }

  methods.getEmailUsuarioFromToken = () => {
    return readToken(methods.getToken()).data.email || ''
  }

  methods.getIdEmpresaFromToken = () => {
    return readToken(methods.getToken()).data.id_empresa || 0
  }

  methods.getPermissaoFromToken = () => {
    return readToken(methods.getToken()).data.permissao || {}
  }

  methods.getAssinaturaStatusFromToken = () => {
    return readToken(methods.getToken()).data.assinatura_expirada || false
  }

  // Methods Using Token

  methods.redirectIfExpiredToken = () => {
    if (!methods.getToken()) {
      window.localStorage.removeItem('token-usuario')
      window.localStorage.removeItem('imagem-avatar')
      window.localStorage.removeItem('imagem-empresa')
      window.location.assign(`${HOSTS().url.app_plataforma}/entrar`)
      return
    }

    if (isTokenExpired()) return

    methods.delToken()
    window.location.assign(`${HOSTS().url.app_plataforma}/entrar`)
  }

  // Helpers

  function isTokenExpired () {
    const expiracaoTimeStamp = methods.getTimestampFromToken()
    const atualTimeStamp = Number(new Date().getTime() / 1000).toFixed(0)
    const diferencaTimeStamp = expiracaoTimeStamp - atualTimeStamp
    return (diferencaTimeStamp > 0)
  }

  function readToken (_token) {
    const payload = _token.match(/\.([^.]+)\./)[1]
    const tokenDesconvertido = b64DecodeUnicode(payload)
    const dadosToken = JSON.parse(tokenDesconvertido)
    return dadosToken
  }

  function b64DecodeUnicode (str) {
    return decodeURIComponent(window.atob(str).split('').map(c => {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
  }

  return methods
}

module.exports = Module
