const Module = () => {
  const methods = {}

  methods.htmlToText = (_html) => {
    const text = new window.DOMParser().parseFromString(_html, 'text/html')
    return text.body.textContent || ''
  }

  return methods
}

module.exports = Module
