/* Examples */

// const patternTelefone = '(##) ####-#####'
// const patternCep = '##.###-###'
// const patternData = '##/##/####'
// const patternCPF = '###.###.###-##'

/* Module */

const Module = () => {
  const methods = {}

  methods.generic = (_texto, _pattern) => {
    const _campo = _texto.replace(/[^0-9]/g, '')
    let indexCampo = 0
    let validacaoFinal = ''

    // loop entre caracteres do pattern
    for (let indexPattern = 0; indexPattern < _pattern.length; indexPattern++) {
      // verifica se input tem caracteres sobrando
      if (indexCampo + 1 > _campo.length) return validacaoFinal

      // opcoes
      switch (_pattern[indexPattern]) {
        case '#': // números
          validacaoFinal += _campo[indexCampo]
          indexCampo++
          break
        default: // restante
          validacaoFinal += _pattern[indexPattern]
          break
      }
    }

    return validacaoFinal
  }

  methods.moneyMask = _valor => {
    const regexNumero = new RegExp('[^,\\d]', 'g')
    const valorEmString = _valor.replace(regexNumero, '').toString()
    const digitoArray = valorEmString.split(',')
    const restoMultiplo1000 = digitoArray[0].length % 3

    const mil = digitoArray[0].substr(restoMultiplo1000).match(/\d{3}/g)
    let resultado = digitoArray[0].substr(0, restoMultiplo1000)

    if (mil) {
      const separador = restoMultiplo1000 ? '.' : ''
      resultado += separador + mil.join('.')
    }

    resultado = (digitoArray[1] !== undefined) ? resultado + ',' + digitoArray[1] : resultado

    // retorno
    if (resultado.length === 0) return ''
    return `R$ ${resultado}`
  }

  return methods
}

module.exports = Module
