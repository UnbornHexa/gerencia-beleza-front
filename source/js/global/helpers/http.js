/* Requires */

const TOKEN = require('./token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  let __method = 'GET'
  let __data = null
  let __headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }

  // Methods

  methods.request = async (_url, _method, _data, _headers, _callback) => {
    if (_headers !== null) __headers = _headers
    if (_data !== null) __data = JSON.stringify(_data)
    if (_method !== null) __method = _method

    const fetchOption = {
      method: __method,
      body: __data,
      headers: __headers
    }
    if (!_data) delete fetchOption.body

    await window.fetch(_url, fetchOption)
      .then(_response => {
        _response.json()
          .then(_dataInJSON => {
            const innerRsponse = {
              status: _response.status,
              body: _dataInJSON
            }
            _callback(innerRsponse)
          })
      })
      .catch(_error => {
        try {
          _callback(_error.json())
        } catch (e) {
          _callback(_error)
        }
      })
  }

  methods.sendFile = async (_url, _method, _data, _callback) => {
    const token = TOKEN().getToken()
    const fetchOption = {
      method: _method,
      body: _data,
      headers: { 'x-access-token': token }
    }

    await window.fetch(_url, fetchOption)
      .then(_response => {
        _response.json()
          .then(_dataInJSON => {
            const innerRsponse = {
              status: _response.status,
              body: _dataInJSON
            }
            _callback(innerRsponse)
          })
      })
      .catch(_error => {
        try {
          _callback(_error.json())
        } catch (e) {
          _callback(_error)
        }
      })
  }

  // Custom Headers

  methods.getHeadersWithToken = () => {
    const token = TOKEN().getToken()
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': token
    }
    return headers
  }

  return methods
}

module.exports = Module
