/* Requires */

const HOSTS = require('../../global/data/hosts')
const TOKEN = require('../helpers/token')
const ASIDE = require('../../structure/aside/helpers/active-aside')

/* Module */

const Module = () => {
  const methods = {}

  methods.hideNotAllowedAreas = () => {
    if (!methods.checkPermission('agenda', 'view')) ASIDE().hideLink('agenda')
    if (!methods.checkPermission('clientes', 'view')) ASIDE().hideLink('clientes')
    if (!methods.checkPermission('comandas', 'view')) ASIDE().hideLink('comandas')
    if (!methods.checkPermission('servicos', 'view')) ASIDE().hideLink('servicos')
    if (!methods.checkPermission('produtos', 'view')) ASIDE().hideLink('produtos')
    if (!methods.checkPermission('materiais', 'view')) ASIDE().hideLink('materiais')
    if (!methods.checkPermission('lancamentos', 'view')) ASIDE().hideLink('financeiro')
    if (!methods.checkPermission('permissoes', 'view')) ASIDE().hideLink('permissoes')
    if (!methods.checkPermission('usuarios', 'view')) ASIDE().hideLink('funcionarios')
    if (!methods.checkAdmin()) ASIDE().hideLink('empresa')
  }

  // Check

  methods.checkPermission = (_setor, _permissao) => {
    const permissaoOBJ = TOKEN().getPermissaoFromToken()
    if (permissaoOBJ[_setor][_permissao] === true) return true
    return false
  }

  methods.checkAdmin = () => {
    const permissaoOBJ = TOKEN().getPermissaoFromToken()
    if (permissaoOBJ.admin === true) return true
    return false
  }

  // Redirects

  methods.checkEmpresaExpired = () => {
    if (TOKEN().getAssinaturaStatusFromToken() === true) {
      window.location.assign(`${HOSTS().url.app_plataforma}/app/empresa`)
    }
  }

  return methods
}

module.exports = Module
