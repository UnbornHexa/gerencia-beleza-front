/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const CONVERT = require('../../../global/helpers/convert')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_cliente')

  const __nomeInput = __modal.querySelector('.nome input')
  const __telefone1Input = __modal.querySelector('.contato__telefone1 input')
  const __telefone2Input = __modal.querySelector('.contato__telefone2 input')
  const __dataNascimentoInput = __modal.querySelector('.nascimento input')
  const __observacaoInput = __modal.querySelector('.observacao textarea')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value && __telefone1Input.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.validateFields = () => {
    const dataNascimentoOk = FORM_VALIDATION().date(CONVERT().dateUS(__dataNascimentoInput.value))
    const telefone1Ok = FORM_VALIDATION().phone(__telefone1Input.value)
    const telefone2Ok = FORM_VALIDATION().phone(__telefone2Input.value)

    if (__dataNascimentoInput.value && !dataNascimentoOk) {
      MESSAGEBOX().alert('Preencha uma data de nascimento válida.')
      return false
    }

    if ((__telefone1Input.value && !telefone1Ok) || (__telefone2Input.value && !telefone2Ok)) {
      MESSAGEBOX().alert('Preencha um número de telefone válido.')
      return false
    }

    return true
  }

  methods.resetForm = () => {
    __nomeInput.value = ''
    __telefone1Input.value = ''
    __telefone2Input.value = ''
    __dataNascimentoInput.value = ''
    __observacaoInput.value = ''
  }

  // Data

  methods.exportDataFromModal = () => {
    const cliente = {}
    cliente.contato = {}

    const dataNascimento = CONVERT().dateUS(__dataNascimentoInput.value)

    // required
    cliente.nome = __nomeInput.value || ''
    cliente.contato.telefone1 = __telefone1Input.value || ''

    // optional
    if (__telefone2Input.value) cliente.contato.telefone2 = __telefone2Input.value || ''
    if (__dataNascimentoInput.value) cliente.data_nascimento = dataNascimento || ''
    if (__observacaoInput.value) cliente.observacao = __observacaoInput.value || ''

    return cliente
  }

  return methods
}

module.exports = Module
