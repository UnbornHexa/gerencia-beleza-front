/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_cliente')

  const __nomeCollapsible = __modal.querySelector('.collapsible .nome span')
  const __telefone1Collapsible = __modal.querySelector('.collapsible .telefone span')
  const __telefone2Collapsible = __modal.querySelector('.collapsible .telefone2 span')
  const __dataNascimentoCollapsible = __modal.querySelector('.collapsible .data__nascimento span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')
  const __observacaoCollapsible = __modal.querySelector('.collapsible .observacao span')
  const __cepCollapsible = __modal.querySelector('.collapsible .cep span')
  const __estadoCollapsible = __modal.querySelector('.collapsible .estado span')
  const __cidadeCollapsible = __modal.querySelector('.collapsible .cidade span')
  const __bairroCollapsible = __modal.querySelector('.collapsible .bairro span')
  const __ruaCollapsible = __modal.querySelector('.collapsible .rua span')
  const __numeroCollapsible = __modal.querySelector('.collapsible .numero span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __nomeCollapsible.innerHTML = '...'
    __dataNascimentoCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'
    __observacaoCollapsible.innerHTML = '...'

    __telefone1Collapsible.innerHTML = '...'
    __telefone2Collapsible.innerHTML = '...'

    __cepCollapsible.innerHTML = '...'
    __estadoCollapsible.innerHTML = '...'
    __cidadeCollapsible.innerHTML = '...'
    __bairroCollapsible.innerHTML = '...'
    __ruaCollapsible.innerHTML = '...'
    __numeroCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    __nomeCollapsible.innerText = _data.nome || '...'
    __dataNascimentoCollapsible.innerText = CONVERT().dateBR(_data.data_nascimento) || '...'
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || '...'
    __observacaoCollapsible.innerText = _data.observacao || '...'

    if (Object.prototype.hasOwnProperty.call(_data, 'contato')) {
      __telefone1Collapsible.innerText = _data.contato.telefone1 || '...'
      __telefone2Collapsible.innerText = _data.contato.telefone2 || '...'
    }

    if (Object.prototype.hasOwnProperty.call(_data, 'endereco')) {
      __cepCollapsible.innerText = _data.endereco.cep || '...'
      __estadoCollapsible.innerText = _data.endereco.estado || '...'
      __cidadeCollapsible.innerText = _data.endereco.cidade || '...'
      __bairroCollapsible.innerText = _data.endereco.bairro || '...'
      __ruaCollapsible.innerText = _data.endereco.rua || '...'
      __numeroCollapsible.innerText = _data.endereco.numero || '...'
    }
  }

  return methods
}

module.exports = Module
