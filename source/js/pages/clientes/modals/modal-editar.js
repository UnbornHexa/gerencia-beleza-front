/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_cliente')

  const __idInput = __modal.querySelector('.informacoes__id input')
  const __nomeInput = __modal.querySelector('.informacoes__nome input')
  const __telefone1Input = __modal.querySelector('.informacoes__contato__telefone1 input')
  const __telefone2Input = __modal.querySelector('.informacoes__contato__telefone2 input')
  const __dataNascimentoInput = __modal.querySelector('.informacoes__nascimento input')
  const __observacaoInput = __modal.querySelector('.informacoes__observacao textarea')

  const __cepInput = __modal.querySelector('.endereco__cep input')
  const __cidadeInput = __modal.querySelector('.endereco__cidade input')
  const __bairroInput = __modal.querySelector('.endereco__bairro input')
  const __ruaInput = __modal.querySelector('.endereco__rua input')
  const __numeroInput = __modal.querySelector('.endereco__numero input')

  const __estadoSelecto = SYMBOL().retrieve('selecto_estado_editar')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value && __telefone1Input.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return true
  }

  methods.validateFields = () => {
    const dataNascimentoOk = FORM_VALIDATION().date(CONVERT().dateUS(__dataNascimentoInput.value))
    const telefone1Ok = FORM_VALIDATION().phone(__telefone1Input.value)
    const telefone2Ok = FORM_VALIDATION().phone(__telefone2Input.value)
    const cepOk = FORM_VALIDATION().cep(__cepInput.value)

    if (__dataNascimentoInput.value && !dataNascimentoOk) {
      MESSAGEBOX().alert('Preencha uma data de nascimento válida.')
      return false
    }

    if ((__telefone1Input.value && !telefone1Ok) || (__telefone2Input.value && !telefone2Ok)) {
      MESSAGEBOX().alert('Preencha um número de telefone válido.')
      return false
    }

    if (__cepInput.value && !cepOk) {
      MESSAGEBOX().alert('Preencha um cep válido.')
      return false
    }

    return true
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __nomeInput.value = ''
    __telefone1Input.value = ''
    __telefone2Input.value = ''
    __dataNascimentoInput.value = ''
    __observacaoInput.value = ''

    __cepInput.value = ''
    __cidadeInput.value = ''
    __bairroInput.value = ''
    __ruaInput.value = ''
    __numeroInput.value = ''
    __estadoSelecto.resetSelected()
    __editarButton.setAttribute('disabled', true)
  }

  // Data

  methods.importDataToModal = (_data) => {
    const dataNascimento = CONVERT().dateBR(_data.data_nascimento)

    __idInput.value = _data._id || ''
    __nomeInput.value = _data.nome || ''
    __dataNascimentoInput.value = dataNascimento || ''
    __observacaoInput.value = _data.observacao || ''

    if (Object.prototype.hasOwnProperty.call(_data, 'contato')) {
      __telefone1Input.value = _data.contato.telefone1 || ''
      __telefone2Input.value = _data.contato.telefone2 || ''
    }

    if (Object.prototype.hasOwnProperty.call(_data, 'endereco')) {
      __cepInput.value = _data.endereco.cep || ''
      __cidadeInput.value = _data.endereco.cidade || ''
      __bairroInput.value = _data.endereco.bairro || ''
      __ruaInput.value = _data.endereco.rua || ''
      __numeroInput.value = _data.endereco.numero || ''
      __estadoSelecto.selectOption(_data.endereco.estado)
    }
  }

  methods.exportDataFromModal = () => {
    const cliente = {}
    cliente.contato = {}
    cliente.endereco = {}

    const dataNascimento = CONVERT().dateUS(__dataNascimentoInput.value)

    // required
    cliente.nome = __nomeInput.value || ''

    // optional
    if (__dataNascimentoInput.value) cliente.data_nascimento = dataNascimento || ''
    if (__telefone1Input.value) cliente.contato.telefone1 = __telefone1Input.value || ''
    if (__telefone2Input.value) cliente.contato.telefone2 = __telefone2Input.value || ''
    if (__observacaoInput.value) cliente.observacao = __observacaoInput.value || ''

    if (__cepInput.value) cliente.endereco.cep = __cepInput.value || ''
    if (__cidadeInput.value) cliente.endereco.cidade = __cidadeInput.value || ''
    if (__bairroInput.value) cliente.endereco.bairro = __bairroInput.value || ''
    if (__ruaInput.value) cliente.endereco.rua = __ruaInput.value || ''
    if (__numeroInput.value) cliente.endereco.numero = FORMAT().onlyNumber(__numeroInput.value) || ''
    if (!__estadoSelecto.isEmpty()) cliente.endereco.estado = __estadoSelecto.value() || ''

    return cliente
  }

  // Cep

  methods.fillCep = (_address) => {
    __cidadeInput.value = _address.localidade || ''
    __bairroInput.value = _address.bairro || ''
    __ruaInput.value = _address.logradouro || ''
    __estadoSelecto.selectOption(_address.uf)
  }

  return methods
}

module.exports = Module
