/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')

const LISTA_CLIENTES = require('./extendeds/lista-clientes')
const SELECTO_ESTADO = require('./extendeds/selecto-estado')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('clientes')
  ASIDE_PICTURE().load()

  LISTA_CLIENTES().init()
  SELECTO_ESTADO().init('section#modal_editar_cliente .endereco__estado .selecto__common', 'selecto_estado_editar')

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_CLIENTES().updateItems()

  EVENTS().activeEvents()
}
start()
