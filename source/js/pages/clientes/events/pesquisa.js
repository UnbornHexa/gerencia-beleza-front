/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaClientes = SYMBOL().retrieve('lista_clientes')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaClientes.getState().filteredItems = __listaClientes.getState().items
          __listaClientes._loadItemsFromPage(1)
          return
        }

        __listaClientes.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaClientes.getState().items) {
          const item = __listaClientes.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaClientes.getState().filteredItems.push(item)
          }
        }

        __listaClientes._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
