/* Requires */

const LISTA_CLIENTES = require('../extendeds/lista-clientes')
const CLIENTES_REQUEST = require('../requests/clientes')
const CEP_REQUEST = require('../../../global/requests/cep')
const MODAL_EDITAR = require('../modals/modal-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_cliente')

  const __idInput = __modal.querySelector('.modal__body .informacoes__id input')
  const __cepInput = __modal.querySelector('.modal__body .endereco__cep input')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idCliente = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_EDITAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await CLIENTES_REQUEST().editCliente(idCliente, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_CLIENTES().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Cep

  methods.activeKeyupOnCep = () => {
    __cepInput.addEventListener('keyup', async (e) => {
      const cep = e.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      try {
        const address = await CEP_REQUEST().getAddress(cep)
        MODAL_EDITAR().fillCep(address)
      } catch (e) { console.log(e) }
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_cliente input') &&
        !e.target.matches('section#modal_editar_cliente textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_cliente .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
