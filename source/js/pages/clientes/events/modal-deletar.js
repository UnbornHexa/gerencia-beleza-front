/* Requires */

const CLIENTES_REQUEST = require('../requests/clientes')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_CLIENTES = require('../extendeds/lista-clientes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_cliente .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_cliente .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idCliente = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await CLIENTES_REQUEST().delCliente(idCliente)
        MODAL_DELETAR().hideModal()
        await LISTA_CLIENTES().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
