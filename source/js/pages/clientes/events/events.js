/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const MAIN_SECTION = require('./section-main')
const MODAL_ADICIONAR = require('./modal-adicionar')
const MODAL_EDITAR = require('./modal-editar')
const MODAL_DELETAR = require('./modal-deletar')
const PESQUISA = require('./pesquisa')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    TOGGLE().activeClickOnToggle()

    MAIN_SECTION().activeClickOnAdicionarButton()

    MODAL_ADICIONAR().activeClickOnAdicionarButton()

    MODAL_EDITAR().activeClickOnEditarButton()
    MODAL_EDITAR().activeKeyupOnCep()
    MODAL_EDITAR().activeChangeOnModal()

    MODAL_DELETAR().activeClickOnDeletarButton()

    PESQUISA().activeKeyupOnPesquisa()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
