/* Requires */

const CLIENTES_REQUEST = require('../requests/clientes')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')
const LISTA_CLIENTES = require('../extendeds/lista-clientes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_cliente .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_ADICIONAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await CLIENTES_REQUEST().addCliente(modalData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_CLIENTES().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
