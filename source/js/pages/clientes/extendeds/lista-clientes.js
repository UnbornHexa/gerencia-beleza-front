/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const CLIENTES_REQUEST = require('../requests/clientes')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_cliente) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li>
      <span class="item__id" hidden>${_cliente._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-user"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_cliente.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__telefone">${_cliente.contato.telefone1}</span>
        </div>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idCliente = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const cliente = await CLIENTES_REQUEST().getClienteById(idCliente)
        MODAL_VISUALIZAR().importDataToModal(cliente)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idCliente = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const cliente = await CLIENTES_REQUEST().getClienteById(idCliente)
        MODAL_EDITAR().importDataToModal(cliente)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idCliente = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeCliente = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_cliente .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_cliente .modal__header p')

      inputIdModal.value = idCliente
      headerNomeModal.innerText = nomeCliente
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__clientes')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_clientes', component)
  }

  methods.updateItems = async () => {
    const listaClientes = SYMBOL().retrieve('lista_clientes')

    try {
      const clientes = await CLIENTES_REQUEST().getClientes()
      listaClientes.importItems(clientes)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
