/* Requires */

const SELECTO_COMPONENT = require('../../../global/components/selecto')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new SELECTO_COMPONENT(_id)

    component.render()

    component.activeClickOnSelecto()
    component.activeHiddenOnLostFocus()
    component.activeClickOnOption()
    component.activeKeyupOnSearchbar()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
