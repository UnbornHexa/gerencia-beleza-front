/* Requires */

const LIST_SELECTOS_COMPONENT = require('../components/list-selectos')

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class ListSelectosExtended extends LIST_SELECTOS_COMPONENT {
  render () {
    let html = ''

    for (const index in this.__state.selected) {
      const item = this.__state.selected[index]

      let metodo
      if (item.metodo === 'dinheiro') metodo = 'Dinheiro'
      if (item.metodo === 'debito') metodo = 'Débito'
      if (item.metodo === 'credito') metodo = 'Crédito'

      html += `
      <div class="item">
        <p class="metodo">${metodo} - </p>
        <span class="valor">${FORMAT().money(item.valor)}</span>
        <i class="remove icon-times-circle"></i>
      </div>
      `
    }

    this.__component.innerHTML = html
  }

  calcTotal () {
    const sum = (accumulator, item) => item.valor + accumulator
    return this.getState().selected.reduce(sum, 0)
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new ListSelectosExtended(_id)
    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
