/* Requires */

const DATEPICKER_COMPONENT = require('../../../global/components/datepicker')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new DATEPICKER_COMPONENT(_id)

    component.init()
    component.render()
    component.activeCalendarOnInputClick()
    component.activeClickOnNextArrow()
    component.activeClickOnPreviousArrow()
    component.activeClickOnDay()
    component.activeClickOutOfComponent()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
