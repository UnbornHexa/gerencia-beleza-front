/* Requires */

const SLIDE_ITEMS_COMPONENT = require('../components/slide-items')

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class SlideItemsExtended extends SLIDE_ITEMS_COMPONENT {
  render () {
    let html = ''

    for (const index in this.__state.selected) {
      const item = this.__state.selected[index]

      html += `
      <div class="item">
        <div class="titulo">
          <p>${item.quantidade}x ${item.servico_nome}</p>
        </div>
        <div class="dados">
          <div class="funcionario">
            <p>Funcionário</p>
            <span>${item.funcionario_nome}</span>
          </div>
          <div class="preco">
            <p>Valor (UN)</p>
            <span>${FORMAT().money(item.valor_unitario)}</span>
          </div>
          <div class="desconto">
            <p>Desconto</p>
            <span>${FORMAT().money(item.valor_desconto)}</span>
          </div>
          <div class="subtotal">
            <p>Subtotal</p>
            <span>${FORMAT().money(item.valor_subtotal)}</span>
          </div>
        </div>
        <div class="botao">
          <button type="button" id="remover_item">Remover</button>
        </div>
      </div>
      `
    }

    this.__component.innerHTML = html
  }

  value () {
    return this.getState().selected.map((_item) => {
      delete _item.servico_nome
      delete _item.funcionario_nome
      return _item
    })
  }

  importItems (_servicos) {
    for (const index in _servicos) {
      const servico = _servicos[index]

      if (servico.id_servico) {
        servico.servico_nome = servico.id_servico.nome
        servico.id_servico = servico.id_servico._id
      }

      if (servico.id_usuario) {
        servico.funcionario_nome = servico.id_usuario.nome
        servico.id_usuario = servico.id_usuario._id
      }

      this.addItem(servico)
    }
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new SlideItemsExtended(_id)

    component.activeOnDrag()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
