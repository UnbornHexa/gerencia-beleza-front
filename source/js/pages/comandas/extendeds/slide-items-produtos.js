/* Requires */

const SLIDE_ITEMS_COMPONENT = require('../components/slide-items')

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class SlideItemsExtended extends SLIDE_ITEMS_COMPONENT {
  render () {
    let html = ''

    for (const index in this.__state.selected) {
      const item = this.__state.selected[index]

      html += `
      <div class="item">
        <div class="titulo">
          <p>${item.quantidade}x ${item.produto_nome}</p>
        </div>
        <div class="dados">
          <div class="preco">
            <p>Valor (UN)</p>
            <span>${FORMAT().money(item.valor_unitario)}</span>
          </div>
          <div class="desconto">
            <p>Desconto</p>
            <span>${FORMAT().money(item.valor_desconto)}</span>
          </div>
          <div class="subtotal">
            <p>Subtotal</p>
            <span>${FORMAT().money(item.valor_subtotal)}</span>
          </div>
        </div>
        <div class="botao">
          <button type="button" id="remover_item">Remover</button>
        </div>
      </div>
      `
    }

    this.__component.innerHTML = html
  }

  value () {
    return this.getState().selected.map((_item) => {
      delete _item.produto_nome
      return _item
    })
  }

  importItems (_produtos) {
    for (const index in _produtos) {
      const produto = _produtos[index]

      if (produto.id_produto) {
        produto.produto_nome = produto.id_produto.nome
        produto.id_produto = produto.id_produto._id
      }

      this.addItem(produto)
    }
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new SlideItemsExtended(_id)

    component.activeOnDrag()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
