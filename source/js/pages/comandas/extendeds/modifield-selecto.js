/* Requires */

const SELECTO_COMPONENT = require('../../../global/components/selecto')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class ModifiedSelecto extends SELECTO_COMPONENT {
  selectOption (_value) {
    const selectedDiv = this.__component.querySelector('.selected')

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      if (currentOption.value === _value) {
        selectedDiv.innerText = currentOption.text
        this.__state.selected = {
          value: currentOption.value,
          text: currentOption.text,
          price: currentOption.price
        }
        break
      }
    }
  }

  price () {
    return this.__state.selected.price
  }

  setOptionsWithEstados () {}
  setOptionsWithFormaPagamento () {}
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new ModifiedSelecto(_id)

    component.render()

    component.activeClickOnSelecto()
    component.activeHiddenOnLostFocus()
    component.activeClickOnOption()
    component.activeKeyupOnSearchbar()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
