/* Requires */

const COMANDAS_COMPONENT = require('../components/comandas')
const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_OPCOES = require('../modals/modal-opcoes')

/* Extended Class */

class ComandasExtended extends COMANDAS_COMPONENT {
  activeMenuOnClick () {
    document.addEventListener('click', (e) => {
      if (!e.target.matches(`${this.__id} .comanda__dia .dia__body .comanda`)) return

      const id = e.target.querySelector('.comanda__id').innerText
      const numero = e.target.querySelector('.comanda__numero').innerText
      const cliente = e.target.querySelector('.comanda__cliente').innerText
      const total = e.target.querySelector('.comanda__total').innerText
      const situacao = e.target.querySelector('.comanda__situacao').innerText

      const obj = { id, numero, cliente, total, situacao }

      MODAL_OPCOES().showModal()
      MODAL_OPCOES().importDataToModal(obj)
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_date) => {
    const component = new ComandasExtended('section.comandas')

    component.setDate(_date.year, _date.month)
    component.render()

    component.activeOnDrag()
    component.activeMenuOnClick()

    SYMBOL().subscribe('comandas_principal', component)
  }

  return methods
}

module.exports = Module
