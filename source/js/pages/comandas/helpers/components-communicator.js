/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const COMANDAS_REQUEST = require('../requests/comandas')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateComandas = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')

    await methods.getComandas(dateSelector.value())
  }

  methods.getComandas = async (_date) => {
    const date = `${_date.year}-${_date.month.toString().padStart(2, '0')}`
    const comandasComponent = SYMBOL().retrieve('comandas_principal')

    comandasComponent.setDate(_date.year, _date.month)
    try {
      const comandas = await COMANDAS_REQUEST().getComandas(date)
      comandasComponent.importComandas(comandas)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
