/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const USUARIOS_REQUEST = require('../requests/usuarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __funcionariosSelectoAdicionar = SYMBOL().retrieve('selecto_funcionarios_adicionar')
  const __funcionariosSelectoEditar = SYMBOL().retrieve('selecto_funcionarios_editar')
  const __funcionariosSelectoFaturar = SYMBOL().retrieve('selecto_funcionarios_faturar')

  // Methods

  methods.updateOptions = async () => {
    try {
      const funcionarios = await USUARIOS_REQUEST().getUsuarios()

      const options = funcionarios.map(_funcionario => {
        return { value: _funcionario._id, text: _funcionario.nome }
      })

      if (options.length === 0) {
        const html = `
        <p>Não há funcionários cadastrados ainda.</p>
        <a href="./funcionarios">
          <button type="button">Cadastrar Funcionário</button>
        </a>
        `
        __funcionariosSelectoAdicionar._alertEmpty(html)
        __funcionariosSelectoEditar._alertEmpty(html)
        __funcionariosSelectoFaturar._alertEmpty(html)
      }

      __funcionariosSelectoAdicionar.importOptions(options)
      __funcionariosSelectoEditar.importOptions(options)
      __funcionariosSelectoFaturar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
