/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SERVICOS_REQUEST = require('../requests/servicos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __servicosSelectoAdicionar = SYMBOL().retrieve('selecto_servicos_adicionar')
  const __servicosSelectoEditar = SYMBOL().retrieve('selecto_servicos_editar')

  // Methods

  methods.updateOptions = async () => {
    try {
      const servicos = await SERVICOS_REQUEST().getServicos()

      const options = servicos.map(_servico => {
        return {
          value: _servico._id,
          text: _servico.nome,
          price: _servico.valor
        }
      })

      if (options.length === 0) {
        const html = `
        <p>Não há serviços cadastrados ainda.</p>
        <a href="./servicos">
          <button type="button">Cadastrar Serviço</button>
        </a>
        `
        __servicosSelectoAdicionar._alertEmpty(html)
        __servicosSelectoEditar._alertEmpty(html)
      }

      __servicosSelectoAdicionar.importOptions(options)
      __servicosSelectoEditar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
