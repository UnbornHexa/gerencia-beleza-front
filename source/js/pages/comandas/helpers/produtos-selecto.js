/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const PRODUTOS_REQUEST = require('../requests/produtos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __produtosSelectoAdicionar = SYMBOL().retrieve('selecto_produtos_adicionar')
  const __produtosSelectoEditar = SYMBOL().retrieve('selecto_produtos_editar')

  // Methods

  methods.updateOptions = async () => {
    try {
      const produtos = await PRODUTOS_REQUEST().getProdutos()

      const options = produtos.map(_produto => {
        return {
          value: _produto._id,
          text: _produto.nome,
          price: _produto.valor
        }
      })

      if (options.length === 0) {
        const html = `
        <p>Não há produtos cadastrados ainda.</p>
        <a href="./produtos">
          <button type="button">Cadastrar Produto</button>
        </a>
        `
        __produtosSelectoAdicionar._alertEmpty(html)
        __produtosSelectoEditar._alertEmpty(html)
      }

      __produtosSelectoAdicionar.importOptions(options)
      __produtosSelectoEditar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
