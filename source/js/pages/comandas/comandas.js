/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_SELECTO_SERVICOS = require('./helpers/servicos-selecto')
const HELPER_SELECTO_PRODUTOS = require('./helpers/produtos-selecto')
const HELPER_SELECTO_CLIENTES = require('./helpers/clientes-selecto')
const HELPER_SELECTO_FUNCIONARIOS = require('./helpers/funcionarios-selecto')

const COMANDAS = require('./extendeds/comandas')
const DATE_SELECTOR = require('./extendeds/date-selector')
const HELPER_COMPONENTS_COMMUNICATOR = require('./helpers/components-communicator')
const SLIDE_ITEMS_SERVICOS = require('./extendeds/slide-items-servicos')
const SLIDE_ITEMS_PRODUTOS = require('./extendeds/slide-items-produtos')
const LIST_SELECTOS_PAGAMENTOS = require('./extendeds/list-selectos-pagamentos')
const LIST_SELECTOS_GORJETAS = require('./extendeds/list-selectos-gorjetas')
const SELECTO = require('./extendeds/selecto')
const MODIFIELD_SELECTO = require('./extendeds/modifield-selecto')
const SELECTO_FORMA_PAGAMENTO = require('./extendeds/selecto-forma-pagamento')
const DATEPICKER = require('./extendeds/datepicker')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('comandas')
  ASIDE_PICTURE().load()

  DATE_SELECTOR().init()
  COMANDAS().init(DATE_SELECTOR().getDate())

  SELECTO().init('section#modal_adicionar_comanda .informacoes__cliente .selecto__common', 'selecto_clientes_adicionar')
  SELECTO().init('section#modal_adicionar_comanda .agrupador__funcionario .selecto__common', 'selecto_funcionarios_adicionar')
  MODIFIELD_SELECTO().init('section#modal_adicionar_comanda .servico .selecto__common', 'selecto_servicos_adicionar')
  MODIFIELD_SELECTO().init('section#modal_adicionar_comanda .agrupador__produto .selecto__common', 'selecto_produtos_adicionar')
  SLIDE_ITEMS_SERVICOS().init('section#modal_adicionar_comanda .servicos .slide__items', 'slide_items_servicos_adicionar')
  SLIDE_ITEMS_PRODUTOS().init('section#modal_adicionar_comanda .produtos .slide__items', 'slide_items_produtos_adicionar')

  SELECTO().init('section#modal_editar_comanda .informacoes__cliente .selecto__common', 'selecto_clientes_editar')
  SELECTO().init('section#modal_editar_comanda .agrupador__funcionario .selecto__common', 'selecto_funcionarios_editar')
  MODIFIELD_SELECTO().init('section#modal_editar_comanda .servico .selecto__common', 'selecto_servicos_editar')
  MODIFIELD_SELECTO().init('section#modal_editar_comanda .agrupador__produto .selecto__common', 'selecto_produtos_editar')
  SLIDE_ITEMS_SERVICOS().init('section#modal_editar_comanda .servicos .slide__items', 'slide_items_servicos_editar')
  SLIDE_ITEMS_PRODUTOS().init('section#modal_editar_comanda .produtos .slide__items', 'slide_items_produtos_editar')

  SELECTO().init('section#modal_faturar_comanda .gorjetas__funcionario .selecto__common', 'selecto_funcionarios_faturar')
  SELECTO_FORMA_PAGAMENTO().init('section#modal_faturar_comanda .pagamentos__forma .selecto__common', 'selecto_formaPagamento_faturar')
  LIST_SELECTOS_PAGAMENTOS().init('section#modal_faturar_comanda .list__selectos__pagamentos', 'list_selectos_pagamentos_faturar')
  LIST_SELECTOS_GORJETAS().init('section#modal_faturar_comanda .list__selectos__gorjetas', 'list_selectos_gorjetas_faturar')

  DATEPICKER().init('section#modal_adicionar_comanda .informacoes__data .datepicker', 'datapicker_adicionar')
  DATEPICKER().init('section#modal_editar_comanda .informacoes__data .datepicker', 'datapicker_editar')

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  HELPER_COMPONENTS_COMMUNICATOR().updateComandas()
  HELPER_SELECTO_SERVICOS().updateOptions()
  HELPER_SELECTO_PRODUTOS().updateOptions()
  HELPER_SELECTO_CLIENTES().updateOptions()
  HELPER_SELECTO_FUNCIONARIOS().updateOptions()

  EVENTS().activeEvents()
}
start()
