class SlideItems {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      selected: []
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () { }

  _reset () {
    this.__component.innerHTML = ''
  }

  // Items

  addItem (_item) {
    this._reset()
    if (!_item) return

    this.getState().selected.push(_item)
    this.render()
  }

  delItem (_item) {
    const itemsNode = this.__component.querySelectorAll('.item')
    const indexOfElement = Array.from(itemsNode).indexOf(_item)

    this.getState().selected.splice(indexOfElement, 1)
    this.render()
  }

  // Utils

  value () {
    return this.getState().selected
  }

  resetSelected () {
    this.getState().selected = []
    this._reset()
  }

  calcTotal () {
    const sum = (accumulator, item) => item.valor_subtotal + accumulator
    return this.getState().selected.reduce(sum, 0)
  }

  // Events

  activeOnDrag () {
    let isDown = false
    let startX
    let scrollLeft

    this.__component.addEventListener('mousedown', (e) => {
      isDown = true

      startX = e.pageX - this.__component.offsetLeft
      scrollLeft = this.__component.scrollLeft
    })

    this.__component.addEventListener('mouseleave', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mouseup', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mousemove', (e) => {
      if (!isDown) return
      e.preventDefault()

      const x = e.pageX - this.__component.offsetLeft
      const walk = (x - startX) * 2
      this.__component.scrollLeft = scrollLeft - walk
      this.__component.classList.add('drag')
    })
  }
}

module.exports = SlideItems
