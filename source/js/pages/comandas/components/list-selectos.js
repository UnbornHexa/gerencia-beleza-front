class ListSelectos {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      selected: []
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () { }

  _reset () {
    this.__component.innerHTML = ''
  }

  // Items

  addItem (_item) {
    this._reset()
    if (!_item) return

    this.getState().selected.push(_item)
    this.render()
  }

  delItem (_item) {
    const itemsNode = this.__component.querySelectorAll('.item')
    const indexOfElement = Array.from(itemsNode).indexOf(_item)

    this.getState().selected.splice(indexOfElement, 1)
    this.render()
  }

  // Utils

  value () {
    return this.getState().selected
  }

  resetSelected () {
    this.getState().selected = []
    this._reset()
  }
}

module.exports = ListSelectos
