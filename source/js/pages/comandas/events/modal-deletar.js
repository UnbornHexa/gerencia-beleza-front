/* Requires */

const COMANDAS_REQUEST = require('../requests/comandas')
const MODAL_DELETAR = require('../modals/modal-deletar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_comanda .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_comanda .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idComanda = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().delComanda(idComanda)
        MODAL_DELETAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateComandas()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
