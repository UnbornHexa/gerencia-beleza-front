/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')

const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_FATURAR = require('../modals/modal-faturar')
const MODAL_OPCOES = require('../modals/modal-opcoes')

const COMANDAS_REQUEST = require('../requests/comandas')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_opcoes_comanda')

  const __idComandaDiv = __modal.querySelector('.content__comanda .comanda__id')
  const __numeroComandaDiv = __modal.querySelector('.content__comanda .comanda__numero')
  const __totalComandaDiv = __modal.querySelector('.content__comanda .comanda__total')
  const __statusComandaDiv = __modal.querySelector('.content__comanda .comanda__status')

  const __deletarOption = __modal.querySelector('.content__opcoes .opcao__deletar')
  const __editarOption = __modal.querySelector('.content__opcoes .opcao__editar')
  const __visualizarOption = __modal.querySelector('.content__opcoes .opcao__visualizar')
  const __faturarOption = __modal.querySelector('.content__opcoes .opcao__faturar')

  // Methods

  methods.activeClickOnMenuDeletar = () => {
    __deletarOption.addEventListener('click', () => {
      const statusComanda = __statusComandaDiv.innerText
      const idComanda = __idComandaDiv.innerText
      const numeroComanda = __numeroComandaDiv.innerText

      if (statusComanda === 'Faturada') {
        MESSAGEBOX().alert('Não é possível deletar uma comanda faturada.')
        return
      }

      MODAL_OPCOES().hideModal()
      MODAL_DELETAR().showModal()

      const inputIdModal = document.querySelector('section#modal_deletar_comanda .id input')
      const headerDataModal = document.querySelector('section#modal_deletar_comanda .modal__header p')

      inputIdModal.value = idComanda
      headerDataModal.innerText = `${numeroComanda}`
    })
  }

  methods.activeClickOnMenuVisualizar = () => {
    __visualizarOption.addEventListener('click', async () => {
      const idComanda = __idComandaDiv.innerText

      MODAL_OPCOES().hideModal()
      MODAL_VISUALIZAR().showModal()

      try {
        const comanda = await COMANDAS_REQUEST().getComandaById(idComanda)
        MODAL_VISUALIZAR().importDataToModal(comanda)
      } catch (e) { console.log(e) }
    })
  }

  methods.activeClickOnMenuEditar = () => {
    __editarOption.addEventListener('click', async () => {
      const statusComanda = __statusComandaDiv.innerText
      const idComanda = __idComandaDiv.innerText

      if (statusComanda === 'Faturada') {
        MESSAGEBOX().alert('Não é possível editar uma comanda faturada.')
        return
      }

      MODAL_OPCOES().hideModal()
      MODAL_EDITAR().showModal()

      try {
        const comanda = await COMANDAS_REQUEST().getComandaById(idComanda)
        MODAL_EDITAR().importDataToModal(comanda)
      } catch (e) { console.log(e) }
    })
  }

  methods.activeClickOnMenuFaturar = () => {
    __faturarOption.addEventListener('click', () => {
      const statusComanda = __statusComandaDiv.innerText
      const idComanda = __idComandaDiv.innerText
      const totalComanda = __totalComandaDiv.innerText

      if (statusComanda === 'Faturada') {
        MESSAGEBOX().alert('Não é possível faturar uma comanda faturada.')
        return
      }

      MODAL_OPCOES().hideModal()
      MODAL_FATURAR().showModal()

      const inputIdModal = document.querySelector('section#modal_faturar_comanda .pagamentos__id input')
      const inputTotalModal = document.querySelector('section#modal_faturar_comanda .pagamentos__total input')
      const labelTotalModal = document.querySelector('section#modal_faturar_comanda .total p span')

      inputIdModal.value = idComanda
      inputTotalModal.value = totalComanda
      labelTotalModal.innerText = totalComanda
    })
  }

  return methods
}

module.exports = Module
