/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const SECTION_MAIN = require('./section_main')
const MODAL_ADICIONAR = require('./modal-adicionar')
const MODAL_EDITAR = require('./modal-editar')
const MODAL_DELETAR = require('./modal-deletar')
const MODAL_FATURAR = require('./modal-faturar')
const MODAL_OPCOES = require('./modal-opcoes')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    SECTION_MAIN().activeClickOnAdicionarButton()

    MODAL_ADICIONAR().activeClickOnAdicionarButton()
    MODAL_ADICIONAR().activeClickOnAdicionarServicoButton()
    MODAL_ADICIONAR().activeClickOnAdicionarProdutoButton()
    MODAL_ADICIONAR().activeClickOnRemoverServicoButton()
    MODAL_ADICIONAR().activeClickOnRemoverProdutoButton()

    MODAL_EDITAR().activeClickOnEditarButton()
    MODAL_EDITAR().activeClickOnAdicionarServicoButton()
    MODAL_EDITAR().activeClickOnAdicionarProdutoButton()
    MODAL_EDITAR().activeClickOnRemoverServicoButton()
    MODAL_EDITAR().activeClickOnRemoverProdutoButton()
    MODAL_EDITAR().activeChangeOnModal()

    MODAL_DELETAR().activeClickOnDeletarButton()

    MODAL_FATURAR().activeClickOnFaturarButton()
    MODAL_FATURAR().activeClickOnAdicionarPagamentoButton()
    MODAL_FATURAR().activeClickOnAdicionarGorjetaButton()
    MODAL_FATURAR().activeClickOnRemoverPagamentoButton()
    MODAL_FATURAR().activeClickOnRemoverGorjetaButton()

    MODAL_OPCOES().activeClickOnMenuDeletar()
    MODAL_OPCOES().activeClickOnMenuVisualizar()
    MODAL_OPCOES().activeClickOnMenuEditar()
    MODAL_OPCOES().activeClickOnMenuFaturar()

    TOGGLE().activeClickOnToggle()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
