/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_FATURAR = require('../modals/modal-faturar')
const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

const COMANDAS_REQUEST = require('../requests/comandas')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_faturar_comanda')

  const __idInput = __modal.querySelector('.pagamentos__id input')

  const __adicionarPagamentoButton = __modal.querySelector('.botao #adicionar__pagamento')
  const __adicionarGorjetaButton = __modal.querySelector('.botao #adicionar__gorjeta')
  const __faturarButton = __modal.querySelector('#faturar')

  const __listSelectosPagamento = SYMBOL().retrieve('list_selectos_pagamentos_faturar')
  const __listSelectosGorjetas = SYMBOL().retrieve('list_selectos_gorjetas_faturar')

  // Methods

  methods.activeClickOnFaturarButton = () => {
    __faturarButton.addEventListener('click', async () => {
      const modalData = MODAL_FATURAR().exportDataFromModal()
      const idComanda = __idInput.value

      const modalOk = MODAL_FATURAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __faturarButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().faturarComanda(idComanda, modalData)
        MODAL_FATURAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateComandas()
      } catch (e) { console.log(e) }

      __faturarButton.removeAttribute('disabled')
    })
  }

  // Items

  methods.activeClickOnAdicionarPagamentoButton = () => {
    __adicionarPagamentoButton.addEventListener('click', () => {
      const newPagamentoData = MODAL_FATURAR().exportNewPagamentoToListSelectos()

      const newPagamentoOk = MODAL_FATURAR().checkRequiredNewPagamentoFields()
      if (newPagamentoOk === false) return

      __listSelectosPagamento.addItem(newPagamentoData)
      MODAL_FATURAR().resetNewPagamentoFields()
      MODAL_FATURAR().calcTotal()
    })
  }

  methods.activeClickOnAdicionarGorjetaButton = () => {
    __adicionarGorjetaButton.addEventListener('click', () => {
      const newGorjetaData = MODAL_FATURAR().exportNewGorjetaToListSelectos()

      const newGorjetaOk = MODAL_FATURAR().checkRequiredNewGorjetasFields()
      if (newGorjetaOk === false) return

      __listSelectosGorjetas.addItem(newGorjetaData)
      MODAL_FATURAR().resetNewGorjetaFields()
      MODAL_FATURAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverPagamentoButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_faturar_comanda .item i.remove')) return
      const item = e.target.parentElement

      __listSelectosPagamento.delItem(item)
      MODAL_FATURAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverGorjetaButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_faturar_comanda .item i.remove')) return
      const item = e.target.parentElement

      __listSelectosGorjetas.delItem(item)
      MODAL_FATURAR().calcTotal()
    })
  }

  return methods
}

module.exports = Module
