/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const COMANDAS_REQUEST = require('../requests/comandas')
const MODAL_EDITAR = require('../modals/modal-editar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_comanda')

  const __idInput = __modal.querySelector('.informacoes__id input')

  const __editarButton = __modal.querySelector('#editar')
  const __adicionarServicoButton = __modal.querySelector('#adicionar__servico')
  const __adicionarProdutoButton = __modal.querySelector('#adicionar__produto')

  const __servicosSlideItems = SYMBOL().retrieve('slide_items_servicos_editar')
  const __produtosSlideItems = SYMBOL().retrieve('slide_items_produtos_editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idComanda = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().editComanda(idComanda, modalData)
        MODAL_EDITAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateComandas()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Items

  methods.activeClickOnAdicionarServicoButton = () => {
    __adicionarServicoButton.addEventListener('click', () => {
      const newServicoData = MODAL_EDITAR().exportNewServicoToSlide()

      const newServicoOk = MODAL_EDITAR().checkRequiredNewServicoFields()
      if (newServicoOk === false) return

      __servicosSlideItems.addItem(newServicoData)
      MODAL_EDITAR().resetNewServicoFields()
      MODAL_EDITAR().calcTotal()
    })
  }

  methods.activeClickOnAdicionarProdutoButton = () => {
    __adicionarProdutoButton.addEventListener('click', () => {
      const newProdutoData = MODAL_EDITAR().exportNewProdutoToSlide()

      const newProdutoOk = MODAL_EDITAR().checkRequiredNewProdutoFields()
      if (newProdutoOk === false) return

      __produtosSlideItems.addItem(newProdutoData)
      MODAL_EDITAR().resetNewProdutoFields()
      MODAL_EDITAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverServicoButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_comanda .servicos .item .botao #remover_item')) return
      const item = e.target.parentElement.parentElement

      __servicosSlideItems.delItem(item)
      MODAL_EDITAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverProdutoButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_comanda .produtos .item .botao #remover_item')) return
      const item = e.target.parentElement.parentElement

      __produtosSlideItems.delItem(item)
      MODAL_EDITAR().calcTotal()
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_comanda input')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_comanda .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.closest('section#modal_editar_comanda .slide__items')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.closest('section#modal_editar_comanda .datepicker')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
