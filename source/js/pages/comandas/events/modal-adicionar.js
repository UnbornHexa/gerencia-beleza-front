/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const COMANDAS_REQUEST = require('../requests/comandas')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_comanda')

  const __adicionarButton = __modal.querySelector('.modal__content .modal__button #adicionar')
  const __adicionarServicoButton = __modal.querySelector('.botao #adicionar__servico')
  const __adicionarProdutoButton = __modal.querySelector('.botao #adicionar__produto')

  const __servicosSlideItems = SYMBOL().retrieve('slide_items_servicos_adicionar')
  const __produtosSlideItems = SYMBOL().retrieve('slide_items_produtos_adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().addComanda(modalData)
        MODAL_ADICIONAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateComandas()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  // Items

  methods.activeClickOnAdicionarServicoButton = () => {
    __adicionarServicoButton.addEventListener('click', () => {
      const newServicoData = MODAL_ADICIONAR().exportNewServicoToSlide()

      const newServicoOk = MODAL_ADICIONAR().checkRequiredNewServicoFields()
      if (newServicoOk === false) return

      __servicosSlideItems.addItem(newServicoData)
      MODAL_ADICIONAR().resetNewServicoFields()
      MODAL_ADICIONAR().calcTotal()
    })
  }

  methods.activeClickOnAdicionarProdutoButton = () => {
    __adicionarProdutoButton.addEventListener('click', () => {
      const newProdutoData = MODAL_ADICIONAR().exportNewProdutoToSlide()

      const newProdutoOk = MODAL_ADICIONAR().checkRequiredNewProdutoFields()
      if (newProdutoOk === false) return

      __produtosSlideItems.addItem(newProdutoData)
      MODAL_ADICIONAR().resetNewProdutoFields()
      MODAL_ADICIONAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverServicoButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_adicionar_comanda .servicos .item .botao #remover_item')) return
      const item = e.target.parentElement.parentElement

      __servicosSlideItems.delItem(item)
      MODAL_ADICIONAR().calcTotal()
    })
  }

  methods.activeClickOnRemoverProdutoButton = () => {
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_adicionar_comanda .produtos .item .botao #remover_item')) return
      const item = e.target.parentElement.parentElement

      __produtosSlideItems.delItem(item)
      MODAL_ADICIONAR().calcTotal()
    })
  }

  return methods
}

module.exports = Module
