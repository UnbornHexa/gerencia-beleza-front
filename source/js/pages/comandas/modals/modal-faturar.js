/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const FORMAT = require('../../../global/helpers/format')
const MESSAGEBOX = require('../../../global/modules/messagebox')

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_faturar_comanda')

  const __idInput = __modal.querySelector('.pagamentos__id input')
  const __totalInput = __modal.querySelector('.pagamentos__total input')
  const __valorPagamentoInput = __modal.querySelector('.pagamentos__valor input')
  const __valorGorjetaInput = __modal.querySelector('.gorjetas__valor input')

  const __totalLabel = __modal.querySelector('.total .total__comanda span')

  const __metodoPagamentoSelecto = SYMBOL().retrieve('selecto_formaPagamento_faturar')
  const __funcionariosSelecto = SYMBOL().retrieve('selecto_funcionarios_faturar')

  const __listSelectosPagamento = SYMBOL().retrieve('list_selectos_pagamentos_faturar')
  const __listSelectosGorjetas = SYMBOL().retrieve('list_selectos_gorjetas_faturar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__totalLabel.classList.contains('quitado')) return true

    MESSAGEBOX().alert('Você precisa quitar a comanda para faturá-la. Adicione os métodos de pagamento')
    return false
  }

  methods.resetForm = () => {
    const total = __totalLabel.parentElement.parentElement
    total.classList.remove('paga')

    __idInput.value = ''
    __valorPagamentoInput.value = ''
    __valorGorjetaInput.value = ''
    __totalLabel.innerText = 'R$ 0,00'

    __metodoPagamentoSelecto.resetSelected()
    __funcionariosSelecto.resetSelected()

    __listSelectosPagamento.resetSelected()
    __listSelectosGorjetas.resetSelected()

    __totalLabel.classList.remove('quitado')
  }

  // Data

  methods.exportDataFromModal = () => {
    const comanda = {}
    comanda.pagamentos = __listSelectosPagamento.value()
    comanda.gorjetas = __listSelectosGorjetas.value()

    return comanda
  }

  // List Selectos

  methods.resetNewPagamentoFields = () => {
    __metodoPagamentoSelecto.resetSelected()
    __valorPagamentoInput.value = ''
  }

  methods.resetNewGorjetaFields = () => {
    __funcionariosSelecto.resetSelected()
    __valorGorjetaInput.value = ''
  }

  methods.checkRequiredNewPagamentoFields = () => {
    if (__valorPagamentoInput.value && !__metodoPagamentoSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos acima para adicionar um pagamento à comanda.')
    return false
  }

  methods.checkRequiredNewGorjetasFields = () => {
    if (__valorGorjetaInput.value && !__funcionariosSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos acima para adicionar uma gorjeta ao funcionário.')
    return false
  }

  methods.exportNewPagamentoToListSelectos = () => {
    const pagamento = {}

    pagamento.metodo = __metodoPagamentoSelecto.value()
    pagamento.valor = FORMAT().unformatMoney(__valorPagamentoInput.value)

    return pagamento
  }

  methods.exportNewGorjetaToListSelectos = () => {
    const gorjeta = {}

    gorjeta.id_usuario = __funcionariosSelecto.value()
    gorjeta.funcionario_nome = __funcionariosSelecto.text()
    gorjeta.valor_gorjeta = FORMAT().unformatMoney(__valorGorjetaInput.value)

    return gorjeta
  }

  methods.calcTotal = () => {
    const totalComanda = FORMAT().unformatMoney(__totalInput.value)
    const totalGorjetas = __listSelectosGorjetas.calcTotal()
    const totalPagamentos = __listSelectosPagamento.calcTotal()

    const totalGeral = totalComanda + totalGorjetas - totalPagamentos

    __totalLabel.innerText = FORMAT().money(totalGeral)

    if (totalGeral === 0) { __totalLabel.classList.add('quitado') } else { __totalLabel.classList.remove('quitado') }
  }

  return methods
}

module.exports = Module
