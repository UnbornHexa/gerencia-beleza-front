/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_comanda')

  const __clienteCollapsible = __modal.querySelector('.collapsible .cliente span')
  const __totalCollapsible = __modal.querySelector('.collapsible .total span')
  const __dataCollapsible = __modal.querySelector('.collapsible .data span')

  const __servicosCollapsible = __modal.querySelector('.collapsible .servicos span')
  const __produtosCollapsible = __modal.querySelector('.collapsible .produtos span')
  const __pagamentosCollapsible = __modal.querySelector('.collapsible .pagamentos span')
  const __gorjetasCollapsible = __modal.querySelector('.collapsible .gorjetas span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __clienteCollapsible.innerHTML = '...'
    __totalCollapsible.innerHTML = '...'
    __dataCollapsible.innerHTML = '...'

    __servicosCollapsible.innerHTML = '...'
    __produtosCollapsible.innerHTML = '...'
    __pagamentosCollapsible.innerHTML = '...'
    __gorjetasCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    const cliente = (_data.id_cliente === null) ? 'Cliente deletado' : _data.id_cliente.nome
    const servicos = _data.itens.servicos.map(_mapServicos).join('<br>')
    const produtos = _data.itens.produtos.map(_mapProdutos).join('<br>')
    const pagamentos = _data.pagamento.map(_mapPagamentos).join('<br>')
    const gorjetas = _data.gorjetas.map(_mapGorjetas).join('<br>')

    __clienteCollapsible.innerText = cliente || '...'
    __totalCollapsible.innerText = FORMAT().money(_data.valor_total)
    __dataCollapsible.innerText = CONVERT().dateBR(_data.data)

    __servicosCollapsible.innerHTML = servicos || '...'
    __produtosCollapsible.innerHTML = produtos || '...'
    __pagamentosCollapsible.innerHTML = pagamentos || '...'
    __gorjetasCollapsible.innerHTML = gorjetas || '...'
  }

  // Map Function

  function _mapServicos (_item) {
    const quantidade = _item.quantidade
    const nome = _item.id_servico.nome
    const funcionario = _item.id_usuario.nome

    return `${quantidade}x ${nome} (${funcionario})`
  }

  function _mapProdutos (_item) {
    const quantidade = _item.quantidade
    const nome = _item.id_produto.nome

    return `${quantidade}x ${nome}`
  }

  function _mapPagamentos (_item) {
    let metodo = ''
    if (_item.metodo === 'dinheiro') metodo = 'Dinheiro'
    if (_item.metodo === 'debito') metodo = 'Débito'
    if (_item.metodo === 'credito') metodo = 'Crédito'

    const valor = FORMAT().money(_item.valor)
    const parcelas = (_item.parcelas) ? `${_item.parcelas}x ` : ''

    return `[${metodo}] ${parcelas}${valor}`
  }

  function _mapGorjetas (_item) {
    const funcionario = _item.id_usuario.nome
    const valor = FORMAT().money(_item.valor_gorjeta)

    return `${valor} (${funcionario})`
  }

  return methods
}

module.exports = Module
