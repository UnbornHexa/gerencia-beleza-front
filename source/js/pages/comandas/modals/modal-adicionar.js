/* Requires */

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')
const DATE = require('../../../global/helpers/date')
const MESSAGEBOX = require('../../../global/modules/messagebox')

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_comanda')

  const __dataDatepicker = SYMBOL().retrieve('datapicker_adicionar')

  const __quantidadeServicoInput = __modal.querySelector('.agrupador__quantidade__servicos input')
  const __quantidadeProdutoInput = __modal.querySelector('.agrupador__quantidade__produtos input')

  const __totalComandaLabel = __modal.querySelector('.totais .totais__comanda span')
  const __totalServicosLabel = __modal.querySelector('.totais .totais__servicos span')
  const __totalProdutosLabel = __modal.querySelector('.totais .totais__produtos span')

  const __clientesSelecto = SYMBOL().retrieve('selecto_clientes_adicionar')
  const __servicosSelecto = SYMBOL().retrieve('selecto_servicos_adicionar')
  const __funcionariosSelecto = SYMBOL().retrieve('selecto_funcionarios_adicionar')
  const __produtosSelecto = SYMBOL().retrieve('selecto_produtos_adicionar')

  const __servicosSlide = SYMBOL().retrieve('slide_items_servicos_adicionar')
  const __produtosSlide = SYMBOL().retrieve('slide_items_produtos_adicionar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (!__clientesSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __quantidadeServicoInput.value = ''
    // __descontoServicoInput.value = ''
    __quantidadeProdutoInput.value = ''
    // __descontoProdutoInput.value = ''

    __dataDatepicker.clear()
    __dataDatepicker.selectDate(DATE().today())

    __clientesSelecto.resetSelected()
    __servicosSelecto.resetSelected()
    __funcionariosSelecto.resetSelected()
    __produtosSelecto.resetSelected()

    __totalComandaLabel.innerText = 'R$ 0,00'
    __totalServicosLabel.innerText = 'R$ 0,00'
    __totalProdutosLabel.innerText = 'R$ 0,00'

    __servicosSlide.resetSelected()
    __produtosSlide.resetSelected()
  }

  // Data

  methods.exportDataFromModal = () => {
    const comanda = {}
    comanda.itens = {}

    comanda.data = __dataDatepicker.value()
    comanda.id_cliente = __clientesSelecto.value()
    comanda.itens.servicos = __servicosSlide.value()
    comanda.itens.produtos = __produtosSlide.value()

    return comanda
  }

  /* Slides Items */

  // Form

  methods.checkRequiredNewServicoFields = () => {
    if (__quantidadeServicoInput.value && !__servicosSelecto.isEmpty() && !__funcionariosSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos acima para adicionar um serviço à comanda.')
    return false
  }

  methods.checkRequiredNewProdutoFields = () => {
    if (__quantidadeProdutoInput.value && !__produtosSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos acima para adicionar um produto à comanda.')
    return false
  }

  methods.resetNewServicoFields = () => {
    __servicosSelecto.resetSelected()
    __funcionariosSelecto.resetSelected()
    __quantidadeServicoInput.value = ''
  }

  methods.resetNewProdutoFields = () => {
    __produtosSelecto.resetSelected()
    __quantidadeProdutoInput.value = ''
  }

  // Data

  methods.exportNewServicoToSlide = () => {
    const servico = {}

    servico.quantidade = Number(__quantidadeServicoInput.value) || 0
    servico.valor_unitario = __servicosSelecto.price() || 0
    servico.valor_desconto = 0
    servico.id_servico = __servicosSelecto.value() || ''
    servico.id_usuario = __funcionariosSelecto.value() || ''

    servico.funcionario_nome = __funcionariosSelecto.text() || ''
    servico.servico_nome = __servicosSelecto.text() || ''

    servico.valor_subtotal = (servico.quantidade * servico.valor_unitario) - servico.valor_desconto

    return servico
  }

  methods.exportNewProdutoToSlide = () => {
    const produto = {}

    produto.quantidade = Number(__quantidadeProdutoInput.value) || 0
    produto.valor_unitario = __produtosSelecto.price() || 0
    produto.valor_desconto = 0
    produto.id_produto = __produtosSelecto.value() || ''

    produto.produto_nome = __produtosSelecto.text() || ''

    produto.valor_subtotal = (produto.quantidade * produto.valor_unitario) - produto.valor_desconto

    return produto
  }

  /* Label Totals */

  methods.importTotalToLabels = (_servicoTotal, _produtoTotal) => {
    const comandaTotal = Number(_servicoTotal) + Number(_produtoTotal)

    __totalServicosLabel.innerText = FORMAT().money(_servicoTotal)
    __totalProdutosLabel.innerText = FORMAT().money(_produtoTotal)
    __totalComandaLabel.innerText = FORMAT().money(comandaTotal)
  }

  methods.calcTotal = () => {
    const totalServicos = __servicosSlide.calcTotal()
    const totalProdutos = __produtosSlide.calcTotal()

    methods.importTotalToLabels(totalServicos, totalProdutos)
  }

  return methods
}

module.exports = Module
