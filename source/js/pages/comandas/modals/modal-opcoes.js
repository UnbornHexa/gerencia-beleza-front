/* Requires */

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_opcoes_comanda')

  const __idDiv = __modal.querySelector('.comanda .comanda__id')
  const __numeroDiv = __modal.querySelector('.comanda .comanda__numero')
  const __clienteDiv = __modal.querySelector('.comanda .comanda__cliente')
  const __totalDiv = __modal.querySelector('.comanda .comanda__total')
  const __situacaoDiv = __modal.querySelector('.comanda .comanda__status')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __idDiv.innerHTML = '...'
    __numeroDiv.innerHTML = '...'
    __clienteDiv.innerHTML = '...'
    __totalDiv.innerHTML = '...'
    __situacaoDiv.innerHTML = '...'

    __situacaoDiv.classList.remove('orange')
    __situacaoDiv.classList.remove('green')
    __situacaoDiv.classList.remove('red')
  }

  // Data

  methods.importDataToModal = (_data) => {
    const classe = (_data.situacao === 'Faturada') ? 'green' : 'red'
    __situacaoDiv.classList.add(classe)

    __idDiv.innerText = _data.id
    __numeroDiv.innerText = _data.numero
    __clienteDiv.innerText = _data.cliente
    __totalDiv.innerText = _data.total
    __situacaoDiv.innerText = _data.situacao
  }

  return methods
}

module.exports = Module
