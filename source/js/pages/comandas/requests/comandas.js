/* Requires */

const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __url = `${HOSTS().url.api_plataforma}/comandas`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getComandas = async (_date) => {
    const url = `${__url}/${__idEmpresa}?data=${_date}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          _response.body.sort(sortComandasByStatus)
          _response.body.sort(sortComandasByNumber)
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getComandaById = async (_idComanda) => {
    const url = `${__url}/${__idEmpresa}/${_idComanda}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.addComanda = async (_data) => {
    const url = `${__url}/${__idEmpresa}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, __headersWithToken, _response => {
        if (_response.status === 201) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editComanda = async (_idComanda, _data) => {
    const url = `${__url}/${__idEmpresa}/${_idComanda}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PUT', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.delComanda = async (_idComanda) => {
    const url = `${__url}/${__idEmpresa}/${_idComanda}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'DELETE', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.faturarComanda = async (_idComanda, _data) => {
    const url = `${__url}/faturar/${__idEmpresa}/${_idComanda}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PATCH', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  // Sort Functions

  const sortComandasByStatus = (a, b) => {
    if (a.is_faturado === false && b.is_faturado === true) return -1
  }

  const sortComandasByNumber = (a, b) => {
    if (Number(a.nome.replace('#', '')) > Number(b.nome.replace('#', ''))) return -1
  }

  return methods
}

module.exports = Module
