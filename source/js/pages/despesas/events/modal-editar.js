/* Requires */

const DESPESAS_REQUEST = require('../requests/despesas')
const MODAL_EDITAR = require('../modals/modal-editar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_despesa .id input')
  const __editarButton = document.querySelector('section#modal_editar_despesa .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const idDespesa = __idInput.value
      const modalData = MODAL_EDITAR().exportDataFromModal()

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await DESPESAS_REQUEST().editDespesa(idDespesa, modalData)
        MODAL_EDITAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateValues()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_despesa input') &&
        !e.target.matches('section#modal_editar_despesa textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_despesa .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_despesa .datepicker input')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
