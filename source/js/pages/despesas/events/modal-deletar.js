/* Requires */

const DESPESAS_REQUEST = require('../requests/despesas')
const MODAL_DELETAR = require('../modals/modal-deletar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_despesa .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_despesa .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idDespesa = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await DESPESAS_REQUEST().delDespesa(idDespesa)
        MODAL_DELETAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateValues()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
