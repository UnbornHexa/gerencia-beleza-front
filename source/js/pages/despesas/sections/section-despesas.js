/* Requires */

const FORMAT = require('../../../global/helpers/format')
const SECTION_GRAFICO = require('./section-grafico')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __despesasSection = document.querySelector('section.financeiro__grafico')

  const __despesaTotalSpan = __despesasSection.querySelector('.grafico__complemento .valor span#despesa__mes')
  const __despesaDinheiroSpan = __despesasSection.querySelector('.info .info__dinheiro span#despesa_dinheiro')
  const __despesaDebitoSpan = __despesasSection.querySelector('.info .info__debito span#despesa_debito')
  const __despesaCreditoSpan = __despesasSection.querySelector('.info .info__credito span#despesa_credito')

  let __dinheiro = 0
  let __debito = 0
  let __credito = 0
  let __total = 0

  // Methods

  methods.load = (_despesas) => {
    const despesas = methods._calcDespesas(_despesas)

    __despesaDinheiroSpan.innerText = FORMAT().money(despesas.dinheiro)
    __despesaDebitoSpan.innerText = FORMAT().money(despesas.debito)
    __despesaCreditoSpan.innerText = FORMAT().money(despesas.credito)
    __despesaTotalSpan.innerText = FORMAT().money(despesas.total)

    SECTION_GRAFICO().build(despesas.dinheiro, despesas.debito, despesas.credito)

    return despesas.total
  }

  methods._calcDespesas = (_despesas) => {
    for (const index in _despesas) {
      const formaPagamento = _despesas[index]._id
      const valor = _despesas[index].valor

      if (formaPagamento === 'dinheiro') __dinheiro = valor
      else if (formaPagamento === 'debito') __debito = valor
      else if (formaPagamento === 'credito') __credito = valor
    }
    __total = __dinheiro + __debito + __credito

    return {
      dinheiro: __dinheiro,
      debito: __debito,
      credito: __credito,
      total: __total
    }
  }

  return methods
}

module.exports = Module
