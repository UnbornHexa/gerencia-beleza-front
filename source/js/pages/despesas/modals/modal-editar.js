/* Requires */

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_despesa')

  const __idInput = __modal.querySelector('.id input')
  const __nomeInput = __modal.querySelector('.nome input')
  const __dataDatepicker = SYMBOL().retrieve('datapicker_editar')
  const __valorInput = __modal.querySelector('.valor input')

  const __formaPagamentoSelecto = SYMBOL().retrieve('selecto_formaPagamento_editar')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value && __dataDatepicker.value() && __valorInput.value &&
      !__formaPagamentoSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __nomeInput.value = ''
    __dataDatepicker.clear()
    __valorInput.value = ''
    __formaPagamentoSelecto.resetSelected()

    __editarButton.setAttribute('disabled', true)
  }

  // Data

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __nomeInput.value = _data.nome || ''
    __dataDatepicker.selectDate(_data.data_vencimento || '')
    __valorInput.value = FORMAT().money(_data.valor) || ''
    __formaPagamentoSelecto.selectOption(_data.forma_pagamento)
  }

  methods.exportDataFromModal = () => {
    const despesa = {}

    despesa.nome = __nomeInput.value || ''
    despesa.data_vencimento = __dataDatepicker.value() || ''
    despesa.forma_pagamento = __formaPagamentoSelecto.value() || ''
    despesa.valor = FORMAT().unformatMoney(__valorInput.value) || 0

    return despesa
  }

  return methods
}

module.exports = Module
