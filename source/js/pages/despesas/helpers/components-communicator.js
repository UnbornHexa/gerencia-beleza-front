/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SECTION_DESPESAS = require('../sections/section-despesas')

const DESPESAS_REQUEST = require('../requests/despesas')
const RESUMO_REQUEST = require('../requests/resumo')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateValues = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const date = dateSelector.value()
    const yearAndMonth = `${date.year}-${date.month.toString().padStart(2, '0')}`

    await methods._updateDespesas(yearAndMonth)
    await methods._updateResumo(yearAndMonth)
  }

  methods._updateDespesas = async (_yearAndMonth) => {
    const listaDespesas = SYMBOL().retrieve('lista_despesas')

    try {
      const despesas = await DESPESAS_REQUEST().getDespesas(_yearAndMonth)
      listaDespesas.importItems(despesas)
    } catch (e) { console.log(e) }
  }

  methods._updateResumo = async (_yearAndMonth) => {
    try {
      const resumo = await RESUMO_REQUEST().getResumoFinanceiro(_yearAndMonth)
      SECTION_DESPESAS().load(resumo.despesas)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
