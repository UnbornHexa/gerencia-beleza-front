/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')
const FORMAT = require('../../../global/helpers/format')
const CONVERT = require('../../../global/helpers/convert')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const DESPESAS_REQUEST = require('../requests/despesas')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_despesa) {
    const itemsDiv = this.__component.querySelector('.lista__itens')
    const formaPagamento = (_despesa.forma_pagamento === 'dinheiro') ? 'dinheiro'
      : (_despesa.forma_pagamento === 'debito') ? 'débito' : 'crédito'

    const li =
      `
    <li>
      <span class="item__id" hidden>${_despesa._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-chart-line-down"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_despesa.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__valor">${FORMAT().money(_despesa.valor)} - em ${formaPagamento}</span>
        </div>
      </div>
      <div class="lista__data">
        <span class="item__data__vencimento">pago em ${CONVERT().dateBR(_despesa.data_vencimento)}</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idDespesa = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const despesa = await DESPESAS_REQUEST().getDespesaById(idDespesa)
        MODAL_VISUALIZAR().importDataToModal(despesa)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idDespesa = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const despesa = await DESPESAS_REQUEST().getDespesaById(idDespesa)
        MODAL_EDITAR().importDataToModal(despesa)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idDespesa = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeDespesa = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_despesa .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_despesa .modal__header p')

      inputIdModal.value = idDespesa
      headerNomeModal.innerText = nomeDespesa
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__despesas')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_despesas', component)
  }

  return methods
}

module.exports = Module
