/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_COMPONENTS_COMMUNICATOR = require('./helpers/components-communicator')

const DATE_SELECTOR = require('./extendeds/date-selector')
const LISTA_DESPESAS = require('./extendeds/lista-despesas')
const SELECTO_FORMA_PAGAMENTO = require('./extendeds/selecto-forma-pagamento')
const DATEPICKER = require('./extendeds/datepicker')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('financeiro')
  ASIDE_PICTURE().load()

  DATE_SELECTOR().init()
  LISTA_DESPESAS().init()
  SELECTO_FORMA_PAGAMENTO().init('section#modal_adicionar_despesa .metodo .selecto__common', 'selecto_formaPagamento_adicionar')
  SELECTO_FORMA_PAGAMENTO().init('section#modal_editar_despesa .metodo .selecto__common', 'selecto_formaPagamento_editar')
  DATEPICKER().init('section#modal_adicionar_despesa .datepicker', 'datapicker_adicionar')
  DATEPICKER().init('section#modal_editar_despesa .datepicker', 'datapicker_editar')

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  HELPER_COMPONENTS_COMMUNICATOR().updateValues()

  EVENTS().activeEvents()
}
start()
