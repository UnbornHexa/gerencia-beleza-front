/* Requires */

const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __idUsuario = TOKEN().getIdUsuarioFromToken()
  const __url = `${HOSTS().url.api_plataforma}/mensagens`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getNotificacoes = async () => {
    const url = `${__url}/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getNotificacaoById = async (_idMensagem) => {
    const url = `${__url}/${__idEmpresa}/${__idUsuario}/${_idMensagem}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
