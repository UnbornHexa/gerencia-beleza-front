/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_notificacoes')
  const __tituloH2 = __modal.querySelector('h2.titulo__notificacao')
  const __descricaoP = __modal.querySelector('p.descricao__notificacao')
  const __dataP = __modal.querySelector('p.data__notificacao')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __tituloH2.innerHTML = ''
    __descricaoP.innerHTML = ''
    __dataP.innerHTML = ''
  }

  // Data

  methods.importDataToModal = (_data) => {
    __tituloH2.innerHTML = _data.titulo
    __descricaoP.innerHTML = _data.mensagem
    __dataP.innerText = CONVERT().dateBR(_data.data_registro)
  }

  return methods
}

module.exports = Module
