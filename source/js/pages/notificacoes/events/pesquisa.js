/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaNotificacoes = SYMBOL().retrieve('lista_notificacoes')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaNotificacoes.getState().filteredItems = __listaNotificacoes.getState().items
          __listaNotificacoes._loadItemsFromPage(1)
          return
        }

        __listaNotificacoes.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaNotificacoes.getState().items) {
          const item = __listaNotificacoes.getState().items[index]
          const itemTitleSanitized = ACCENT().remove(item.titulo.toString().toUpperCase())

          if (itemTitleSanitized.includes(textSanitized)) {
            __listaNotificacoes.getState().filteredItems.push(item)
          }
        }

        __listaNotificacoes._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
