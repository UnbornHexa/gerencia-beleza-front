/* Requires */

const NOTIFICACOES_REQUEST = require('../../../global/requests/notificacoes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __notificacoesTop = document.querySelector('section.top .top__options a[name="notificacoes"]')
  const __notificacoesNavbar = document.querySelector('navbar .navbar__notificacoes a[name="notificacoes"]')

  // Methods

  methods.checkNews = async () => {
    try {
      const count = await NOTIFICACOES_REQUEST().countNotViewed()
      methods._desactiveIcon()
      if (count > 0) methods._activeIcon()
    } catch (e) { console.log(e) }
  }

  methods._activeIcon = () => {
    __notificacoesTop.classList.add('active')
    __notificacoesNavbar.classList.add('active')
  }

  methods._desactiveIcon = () => {
    __notificacoesTop.classList.remove('active')
    __notificacoesNavbar.classList.remove('active')
  }

  return methods
}

module.exports = Module
