/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const CONVERT = require('../../../global/helpers/convert')
const SYMBOL = require('../../../global/helpers/symbols')
const PARSER = require('../../../global/helpers/parser')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const NOTIFICACOES_REQUEST = require('../requests/notificacoes')
const NOTIFICACOES_HELPER = require('../helpers/notificacoes')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_notificacao) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    let mensagem = PARSER().htmlToText(_notificacao.mensagem)
    if (mensagem.length > 45) mensagem = mensagem.slice(0, 40).concat('...')

    let classe = ''
    if (_notificacao.is_visualizado === false) classe = 'unseen'

    const li =
      `
    <li class="${classe}">
      <span class="item__id" hidden>${_notificacao._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-bell"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_notificacao.titulo}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
            </div>

          </span>
          <span class="item__sobre">${mensagem}</span>
        </div>
      </div>
      <div class="lista__data">
        <span class="item__data">${CONVERT().dateBR(_notificacao.data_registro)}</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idNotificacao = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const notificacao = await NOTIFICACOES_REQUEST().getNotificacaoById(idNotificacao)
        MODAL_VISUALIZAR().importDataToModal(notificacao)

        const notificacoes = await NOTIFICACOES_REQUEST().getNotificacoes()
        this.importItems(notificacoes)

        NOTIFICACOES_HELPER().checkNews()
      } catch (e) { console.log(e) }
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__notificacoes')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()

    SYMBOL().subscribe('lista_notificacoes', component)
  }

  methods.updateItems = async () => {
    const listaNotificacoes = SYMBOL().retrieve('lista_notificacoes')

    try {
      const notificacoes = await NOTIFICACOES_REQUEST().getNotificacoes()
      listaNotificacoes.importItems(notificacoes)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
