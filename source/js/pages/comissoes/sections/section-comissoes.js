/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __comissoesSection = document.querySelector('section.financeiro__info')

  const __comissoesTotalSpan = __comissoesSection.querySelector('.info__complemento .complemento__comissoes span#comissoes__mes')
  const __comissoesGallery = __comissoesSection.querySelector('.info__content .content__funcionarios')

  let __total = 0

  // Methods

  methods.load = (_comissoes) => {
    methods._resetGallery()

    const comissoes = methods._calcComissoes(_comissoes)
    __comissoesTotalSpan.innerText = FORMAT().money(comissoes.total)

    return comissoes.total
  }

  methods._calcComissoes = (_comissoes) => {
    for (const index in _comissoes) {
      const nomeFuncionario = _comissoes[index]._id
      const valor = _comissoes[index].valor

      methods._addItemInGallery(nomeFuncionario, valor)
      __total += valor
    }

    return { total: __total }
  }

  // Gallery

  methods._addItemInGallery = (_funcionario, _valor) => {
    const funcionario = (_funcionario.split(' ').length > 2)
      ? `${_funcionario.split(' ')[0]} ${_funcionario.split(' ')[1]}`
      : _funcionario

    const item =
    `
    <div class="info_funcionario fade">
      <div class="imagem">
        <img src="https://app.gerenciabeleza.com.br/img/plataforma/all/placeholder-avatar.svg">
      </div>
      <div class="nome">
        <span id="nome_funcionario">${funcionario}</span>
      </div>
      <div class="comissao">
        <span id="comissao_funcionario">${FORMAT().money(_valor)}</span>
        <p>em comissões</p>
      </div>
    </div>
    `

    __comissoesGallery.innerHTML += item
  }

  methods._resetGallery = () => {
    __comissoesGallery.innerHTML = ''
  }

  return methods
}

module.exports = Module
