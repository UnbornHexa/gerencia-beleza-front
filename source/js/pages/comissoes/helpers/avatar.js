/* Requires */

const USUARIOS_REQUEST = require('../../../global/requests/usuarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __asideAvatarImg = document.querySelector('aside .aside__perfil a img')
  const __navbarMobileAvatarImg = document.querySelector('navbar .navbar__perfil a img')

  // Methods

  methods.updateImage = async () => {
    try {
      const avatar = await USUARIOS_REQUEST().getImagem()
      if (!avatar.imagem) return

      window.localStorage.setItem('imagem-avatar', avatar.imagem)
      methods._showAvatar(avatar.imagem)
    } catch (e) { console.log(e) }
  }

  methods._showAvatar = (_image) => {
    __asideAvatarImg.src = _image
    __navbarMobileAvatarImg.src = _image
  }

  return methods
}

module.exports = Module
