/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SECTION_COMISSOES = require('../sections/section-comissoes')

const COMISSOES_REQUEST = require('../requests/comissoes')
const RESUMO_REQUEST = require('../requests/resumo')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateValues = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const date = dateSelector.value()
    const yearAndMonth = `${date.year}-${date.month.toString().padStart(2, '0')}`

    await methods._updateComissoes(yearAndMonth)
    await methods._updateResumo(yearAndMonth)
  }

  methods._updateComissoes = async (_yearAndMonth) => {
    const listaComissoes = SYMBOL().retrieve('lista_comissoes')

    try {
      const comissoes = await COMISSOES_REQUEST().getComissoes(_yearAndMonth)
      listaComissoes.importItems(comissoes)
    } catch (e) { console.log(e) }
  }

  methods._updateResumo = async (_yearAndMonth) => {
    try {
      const resumo = await RESUMO_REQUEST().getResumoFinanceiro(_yearAndMonth)
      SECTION_COMISSOES().load(resumo.comissoes)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
