/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_COMPONENTS_COMMUNICATOR = require('./helpers/components-communicator')

const DATE_SELECTOR = require('./extendeds/date-selector')
const LISTA_COMISSOES = require('./extendeds/lista-comissoes')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('financeiro')
  ASIDE_PICTURE().load()

  DATE_SELECTOR().init()
  LISTA_COMISSOES().init()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  HELPER_COMPONENTS_COMMUNICATOR().updateValues()

  EVENTS().activeEvents()
}
start()
