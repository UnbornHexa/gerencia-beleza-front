const Module = () => {
  const methods = {}

  // Internal Variables

  const __galleryDiv = document.querySelector('section.financeiro__info .info__content .content__funcionarios')

  // Methods

  methods.activeOnDrag = () => {
    let isDown = false
    let startX
    let scrollLeft

    __galleryDiv.addEventListener('mousedown', (e) => {
      isDown = true

      startX = e.pageX - __galleryDiv.offsetLeft
      scrollLeft = __galleryDiv.scrollLeft
    })

    __galleryDiv.addEventListener('mouseleave', () => {
      isDown = false
      __galleryDiv.classList.remove('drag')
    })

    __galleryDiv.addEventListener('mouseup', () => {
      isDown = false
      __galleryDiv.classList.remove('drag')
    })

    __galleryDiv.addEventListener('mousemove', (e) => {
      if (!isDown) return
      e.preventDefault()

      const x = e.pageX - __galleryDiv.offsetLeft
      const walk = (x - startX) * 2
      __galleryDiv.scrollLeft = scrollLeft - walk
      __galleryDiv.classList.add('drag')
    })
  }

  return methods
}

module.exports = Module
