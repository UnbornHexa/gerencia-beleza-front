/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_permissao')

  const __nomeCollapsible = __modal.querySelector('.collapsible .nome span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')

  const __agendaCollapsible = __modal.querySelector('.collapsible#permissoes .agenda span')
  const __clientesCollapsible = __modal.querySelector('.collapsible#permissoes .clientes span')
  const __comandasCollapsible = __modal.querySelector('.collapsible#permissoes .comandas span')
  const __servicosCollapsible = __modal.querySelector('.collapsible#permissoes .servicos span')
  const __produtosCollapsible = __modal.querySelector('.collapsible#permissoes .produtos span')
  const __materiaisCollapsible = __modal.querySelector('.collapsible#permissoes .materiais span')
  const __financeiroCollapsible = __modal.querySelector('.collapsible#permissoes .financeiro span')
  const __permissoesCollapsible = __modal.querySelector('.collapsible#permissoes .permissoes span')
  const __funcionariosCollapsible = __modal.querySelector('.collapsible#permissoes .funcionarios span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __nomeCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'

    __agendaCollapsible.innerHTML = ''
    __clientesCollapsible.innerHTML = ''
    __comandasCollapsible.innerHTML = ''
    __servicosCollapsible.innerHTML = ''
    __produtosCollapsible.innerHTML = ''
    __materiaisCollapsible.innerHTML = ''
    __financeiroCollapsible.innerHTML = ''
    __permissoesCollapsible.innerHTML = ''
    __funcionariosCollapsible.innerHTML = ''

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    __nomeCollapsible.innerText = _data.nome || '...'
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || '...'

    if (_data.permissao.agenda.view) __agendaCollapsible.innerHTML += '<span>Visualizar Horários</span>'
    if (_data.permissao.agenda.add) __agendaCollapsible.innerHTML += '<span>Adicionar Horários</span>'
    if (_data.permissao.agenda.edit) __agendaCollapsible.innerHTML += '<span>Editar Horários</span>'
    if (_data.permissao.agenda.del) __agendaCollapsible.innerHTML += '<span>Deletar Horários</span>'
    if (_data.permissao.agenda.view_others) __agendaCollapsible.innerHTML += '<span>Visualizar Outras Agendas</span>'

    if (_data.permissao.clientes.view) __clientesCollapsible.innerHTML += '<span>Visualizar Clientes</span>'
    if (_data.permissao.clientes.add) __clientesCollapsible.innerHTML += '<span>Adicionar Clientes</span>'
    if (_data.permissao.clientes.edit) __clientesCollapsible.innerHTML += '<span>Editar Clientes</span>'
    if (_data.permissao.clientes.del) __clientesCollapsible.innerHTML += '<span>Deletar Clientes</span>'

    if (_data.permissao.comandas.view) __comandasCollapsible.innerHTML += '<span>Visualizar Comandas</span>'
    if (_data.permissao.comandas.add) __comandasCollapsible.innerHTML += '<span>Adicionar Comandas</span>'
    if (_data.permissao.comandas.edit) __comandasCollapsible.innerHTML += '<span>Editar Comandas</span>'
    if (_data.permissao.comandas.del) __comandasCollapsible.innerHTML += '<span>Deletar Comandas</span>'

    if (_data.permissao.servicos.view) __servicosCollapsible.innerHTML += '<span>Visualizar Serviços</span>'
    if (_data.permissao.servicos.add) __servicosCollapsible.innerHTML += '<span>Adicionar Serviços</span>'
    if (_data.permissao.servicos.edit) __servicosCollapsible.innerHTML += '<span>Editar Serviços</span>'
    if (_data.permissao.servicos.del) __servicosCollapsible.innerHTML += '<span>Deletar Serviços</span>'

    if (_data.permissao.produtos.view) __produtosCollapsible.innerHTML += '<span>Visualizar Produtos</span>'
    if (_data.permissao.produtos.add) __produtosCollapsible.innerHTML += '<span>Adicionar Produtos</span>'
    if (_data.permissao.produtos.edit) __produtosCollapsible.innerHTML += '<span>Editar Produtos</span>'
    if (_data.permissao.produtos.del) __produtosCollapsible.innerHTML += '<span>Deletar Produtos</span>'

    if (_data.permissao.materiais.view) __materiaisCollapsible.innerHTML += '<span>Visualizar Materiais</span>'
    if (_data.permissao.materiais.add) __materiaisCollapsible.innerHTML += '<span>Adicionar Materiais</span>'
    if (_data.permissao.materiais.edit) __materiaisCollapsible.innerHTML += '<span>Editar Materiais</span>'
    if (_data.permissao.materiais.del) __materiaisCollapsible.innerHTML += '<span>Deletar Materiais</span>'
    if (_data.permissao.materiais.manage) __materiaisCollapsible.innerHTML += '<span>Repor Materiais</span>'

    if (_data.permissao.lancamentos.view) __financeiroCollapsible.innerHTML += '<span>Visualizar Financeiro</span>'
    if (_data.permissao.lancamentos.add) __financeiroCollapsible.innerHTML += '<span>Adicionar Lançamentos</span>'
    if (_data.permissao.lancamentos.edit) __financeiroCollapsible.innerHTML += '<span>Editar Lançamentos</span>'
    if (_data.permissao.lancamentos.del) __financeiroCollapsible.innerHTML += '<span>Deletar Lançamentos</span>'

    if (_data.permissao.permissoes.view) __permissoesCollapsible.innerHTML += '<span>Visualizar Permissões</span>'
    if (_data.permissao.permissoes.add) __permissoesCollapsible.innerHTML += '<span>Adicionar Permissões</span>'
    if (_data.permissao.permissoes.edit) __permissoesCollapsible.innerHTML += '<span>Editar Permissões</span>'
    if (_data.permissao.permissoes.del) __permissoesCollapsible.innerHTML += '<span>Deletar Permissões</span>'

    if (_data.permissao.usuarios.view) __funcionariosCollapsible.innerHTML += '<span>Visualizar Funcionários</span>'
    if (_data.permissao.usuarios.add) __funcionariosCollapsible.innerHTML += '<span>Adicionar Funcionários</span>'
    if (_data.permissao.usuarios.edit) __funcionariosCollapsible.innerHTML += '<span>Editar Funcionários</span>'
    if (_data.permissao.usuarios.del) __funcionariosCollapsible.innerHTML += '<span>Deletar Funcionários</span>'
    if (_data.permissao.usuarios.pay) __funcionariosCollapsible.innerHTML += '<span>Pagar Funcionários</span>'
  }

  return methods
}

module.exports = Module
