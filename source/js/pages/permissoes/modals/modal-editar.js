/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const EXPORTAR_PERMISSAO = require('../helpers/exportar-permissao')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_permissao')

  const __idInput = __modal.querySelector('.permissao__id input')
  const __nomeInput = __modal.querySelector('.nome input')

  const __setoresDiv = __modal.querySelector('.setores .setores__button')
  const __permissoesDiv = __modal.querySelector('.setor__permissoes')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Setor Buttons

  methods.desactiveButtons = () => {
    const setores = __setoresDiv.querySelectorAll('button')

    setores.forEach(_setor => {
      _setor.classList.remove('active')
    })
  }

  methods.activeButtonOfSetor = (_setor) => {
    methods.desactiveButtons()
    _setor.classList.add('active')
  }

  // Setor Permissoes

  methods.hidePermissoes = () => {
    const permissoes = __permissoesDiv.querySelectorAll('div')

    permissoes.forEach(_permissao => {
      if (_permissao.getAttribute('data-setor')) {
        _permissao.classList.remove('active')
        _permissao.classList.add('hidden')
      }
    })
  }

  methods.showPermissoesOfSetor = (_setor) => {
    methods.hidePermissoes()

    const permissoes = __modal.querySelector(`.setor__permissoes div.setor__${_setor}`)
    permissoes.classList.remove('hidden')
    permissoes.classList.add('active')
  }

  methods.desactivePermissoes = () => {
    const permissoes = __modal.querySelectorAll('.setor__permissoes div div')
    permissoes.forEach(_permissao => {
      _permissao.classList.remove('active')
    })
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    const botaoPermissaoAgenda = __modal.querySelector('.setores__button button[data-setor="agenda"]')

    __nomeInput.value = ''
    botaoPermissaoAgenda.click()
    methods.desactivePermissoes()

    __editarButton.setAttribute('disabled', true)
  }

  // Data

  methods.activePermissaoIfEnabled = (_value, _setor, _permissao) => {
    const div = __modal.querySelector(`.setor__permissoes .${_setor}__${_permissao}`)
    if (_value === true) {
      div.classList.add('active')
      return
    }

    div.classList.remove('active')
  }

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __nomeInput.value = _data.nome || ''

    methods.activePermissaoIfEnabled(_data.permissao.agenda.view, 'agenda', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.agenda.add, 'agenda', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.agenda.edit, 'agenda', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.agenda.del, 'agenda', 'deletar')
    methods.activePermissaoIfEnabled(_data.permissao.agenda.view_others, 'agenda', 'multipla')

    methods.activePermissaoIfEnabled(_data.permissao.clientes.view, 'clientes', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.clientes.add, 'clientes', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.clientes.edit, 'clientes', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.clientes.del, 'clientes', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.comandas.view, 'comandas', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.comandas.add, 'comandas', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.comandas.edit, 'comandas', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.comandas.del, 'comandas', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.servicos.view, 'servicos', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.servicos.add, 'servicos', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.servicos.edit, 'servicos', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.servicos.del, 'servicos', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.produtos.view, 'produtos', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.produtos.add, 'produtos', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.produtos.edit, 'produtos', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.produtos.del, 'produtos', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.materiais.view, 'materiais', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.materiais.add, 'materiais', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.materiais.edit, 'materiais', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.materiais.del, 'materiais', 'deletar')
    methods.activePermissaoIfEnabled(_data.permissao.materiais.manage, 'materiais', 'repor')

    methods.activePermissaoIfEnabled(_data.permissao.lancamentos.view, 'financeiro', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.lancamentos.add, 'financeiro', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.lancamentos.edit, 'financeiro', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.lancamentos.del, 'financeiro', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.permissoes.view, 'permissoes', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.permissoes.add, 'permissoes', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.permissoes.edit, 'permissoes', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.permissoes.del, 'permissoes', 'deletar')

    methods.activePermissaoIfEnabled(_data.permissao.usuarios.view, 'funcionarios', 'visualizar')
    methods.activePermissaoIfEnabled(_data.permissao.usuarios.add, 'funcionarios', 'adicionar')
    methods.activePermissaoIfEnabled(_data.permissao.usuarios.edit, 'funcionarios', 'editar')
    methods.activePermissaoIfEnabled(_data.permissao.usuarios.del, 'funcionarios', 'deletar')
    methods.activePermissaoIfEnabled(_data.permissao.usuarios.pay, 'funcionarios', 'pagar')
  }

  methods.exportDataFromModal = () => {
    const setorPermissaoModalEditar = 'section#modal_editar_permissao .setor__permissoes div'
    const permissao = {}
    permissao.permissao = {}

    // required
    permissao.nome = __nomeInput.value || ''
    permissao.permissao = EXPORTAR_PERMISSAO().exportAsObject(setorPermissaoModalEditar)

    return permissao
  }

  return methods
}

module.exports = Module
