/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const EXPORTAR_PERMISSAO = require('../helpers/exportar-permissao')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_permissao')

  const __nomeInput = __modal.querySelector('.nome input')

  const __setoresDiv = __modal.querySelector('.setores .setores__button')
  const __permissoesDiv = __modal.querySelector('.setor__permissoes')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Setor Buttons

  methods.desactiveButtons = () => {
    const setores = __setoresDiv.querySelectorAll('button')

    setores.forEach(_setor => {
      _setor.classList.remove('active')
    })
  }

  methods.activeButtonOfSetor = (_setor) => {
    methods.desactiveButtons()
    _setor.classList.add('active')
  }

  // Setor Permissoes

  methods.hidePermissoes = () => {
    const permissoes = __permissoesDiv.querySelectorAll('div')

    permissoes.forEach(_permissao => {
      if (_permissao.getAttribute('data-setor')) {
        _permissao.classList.remove('active')
        _permissao.classList.add('hidden')
      }
    })
  }

  methods.showPermissoesOfSetor = (_setor) => {
    methods.hidePermissoes()

    const permissoes = __modal.querySelector(`.setor__permissoes div.setor__${_setor}`)
    permissoes.classList.remove('hidden')
    permissoes.classList.add('active')
  }

  methods.desactivePermissoes = () => {
    const permissoes = __modal.querySelectorAll('.setor__permissoes div div')
    permissoes.forEach(_permissao => {
      _permissao.classList.remove('active')
    })
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    const botaoPermissaoAgenda = __modal.querySelector('.setores__button button[data-setor="agenda"]')

    __nomeInput.value = ''
    botaoPermissaoAgenda.click()
    methods.desactivePermissoes()
  }

  // Data

  methods.exportDataFromModal = () => {
    const setorPermissaoModalAdicionar = 'section#modal_adicionar_permissao .setor__permissoes div'
    const permissao = {}

    // required
    permissao.nome = __nomeInput.value || ''
    permissao.permissao = EXPORTAR_PERMISSAO().exportAsObject(setorPermissaoModalAdicionar)
    permissao.admin = false

    return permissao
  }

  return methods
}

module.exports = Module
