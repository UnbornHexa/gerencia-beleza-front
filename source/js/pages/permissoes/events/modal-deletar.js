/* Requires */

const PERMISSOES_REQUEST = require('../requests/permissoes')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_PERMISSOES = require('../extendeds/lista-permissoes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_permissao .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_permissao .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idPermissao = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await PERMISSOES_REQUEST().delPermissao(idPermissao)
        MODAL_DELETAR().hideModal()
        await LISTA_PERMISSOES().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
