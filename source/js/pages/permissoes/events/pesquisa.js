/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaPermissoes = SYMBOL().retrieve('lista_permissoes')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaPermissoes.getState().filteredItems = __listaPermissoes.getState().items
          __listaPermissoes._loadItemsFromPage(1)
          return
        }

        __listaPermissoes.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaPermissoes.getState().items) {
          const item = __listaPermissoes.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaPermissoes.getState().filteredItems.push(item)
          }
        }

        __listaPermissoes._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
