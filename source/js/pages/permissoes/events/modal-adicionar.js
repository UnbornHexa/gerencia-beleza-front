/* Requires */

const PERMISSOES_REQUEST = require('../requests/permissoes')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const LISTA_PERMISSOES = require('../extendeds/lista-permissoes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_permissao .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await PERMISSOES_REQUEST().addPermissao(modalData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_PERMISSOES().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  // Setores

  methods.activeClickOnSetor = () => {
    document.addEventListener('click', (e) => {
      const match = 'section#modal_adicionar_permissao .setores .setores__button button'
      if (!e.target.matches(match)) return

      MODAL_ADICIONAR().activeButtonOfSetor(e.target)

      const setorNome = e.target.getAttribute('data-setor')
      MODAL_ADICIONAR().showPermissoesOfSetor(setorNome)
    })
  }

  methods.activeClickOnPermissao = () => {
    document.addEventListener('click', (e) => {
      const match = 'section#modal_adicionar_permissao .setor__permissoes div div'
      if (!e.target.matches(match)) return

      e.target.classList.toggle('active')
    })
  }

  return methods
}

module.exports = Module
