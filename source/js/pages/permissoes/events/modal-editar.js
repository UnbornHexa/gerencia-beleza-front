/* Requires */

const LISTA_PERMISSOES = require('../extendeds/lista-permissoes')
const PERMISSOES_REQUEST = require('../requests/permissoes')
const MODAL_EDITAR = require('../modals/modal-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_permissao')

  const __idInput = __modal.querySelector('.permissao__id input')
  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idPermissao = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await PERMISSOES_REQUEST().editPermissao(idPermissao, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_PERMISSOES().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Setores

  methods.activeClickOnSetor = () => {
    document.addEventListener('click', (e) => {
      const match = 'section#modal_editar_permissao .setores .setores__button button'
      if (!e.target.matches(match)) return

      MODAL_EDITAR().activeButtonOfSetor(e.target)

      const setorNome = e.target.getAttribute('data-setor')
      MODAL_EDITAR().showPermissoesOfSetor(setorNome)
    })
  }

  methods.activeClickOnPermissao = () => {
    document.addEventListener('click', (e) => {
      const match = 'section#modal_editar_permissao .setor__permissoes div div'
      if (!e.target.matches(match)) return

      e.target.classList.toggle('active')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_permissao input') &&
        !e.target.matches('section#modal_editar_permissao textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_permissao .setor__permissoes div div')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
