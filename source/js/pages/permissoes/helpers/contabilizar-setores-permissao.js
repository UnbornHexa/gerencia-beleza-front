const Module = () => {
  const methods = {}

  methods.count = (_permissao) => {
    let contadorPermissoes = 0

    for (const setor of Object.entries(_permissao)) {
      if (typeof setor[1] !== 'object') continue

      for (const acesso of Object.entries(setor[1])) {
        if (acesso[1] === true) {
          contadorPermissoes++
          break
        }
      }
    }

    return contadorPermissoes
  }

  return methods
}

module.exports = Module
