const Module = () => {
  const methods = {}

  methods.exportAsObject = (_modalPermissoes) => {
    const setores = document.querySelectorAll(_modalPermissoes)
    const permissaoOBJ = {}

    for (let index = 0; index <= setores.length - 1; index++) {
      const setorItem = setores[index]
      if (!setorItem.getAttribute('data-setor')) continue

      const setorNome = setorItem.getAttribute('data-setor')
      const permissoes = setorItem.querySelectorAll('div')
      permissaoOBJ[setorNome] = {}

      for (let index1 = 0; index1 <= permissoes.length - 1; index1++) {
        const permissaoItem = permissoes[index1]
        if (!permissaoItem.getAttribute('data-permissao')) continue

        const permissaoNome = permissaoItem.getAttribute('data-permissao')
        const permissaoValor = (permissaoItem.classList.contains('active'))
        permissaoOBJ[setorNome][permissaoNome] = permissaoValor
      }

      permissaoOBJ.admin = false
    }

    return permissaoOBJ
  }

  return methods
}

module.exports = Module
