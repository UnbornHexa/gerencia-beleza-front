/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')

const CONTABILIZAR_SETORES_PERMISSAO = require('../helpers/contabilizar-setores-permissao')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const PERMISSOES_REQUEST = require('../requests/permissoes')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_permissao) {
    const itemsDiv = this.__component.querySelector('.lista__itens')
    const numeroAreasAtivas = CONTABILIZAR_SETORES_PERMISSAO().count(_permissao.permissao)

    const li =
      `
    <li>
      <span class="item__id" hidden>${_permissao._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-id-badge"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_permissao.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__acessos">${numeroAreasAtivas} acessos</span>
        </div>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idPermissao = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const permissao = await PERMISSOES_REQUEST().getPermissaoById(idPermissao)
        MODAL_VISUALIZAR().importDataToModal(permissao)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idPermissao = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const permissao = await PERMISSOES_REQUEST().getPermissaoById(idPermissao)
        MODAL_EDITAR().importDataToModal(permissao)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idPermissao = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomePermissao = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_permissao .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_permissao .modal__header p')

      inputIdModal.value = idPermissao
      headerNomeModal.innerText = nomePermissao
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__permissoes')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_permissoes', component)
  }

  methods.updateItems = async () => {
    const listaPermissoes = SYMBOL().retrieve('lista_permissoes')

    try {
      const permissoes = await PERMISSOES_REQUEST().getPermissoes()
      listaPermissoes.importItems(permissoes)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
