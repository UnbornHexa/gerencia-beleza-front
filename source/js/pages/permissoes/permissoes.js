/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')

const LISTA_PERMISSOES = require('./extendeds/lista-permissoes')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('permissoes')
  ASIDE_PICTURE().load()

  LISTA_PERMISSOES().init()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_PERMISSOES().updateItems()

  EVENTS().activeEvents()
}
start()
