/* Requires */

const HOSTS = require('../../../global/data/hosts')
const TOKEN = require('../../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  methods.redirectAuthenticatedUser = () => {
    if (!TOKEN().getToken()) return
    window.location.assign(`${HOSTS().url.app_plataforma}/app/agenda`)
  }

  methods.redirectBemVindo = () => {
    const data = 'bemvindo_visualizado'
    if (window.localStorage.getItem(data)) return false

    window.localStorage.setItem(data, true)
    window.location.assign(`${HOSTS().url.app_plataforma}/app/bem-vindo`)
  }

  return methods
}

module.exports = Module
