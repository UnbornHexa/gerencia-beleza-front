/* Requires */

const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_recuperar_senha')
  const __emailInput = __modal.querySelector('.email input')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__emailInput.value) return true

    MESSAGEBOX().alert('Preencha os dados obrigatórios')
    return false
  }

  methods.validateFields = () => {
    const emailOk = FORM_VALIDATION().email(__emailInput.value)
    if (emailOk) return true

    MESSAGEBOX().alert('Preencha um email válido')
    return false
  }

  methods.resetForm = () => {
    __emailInput.value = ''
  }

  return methods
}

module.exports = Module
