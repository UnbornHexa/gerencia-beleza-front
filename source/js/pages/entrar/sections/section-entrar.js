/* Requires */

const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __emailInput = document.querySelector('.entrar .entrar__email input')
  const __senhaInput = document.querySelector('.entrar .entrar__senha input')

  // Form

  methods.checkRequiredFields = () => {
    if (__emailInput.value && __senhaInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.validateFields = () => {
    const emailOk = FORM_VALIDATION().email(__emailInput.value)
    if (emailOk) return true

    MESSAGEBOX().alert('Preencha um email válido')
    return false
  }

  // Data

  methods.exportDataFromModal = () => {
    const email = __emailInput.value
    const senha = __senhaInput.value

    return { email, senha }
  }

  return methods
}

module.exports = Module
