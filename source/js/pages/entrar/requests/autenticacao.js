/* Requires */

const HOSTS = require('../../../global/data/hosts')
const HTTP = require('../../../global/helpers/http')
const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __url = `${HOSTS().url.api_plataforma}/autenticacao`

  // Methods

  methods.entrar = async (_data) => {
    const url = `${__url}/entrar`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, null, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.recuperarSenha = async (_email) => {
    const url = `${__url}/recuperar/senha/${_email}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, null, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
