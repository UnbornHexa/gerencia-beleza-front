/* Requires */

const AUTENTICACAO_REQUEST = require('../requests/autenticacao')
const MODAL_RECUPERAR = require('../modal/modal-recuperar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __emailInput = document.querySelector('#modal_recuperar_senha .email input')
  const __recuperarButton = document.querySelector('#modal_recuperar_senha .modal__button #recuperar')

  // Methods

  methods.activeClickOnRecuperarButton = () => {
    __recuperarButton.addEventListener('click', async () => {
      const email = __emailInput.value

      const modalOk = MODAL_RECUPERAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_RECUPERAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __recuperarButton.setAttribute('disabled', true)
        await AUTENTICACAO_REQUEST().recuperarSenha(email)
        MODAL_RECUPERAR().hideModal()
      } catch (e) { console.log(e) }

      __recuperarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
