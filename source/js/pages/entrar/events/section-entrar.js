/* Requires */

const TOKEN = require('../../../global/helpers/token')

const REDIRECIONAMENTO = require('../helpers/redirecionamento')
const AUTENTICACAO_REQUEST = require('../requests/autenticacao')
const MODAL_RECUPERAR = require('../modal/modal-recuperar')
const SECTION_ENTRAR = require('../sections/section-entrar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __entrarButton = document.querySelector('section.entrar .entrar__button #entrar')
  const __senhaInput = document.querySelector('section.entrar .entrar__senha input')
  const __recuperarLink = document.querySelector('section.entrar a#opcoes__recuperacao__senha')

  // Methods

  methods.activeClickOnEntrarButton = () => {
    __entrarButton.addEventListener('click', async () => {
      const modalData = SECTION_ENTRAR().exportDataFromModal()

      const modalOk = SECTION_ENTRAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = SECTION_ENTRAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __entrarButton.setAttribute('disabled', true)
        const tokenOk = await AUTENTICACAO_REQUEST().entrar(modalData)
        TOKEN().setToken(tokenOk)

        if (REDIRECIONAMENTO().redirectBemVindo() !== false) return
        REDIRECIONAMENTO().redirectAuthenticatedUser()
      } catch (e) { console.log(e) }

      __entrarButton.removeAttribute('disabled')
    })
  }

  methods.activeEnterKeyOnSenhaInput = () => {
    __senhaInput.addEventListener('keypress', (e) => {
      const keyCode = e.keyCode || e.which
      if (keyCode !== 13) return false

      __entrarButton.click()
    })
  }

  methods.activeClickOnRecuperarLink = () => {
    __recuperarLink.addEventListener('click', () => {
      MODAL_RECUPERAR().showModal()
    })
  }

  return methods
}

module.exports = Module
