/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')

const SECTION_ENTRAR = require('./section-entrar')
const MODAL_RECUPERAR = require('./modal-recuperar')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()

    SECTION_ENTRAR().activeClickOnEntrarButton()
    SECTION_ENTRAR().activeEnterKeyOnSenhaInput()
    SECTION_ENTRAR().activeClickOnRecuperarLink()

    MODAL_RECUPERAR().activeClickOnRecuperarButton()
  }

  return methods
}

module.exports = Module
