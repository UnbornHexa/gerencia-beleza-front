/* Requires */

require('regenerator-runtime/runtime')

const EVENTS = require('./events/events')

const REDIRECIONAMENTO = require('./helpers/redirecionamento')

/* Start */

const start = async () => {
  REDIRECIONAMENTO().redirectAuthenticatedUser()

  EVENTS().activeEvents()
}
start()
