/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')

const SECTION_REGISTRAR = require('./section-registrar')
const MODAL_CONCLUIR = require('./modal-concluir')
const MODAL_AJUDA = require('./modal-ajuda')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()

    SECTION_REGISTRAR().activeClickOnRegistrarButton()

    MODAL_CONCLUIR().activeClickOnEntrarButton()

    MODAL_AJUDA().activeClickOnAjuda()
  }

  return methods
}

module.exports = Module
