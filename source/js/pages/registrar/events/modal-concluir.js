/* Requires */

const HELPER_REDIRECT = require('../helpers/redirect')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __entrarButton = document.querySelector('section.modal .modal__button #entrar')

  // Methods

  methods.activeClickOnEntrarButton = () => {
    __entrarButton.addEventListener('click', () => {
      HELPER_REDIRECT().entrar()
    })
  }

  return methods
}

module.exports = Module
