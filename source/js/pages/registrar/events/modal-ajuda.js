/* Requires */

const MODAL_AJUDA = require('../modals/modal-ajuda')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __ajuda = document.querySelector('section.registrar .registrar__opcoes #ajuda')

  // Methods

  methods.activeClickOnAjuda = () => {
    __ajuda.addEventListener('click', async () => {
      MODAL_AJUDA().showModal()
    })
  }

  return methods
}

module.exports = Module
