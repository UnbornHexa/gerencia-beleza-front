/* Requires */

const SECTION_REGISTRAR = require('../sections/section-registrar')
const AUTENTICACAO_REQUEST = require('../requests/autenticacao')

const MODAL_CONCLUIR = require('../modals/modal-concluir')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __registrarButton = document.querySelector('section.registrar .registrar__button #registrar')

  // Methods

  methods.activeClickOnRegistrarButton = () => {
    __registrarButton.addEventListener('click', async () => {
      const modalData = SECTION_REGISTRAR().exportDataFromModal()

      const formOk = SECTION_REGISTRAR().checkRequiredFields()
      if (formOk === false) return

      const formValidatedOk = SECTION_REGISTRAR().validateFields()
      if (formValidatedOk === false) return

      try {
        __registrarButton.setAttribute('disabled', true)
        await AUTENTICACAO_REQUEST().registrar(modalData)
        MODAL_CONCLUIR().showModal()
        SECTION_REGISTRAR().resetForm()
        window.fbq('track', 'CompleteRegistration')
      } catch (e) { console.log(e) }

      __registrarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
