/* Requires */

const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section.registrar')

  const __nomeEmpresaInput = __modal.querySelector('.registrar__inputs .registrar__salao input')
  const __nomeProprietarioInput = __modal.querySelector('.registrar__inputs .registrar__nome input')
  const __telefoneInput = __modal.querySelector('.registrar__inputs .registrar__telefone input')

  const __emailInput = __modal.querySelector('.registrar__inputs .registrar__email input')
  const __senhaInput = __modal.querySelector('.registrar__inputs .registrar__senha input')
  const __confirmarSenhaInput = __modal.querySelector('.registrar__inputs .registrar__salao__confirmar input')

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeProprietarioInput.value && __telefoneInput.value && __nomeEmpresaInput.value &&
      __emailInput.value && __senhaInput.value && __confirmarSenhaInput.value) return true

    MESSAGEBOX().alert('Preencha todos os campos para criar sua conta.')
    return false
  }

  methods.validateFields = () => {
    const telefoneOk = FORM_VALIDATION().phone(__telefoneInput.value)
    const emailOk = FORM_VALIDATION().email(__emailInput.value)

    if (!telefoneOk) {
      MESSAGEBOX().alert('O Telefone não é válido.')
      return false
    }

    if (!emailOk) {
      MESSAGEBOX().alert('O Email não é válido')
      return false
    }

    if (__senhaInput.value.length < 6) {
      MESSAGEBOX().alert('A senha deve ter no mínimo 6 caractéres.')
      return false
    }

    if (__senhaInput.value !== __confirmarSenhaInput.value) {
      MESSAGEBOX().alert('As senhas devem ser iguais.')
      return false
    }

    return true
  }

  methods.resetForm = () => {
    __nomeEmpresaInput.value = ''
    __nomeProprietarioInput.value = ''
    __telefoneInput.value = ''

    __emailInput.value = ''
    __senhaInput.value = ''
    __confirmarSenhaInput.value = ''
  }

  // Data

  methods.exportDataFromModal = () => {
    const empresa = {}
    const usuario = {}

    usuario.contato = {}
    empresa.contato = {}
    empresa.endereco = {}

    // required
    empresa.nome = __nomeEmpresaInput.value || ''

    usuario.nome = __nomeProprietarioInput.value || ''
    usuario.contato.telefone1 = __telefoneInput.value || ''
    usuario.email = __emailInput.value || ''
    usuario.senha = __senhaInput.value || ''

    return { empresa, usuario }
  }

  return methods
}

module.exports = Module
