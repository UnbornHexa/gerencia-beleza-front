/* Requires */

const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  methods.entrar = () => {
    window.location.assign(`${HOSTS().url.app_plataforma}/entrar`)
  }

  return methods
}

module.exports = Module
