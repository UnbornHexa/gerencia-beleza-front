/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')

const FUNCIONALIDADES_DATA = require('../data/funcionalidades')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_funcionalidade) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li = `
    <li>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="${_funcionalidade.icon}"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_funcionalidade.name}

            <div class="lista__opcoes">
              <div class="opcoes__tutorial">
                <i class="icon-question-circle"></i>
                <span>Aprender a usar</span>
              </div>
              <div class="opcoes__acessar">
                <i class="${_funcionalidade.icon}"></i>
                <span>Acessar Página</span>
              </div>
            </div>

          </span>

          <span class="item__sobre">${_funcionalidade.description}</span>
        </div>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuTutorial () {
    const match = `${this.__id} .lista__opcoes .opcoes__tutorial`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      const nomeFuncionalidade = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText
      const funcionalidade = FUNCIONALIDADES_DATA.filter(_funcionalidade => _funcionalidade.name === nomeFuncionalidade)

      const url = 'https://wiki.gerenciabeleza.com'
      window.open(`${url}/${funcionalidade[0].wiki}`, '_blank')
    })
  }

  activeClickOnMenuAcessar () {
    const match = `${this.__id} .lista__opcoes .opcoes__acessar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      const nomeFuncionalidade = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText
      const funcionalidade = FUNCIONALIDADES_DATA.filter(_funcionalidade => _funcionalidade.name === nomeFuncionalidade)

      window.location.assign(funcionalidade[0].page)
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__funcionalidades')

    component.render()
    component.getState().itemsPerPage = 30

    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuTutorial()
    component.activeClickOnMenuAcessar()

    SYMBOL().subscribe('lista_funcionalidades', component)
  }

  methods.updateItems = async () => {
    const listaFuncionalidades = SYMBOL().retrieve('lista_funcionalidades')

    try {
      const funcionalidades = FUNCIONALIDADES_DATA
      listaFuncionalidades.importItems(funcionalidades)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
