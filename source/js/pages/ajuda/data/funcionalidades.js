module.exports = [
  {
    name: 'Agenda',
    description: 'Agende os horários de suas clientes',
    icon: 'icon-calendar-check',
    page: 'agenda',
    wiki: 'agenda'
  },
  {
    name: 'Comandas',
    description: 'Controle suas vendas com as comandas',
    icon: 'icon-receipts',
    page: 'comandas',
    wiki: 'comandas'
  },
  {
    name: 'Clientes',
    description: 'Tenha sua lista de clientes',
    icon: 'icon-user-friends',
    page: 'clientes',
    wiki: 'clientes'
  },
  {
    name: 'Serviços',
    description: 'Controle os serviços que seu salão faz',
    icon: 'icon-cut',
    page: 'servicos',
    wiki: 'servicos'
  },
  {
    name: 'Produtos',
    description: 'Organize seu estoque de produtos',
    icon: 'icon-flask-potion',
    page: 'produtos',
    wiki: 'produtos'
  },
  {
    name: 'Materiais',
    description: 'Controle a matéria-prima usada em seu salão',
    icon: 'icon-box-open',
    page: 'materiais',
    wiki: 'materiais'
  },
  {
    name: 'Financeiro',
    description: 'Mantenha suas finanças organizadas',
    icon: 'icon-chart-bar',
    page: 'financeiro',
    wiki: 'financeiro'
  },
  {
    name: 'Financeiro - Receitas',
    description: 'Gerencie tudo que o seu salão fatura',
    icon: 'icon-chart-line',
    page: 'financeiro/receitas',
    wiki: 'receitas'
  },
  {
    name: 'Financeiro - Despesas',
    description: 'Gerencie tudo que o seu salão gasta',
    icon: 'icon-chart-line-down',
    page: 'financeiro/despesas',
    wiki: 'despesas'
  },
  {
    name: 'Financeiro - Comissões',
    description: 'Controle as comissões de suas funcionárias',
    icon: 'icon-sack-dollar',
    page: 'financeiro/comissoes',
    wiki: 'comissoes'
  },
  {
    name: 'Financeiro - Gorjetas',
    description: 'Controle as gorjetas de suas funcionárias',
    icon: 'icon-calendar-check',
    page: 'financeiro/gorjetas',
    wiki: 'gorjetas'
  },
  {
    name: 'Funcionários',
    description: 'Suas funcionárias podem usar o sistema também',
    icon: 'icon-user-tie',
    page: 'funcionarios',
    wiki: 'funcionarios'
  },
  {
    name: 'Permissões',
    description: 'Defina o que suas funcionárias podem fazer no sistema',
    icon: 'icon-id-badge',
    page: 'permissoes',
    wiki: 'permissoes'
  },
  {
    name: 'Empresa',
    description: 'Altere o perfil de sua empresa no sistema',
    icon: 'icon-house',
    page: 'empresa',
    wiki: 'empresa'
  },
  {
    name: 'Perfil',
    description: 'Edite o seu perfil na plataforma',
    icon: 'icon-user',
    page: 'perfil',
    wiki: 'perfil'
  },
  {
    name: 'Notificações',
    description: 'Receba notificações sobre tudo que acontece em seu salão',
    icon: 'icon-bell',
    page: 'notificacoes',
    wiki: 'notificacoes'
  },
  {
    name: 'Assinatura',
    description: 'Tenha sua conta premium na gerencia beleza',
    icon: 'icon-star',
    page: 'empresa',
    wiki: 'assinatura'
  }
]
