const Module = () => {
  const methods = {}

  // Internal Variables

  const __welcomeSection = document.querySelector('section.welcome')

  const __quadro1 = __welcomeSection.querySelector('.welcome__quadro[data-slide="1"]')
  const __quadro2 = __welcomeSection.querySelector('.welcome__quadro[data-slide="2"]')
  const __quadro3 = __welcomeSection.querySelector('.welcome__quadro[data-slide="3"]')
  const __quadro4 = __welcomeSection.querySelector('.welcome__quadro[data-slide="4"]')

  const __continuarButton = document.querySelector('section.button .button__continuar')
  const __comecarButton = document.querySelector('section.button .button__comecar')

  // Methods

  methods.showComecarButton = (_index) => {
    __continuarButton.style.display = 'none'
    __comecarButton.style.display = 'block'
  }

  methods.showQuadro = (_index) => {
    methods.hideAllQuadro()
    const quadro = document.querySelector(methods._getQuadro(_index))
    quadro.style.display = 'flex'
  }

  methods.hideAllQuadro = () => {
    __quadro1.style.display = 'none'
    __quadro2.style.display = 'none'
    __quadro3.style.display = 'none'
    __quadro4.style.display = 'none'
  }

  methods._getQuadro = (_index) => {
    return `section.welcome .welcome__quadro[data-slide="${_index}"]`
  }

  return methods
}

module.exports = Module
