/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CONTINUE_BUTTON = require('./continue-button')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    CONTINUE_BUTTON().activeClickOnButtonContinue()
  }

  return methods
}

module.exports = Module
