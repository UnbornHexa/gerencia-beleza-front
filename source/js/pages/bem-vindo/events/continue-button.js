/* Requires */

const SECTION_QUADROS = require('../sections/quadros')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __buttonContinue = document.querySelector('section.button .button__continuar')

  // Methods

  methods.activeClickOnButtonContinue = () => {
    __buttonContinue.addEventListener('click', () => {
      const indexSlide = __buttonContinue.getAttribute('data-slide')
      const nextSlideIndex = Number(indexSlide) + 1

      if (indexSlide >= 3) {
        SECTION_QUADROS().showComecarButton()
      }

      SECTION_QUADROS().showQuadro(nextSlideIndex)
      __buttonContinue.setAttribute('data-slide', nextSlideIndex)
    })
  }

  return methods
}

module.exports = Module
