/* Requires */

const SERVICOS_REQUEST = require('../requests/servicos')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_SERVICOS = require('../extendeds/lista-servicos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_servico .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_servico .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idServico = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await SERVICOS_REQUEST().delServico(idServico)
        MODAL_DELETAR().hideModal()
        await LISTA_SERVICOS().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
