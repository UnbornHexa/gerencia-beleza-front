/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaServicos = SYMBOL().retrieve('lista_servicos')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaServicos.getState().filteredItems = __listaServicos.getState().items
          __listaServicos._loadItemsFromPage(1)
          return
        }

        __listaServicos.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaServicos.getState().items) {
          const item = __listaServicos.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaServicos.getState().filteredItems.push(item)
          }
        }

        __listaServicos._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
