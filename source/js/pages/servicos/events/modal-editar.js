/* Requires */

const SERVICOS_REQUEST = require('../requests/servicos')
const MODAL_EDITAR = require('../modals/modal-editar')
const LISTA_SERVICOS = require('../extendeds/lista-servicos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_servico .id input')
  const __editarButton = document.querySelector('section#modal_editar_servico .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idServico = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await SERVICOS_REQUEST().editServico(idServico, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_SERVICOS().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_servico input') &&
        !e.target.matches('section#modal_editar_servico textarea')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
