/* Requires */

const SERVICOS_REQUEST = require('../requests/servicos')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const LISTA_SERVICOS = require('../extendeds/lista-servicos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_servico .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await SERVICOS_REQUEST().addServico(modalData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_SERVICOS().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
