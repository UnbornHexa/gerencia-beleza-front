/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const SERVICOS_REQUEST = require('../requests/servicos')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_servico) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li>
      <span class="item__id" hidden>${_servico._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-cut"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_servico.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__comissao">${_servico.comissao_base}% de comissão</span>
        </div>
      </div>
      <div class="lista__preco">
        <span class="item__preco">${FORMAT().money(_servico.valor)}</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idServico = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const servico = await SERVICOS_REQUEST().getServicoById(idServico)
        MODAL_VISUALIZAR().importDataToModal(servico)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idServico = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const servico = await SERVICOS_REQUEST().getServicoById(idServico)
        MODAL_EDITAR().importDataToModal(servico)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idServico = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeServico = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_servico .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_servico .modal__header p')

      inputIdModal.value = idServico
      headerNomeModal.innerText = nomeServico
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__servicos')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_servicos', component)
  }

  methods.updateItems = async () => {
    const listaServicos = SYMBOL().retrieve('lista_servicos')

    try {
      const servicos = await SERVICOS_REQUEST().getServicos()
      listaServicos.importItems(servicos)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
