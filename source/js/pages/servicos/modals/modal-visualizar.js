/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_servico')

  const __nomeCollapsible = __modal.querySelector('.collapsible .nome span')
  const __descricaoCollapsible = __modal.querySelector('.collapsible .descricao span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')
  const __valorCollapsible = __modal.querySelector('.collapsible .valor span')
  const __comissaoCollapsible = __modal.querySelector('.collapsible .comissao__base span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __nomeCollapsible.innerHTML = '...'
    __descricaoCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'
    __valorCollapsible.innerHTML = '...'
    __comissaoCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    __nomeCollapsible.innerText = _data.nome || '...'
    __descricaoCollapsible.innerText = _data.descricao || '...'
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || '...'
    __valorCollapsible.innerText = FORMAT().money(_data.valor) || '...'
    __comissaoCollapsible.innerText = _data.comissao_base + '%' || '...'
  }

  return methods
}

module.exports = Module
