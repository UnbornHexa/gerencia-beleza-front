/* Requires */

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_deletar_servico')

  const __headerNomeP = __modal.querySelector('.modal__header p')
  const __idInput = __modal.querySelector('.id input')

  // Methods

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __idInput.value = ''
    __headerNomeP.innerHTML = '...'
  }

  return methods
}

module.exports = Module
