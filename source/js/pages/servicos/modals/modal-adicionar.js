/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_servico')

  const __nomeInput = __modal.querySelector('.nome input')
  const __precoInput = __modal.querySelector('.valor__preco input')
  const __comissaoInput = __modal.querySelector('.valor__comissao input')
  const __descricaoInput = __modal.querySelector('.descricao textarea')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value && __comissaoInput.value && __precoInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __nomeInput.value = ''
    __precoInput.value = ''
    __comissaoInput.value = ''
    __descricaoInput.value = ''
  }

  // Data

  methods.exportDataFromModal = () => {
    const servico = {}

    // required
    servico.nome = __nomeInput.value || ''
    servico.valor = FORMAT().unformatMoney(__precoInput.value) || 0
    servico.comissao_base = FORMAT().onlyNumber(__comissaoInput.value) || 0

    // optional
    if (__descricaoInput.value) servico.descricao = __descricaoInput.value || ''

    return servico
  }

  return methods
}

module.exports = Module
