/* Requires */

const MATERIAIS_REQUEST = require('../requests/materiais')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_MATERIAIS = require('../extendeds/lista-materiais')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_material .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_material .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idMaterial = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await MATERIAIS_REQUEST().delMaterial(idMaterial)
        MODAL_DELETAR().hideModal()
        await LISTA_MATERIAIS().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
