/* Requires */

const LISTA_MATERIAIS = require('../extendeds/lista-materiais')
const MATERIAIS_REQUEST = require('../requests/materiais')
const MODAL_REPOR = require('../modals/modal-repor')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_repor_material .id input')
  const __reporButton = document.querySelector('section#modal_repor_material .modal__button #repor')

  // Methods

  methods.activeClickOnReporButton = () => {
    __reporButton.addEventListener('click', async () => {
      const modalData = MODAL_REPOR().exportDadataFromModal()
      const idMaterial = __idInput.value

      const modalOk = MODAL_REPOR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __reporButton.setAttribute('disabled', true)
        await MATERIAIS_REQUEST().refillMaterial(idMaterial, modalData)
        MODAL_REPOR().hideModal()
        await LISTA_MATERIAIS().updateItems()
      } catch (e) { console.log(e) }

      __reporButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
