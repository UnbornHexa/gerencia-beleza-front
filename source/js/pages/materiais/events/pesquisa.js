/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaMateriais = SYMBOL().retrieve('lista_materiais')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaMateriais.getState().filteredItems = __listaMateriais.getState().items
          __listaMateriais._loadItemsFromPage(1)
          return
        }

        __listaMateriais.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaMateriais.getState().items) {
          const item = __listaMateriais.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaMateriais.getState().filteredItems.push(item)
          }
        }

        __listaMateriais._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
