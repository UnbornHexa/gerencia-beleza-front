/* Requires */

const MATERIAIS_REQUEST = require('../requests/materiais')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const LISTA_MATERIAIS = require('../extendeds/lista-materiais')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_material .modal__button #adicionar')

  // Methods

  methods.activeAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await MATERIAIS_REQUEST().addMaterial(modalData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_MATERIAIS().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
