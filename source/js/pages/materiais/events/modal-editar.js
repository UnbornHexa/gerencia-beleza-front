/* Requires */

const LISTA_MATERIAIS = require('../extendeds/lista-materiais')
const MATERIAIS_REQUEST = require('../requests/materiais')
const MODAL_EDITAR = require('../modals/modal-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_material .id input')
  const __editarButton = document.querySelector('section#modal_editar_material .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idMaterial = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFiels()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await MATERIAIS_REQUEST().editMaterial(idMaterial, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_MATERIAIS().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_material input') &&
        !e.target.matches('section#modal_editar_material textarea')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
