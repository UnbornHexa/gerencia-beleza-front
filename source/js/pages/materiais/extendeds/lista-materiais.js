/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')
const FORMAT = require('../../../global/helpers/format')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_REPOR = require('../modals/modal-repor')
const MODAL_DELETAR = require('../modals/modal-deletar')
const MATERIAIS_REQUEST = require('../requests/materiais')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_material) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li>
      <span class="item__id" hidden>${_material._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-box-open"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_material.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Repor -->
              <div class="opcoes__repor">
                <i class="icon-refill"></i>
                <span>Repor</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__estoque">${FORMAT().number(_material.quantidade)} unidades</span>
        </div>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idMaterial = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const material = await MATERIAIS_REQUEST().getMaterialById(idMaterial)
        MODAL_VISUALIZAR().importDataToModal(material)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuRepor () {
    const match = `${this.__id} .lista__opcoes .opcoes__repor`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_REPOR().showModal()
      const idMaterial = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const material = await MATERIAIS_REQUEST().getMaterialById(idMaterial)
        MODAL_REPOR().importDataToModal(material)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idMaterial = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const material = await MATERIAIS_REQUEST().getMaterialById(idMaterial)
        MODAL_EDITAR().importDataToModal(material)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idMaterial = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeMaterial = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_material .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_material .modal__header p')

      inputIdModal.value = idMaterial
      headerNomeModal.innerText = nomeMaterial
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__materiais')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuRepor()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_materiais', component)
  }

  methods.updateItems = async () => {
    const listaMateriais = SYMBOL().retrieve('lista_materiais')

    try {
      const materiais = await MATERIAIS_REQUEST().getMateriais()
      listaMateriais.importItems(materiais)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
