/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')

const LISTA_MATERIAIS = require('./extendeds/lista-materiais')
const SELECTO_FORMA_PAGAMENTO = require('./extendeds/selecto-forma-pagamento')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('materiais')
  ASIDE_PICTURE().load()

  LISTA_MATERIAIS().init()
  SELECTO_FORMA_PAGAMENTO().init('section#modal_repor_material .metodo .selecto__common', 'selecto_formaPagamento_repor')

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_MATERIAIS().updateItems()

  EVENTS().activeEvents()
}
start()
