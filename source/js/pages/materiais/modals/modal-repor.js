/* Requires */

const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_repor_material')

  const __idInput = __modal.querySelector('.id input')
  const __nomeInput = __modal.querySelector('.nome input')
  const __quantidadeInput = __modal.querySelector('.quantidade_reposta input')
  const __custoTotalInput = __modal.querySelector('.preco input')

  const __formaPagamentoSelecto = SYMBOL().retrieve('selecto_formaPagamento_repor')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (!__formaPagamentoSelecto.isEmpty() && __quantidadeInput.value && __custoTotalInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __nomeInput.value = ''
    __quantidadeInput.value = ''
    __custoTotalInput.value = ''

    __formaPagamentoSelecto.resetSelected()
  }

  // Data

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __nomeInput.value = _data.nome || ''
  }

  methods.exportDadataFromModal = () => {
    const nome = __nomeInput.value

    const material = {}
    const lancamento = {}

    material.quantidade = __quantidadeInput.value || 0

    lancamento.nome = `+${material.quantidade} ${nome}` || ''
    lancamento.tipo = 'Despesa'
    lancamento.valor = FORMAT().unformatMoney(__custoTotalInput.value) || 0
    lancamento.forma_pagamento = __formaPagamentoSelecto.value() || ''

    return {
      material,
      lancamento
    }
  }

  return methods
}

module.exports = Module
