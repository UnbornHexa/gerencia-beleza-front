/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_material')

  const __nomeCollapsible = __modal.querySelector('.collapsible .nome span')
  const __quantidadeCollapsible = __modal.querySelector('.collapsible .quantidade span')
  const __quantidadeMinimaCollapsible = __modal.querySelector('.collapsible .quantidade__minima span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __nomeCollapsible.innerHTML = '...'
    __quantidadeCollapsible.innerHTML = '...'
    __quantidadeMinimaCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    __nomeCollapsible.innerText = _data.nome || '...'
    __quantidadeCollapsible.innerText = FORMAT().number(_data.quantidade) || '...'
    __quantidadeMinimaCollapsible.innerText = FORMAT().number(_data.quantidade_minima) || '...'
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || '...'
  }

  return methods
}

module.exports = Module
