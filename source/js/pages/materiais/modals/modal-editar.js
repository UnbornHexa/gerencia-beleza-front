/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_material')

  const __idInput = __modal.querySelector('.id input')
  const __nomeInput = __modal.querySelector('.nome input')
  const __estoqueAtualInput = __modal.querySelector('.estoque__atual input')
  const __estoqueMinimoInput = __modal.querySelector('.estoque__minimo input')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFiels = () => {
    if (__nomeInput.value && __estoqueAtualInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __nomeInput.value = ''
    __estoqueAtualInput.value = ''
    __estoqueMinimoInput.value = ''

    __editarButton.setAttribute('disabled', true)
  }

  // Data

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __nomeInput.value = _data.nome || ''
    __estoqueAtualInput.value = _data.quantidade || ''
    __estoqueMinimoInput.value = _data.quantidade_minima || ''
  }

  methods.exportDataFromModal = () => {
    const material = {}

    // required
    material.nome = __nomeInput.value || ''
    material.quantidade = __estoqueAtualInput.value || 0

    // optional
    if (__estoqueMinimoInput.value) material.quantidade_minima = __estoqueMinimoInput.value || 0

    return material
  }

  return methods
}

module.exports = Module
