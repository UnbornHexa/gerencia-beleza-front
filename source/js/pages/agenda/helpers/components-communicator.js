/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const HORARIOS_REQUEST = require('../requests/horarios')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateAgenda = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const selectoMultipleFuncionarios = SYMBOL().retrieve('selecto_multiple_funcionarios')

    const idFuncionarios = selectoMultipleFuncionarios.value().map(_funcionario => { return _funcionario.value }).join(',')
    await methods.getHorarios(dateSelector.value(), idFuncionarios)
  }

  methods.getHorarios = async (_date, _funcionarios) => {
    const date = `${_date.year}-${_date.month.toString().padStart(2, '0')}`
    const agenda = SYMBOL().retrieve('agenda_principal')

    agenda.setDate(_date.year, _date.month)
    try {
      const horarios = await HORARIOS_REQUEST().getHorarios(date, _funcionarios)
      agenda.importSchedules(horarios)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
