/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SERVICOS_REQUEST = require('../requests/servicos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __servicosModifieldSelecto1Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_1')
  const __servicosModifieldSelecto2Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_2')
  const __servicosModifieldSelecto3Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_3')
  const __servicosModifieldSelecto4Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_4')
  const __servicosModifieldSelecto5Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_5')
  const __servicosModifieldSelecto6Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_6')
  const __servicosModifieldSelecto7Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_7')
  const __servicosModifieldSelecto8Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_8')
  const __servicosModifieldSelecto9Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_9')
  const __servicosModifieldSelecto10Adicionar = SYMBOL().retrieve('selecto_servicos_adicionar_10')

  const __servicosModifieldSelecto1Editar = SYMBOL().retrieve('selecto_servicos_editar_1')
  const __servicosModifieldSelecto2Editar = SYMBOL().retrieve('selecto_servicos_editar_2')
  const __servicosModifieldSelecto3Editar = SYMBOL().retrieve('selecto_servicos_editar_3')
  const __servicosModifieldSelecto4Editar = SYMBOL().retrieve('selecto_servicos_editar_4')
  const __servicosModifieldSelecto5Editar = SYMBOL().retrieve('selecto_servicos_editar_5')
  const __servicosModifieldSelecto6Editar = SYMBOL().retrieve('selecto_servicos_editar_6')
  const __servicosModifieldSelecto7Editar = SYMBOL().retrieve('selecto_servicos_editar_7')
  const __servicosModifieldSelecto8Editar = SYMBOL().retrieve('selecto_servicos_editar_8')
  const __servicosModifieldSelecto9Editar = SYMBOL().retrieve('selecto_servicos_editar_9')
  const __servicosModifieldSelecto10Editar = SYMBOL().retrieve('selecto_servicos_editar_10')

  // Methods

  methods.updateOptions = async () => {
    try {
      const servicos = await SERVICOS_REQUEST().getServicos()

      const options = servicos.map(_servico => {
        return { value: _servico._id, text: _servico.nome }
      })

      if (options.length === 0) {
        const html = `
        <p>Não há serviços cadastrados ainda.</p>
        <a href="./servicos">
          <button type="button">Cadastrar Serviço</button>
        </a>
        `
        __servicosModifieldSelecto1Adicionar._alertEmpty(html)
        __servicosModifieldSelecto1Editar._alertEmpty(html)
      }

      __servicosModifieldSelecto1Adicionar.importOptions(options)
      __servicosModifieldSelecto2Adicionar.importOptions(options)
      __servicosModifieldSelecto3Adicionar.importOptions(options)
      __servicosModifieldSelecto4Adicionar.importOptions(options)
      __servicosModifieldSelecto5Adicionar.importOptions(options)
      __servicosModifieldSelecto6Adicionar.importOptions(options)
      __servicosModifieldSelecto7Adicionar.importOptions(options)
      __servicosModifieldSelecto8Adicionar.importOptions(options)
      __servicosModifieldSelecto9Adicionar.importOptions(options)
      __servicosModifieldSelecto10Adicionar.importOptions(options)

      __servicosModifieldSelecto1Editar.importOptions(options)
      __servicosModifieldSelecto2Editar.importOptions(options)
      __servicosModifieldSelecto3Editar.importOptions(options)
      __servicosModifieldSelecto4Editar.importOptions(options)
      __servicosModifieldSelecto5Editar.importOptions(options)
      __servicosModifieldSelecto6Editar.importOptions(options)
      __servicosModifieldSelecto7Editar.importOptions(options)
      __servicosModifieldSelecto8Editar.importOptions(options)
      __servicosModifieldSelecto9Editar.importOptions(options)
      __servicosModifieldSelecto10Editar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
