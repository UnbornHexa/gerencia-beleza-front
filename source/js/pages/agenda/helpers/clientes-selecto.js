/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const CLIENTES_REQUEST = require('../requests/clientes')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __clientesSelectoAdicionar = SYMBOL().retrieve('selecto_clientes_adicionar')
  const __clientesSelectoEditar = SYMBOL().retrieve('selecto_clientes_editar')

  // Methods

  methods.updateOptions = async () => {
    try {
      const clientes = await CLIENTES_REQUEST().getClientes()

      const options = clientes.map(_cliente => {
        return { value: _cliente._id, text: _cliente.nome }
      })

      if (options.length <= 1) {
        const html = `
        <p>Não há clientes cadastrados ainda.</p>
        <a href="./clientes">
          <button type="button">Cadastrar Cliente</button>
        </a>
        `
        __clientesSelectoAdicionar._alertEmpty(html)
        __clientesSelectoEditar._alertEmpty(html)
      }

      __clientesSelectoAdicionar.importOptions(options)
      __clientesSelectoEditar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
