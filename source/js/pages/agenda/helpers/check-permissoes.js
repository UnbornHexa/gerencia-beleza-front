/* Require */

const TOKEN = require('../../../global/helpers/token')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __funcionariosSelectoAdicionar = SYMBOL().retrieve('selecto_funcionarios_adicionar')
  const __funcionariosSelectoEditar = SYMBOL().retrieve('selecto_funcionarios_editar')
  const __funcionariosSelectoMultiple = SYMBOL().retrieve('selecto_multiple_funcionarios')

  // Methods

  methods.checkAccessToOthersAgendas = () => {
    const permissoes = TOKEN().getPermissaoFromToken()
    if (permissoes.agenda.view_others === true) return true

    __funcionariosSelectoMultiple.disable()
    __funcionariosSelectoAdicionar.disable()
    __funcionariosSelectoEditar.disable()
  }

  return methods
}

module.exports = Module
