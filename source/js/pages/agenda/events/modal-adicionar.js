/* Requires */

const MODAL_ADICIONAR = require('../modals/modal-adicionar')
const HORARIOS_REQUEST = require('../requests/horarios')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_horario')

  const __adicionarButton = __modal.querySelector('.modal__button #adicionar')
  const __adicionarServicoButton = __modal.querySelector('#adicionar__servico')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_ADICIONAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await HORARIOS_REQUEST().addHorario(modalData)
        MODAL_ADICIONAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  methods.activeClickOnAdicionarServicoButton = () => {
    __adicionarServicoButton.addEventListener('click', () => {
      const modifieldSelectos = __modal.querySelector('.selecto__common.hidden')
      if (modifieldSelectos) modifieldSelectos.classList.remove('hidden')
    })
  }

  return methods
}

module.exports = Module
