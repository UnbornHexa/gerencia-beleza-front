/* Requires */

const MODAL_EDITAR = require('../modals/modal-editar')
const HORARIOS_REQUEST = require('../requests/horarios')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_horario')

  const __idInput = __modal.querySelector('.informacoes__id input')

  const __editarButton = __modal.querySelector('.modal__button #editar')
  const __adicionarServicoButton = __modal.querySelector('#adicionar__servico')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idHorario = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_EDITAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await HORARIOS_REQUEST().editHorario(idHorario, modalData)
        MODAL_EDITAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  methods.activeClickOnAdicionarServicoButton = () => {
    __adicionarServicoButton.addEventListener('click', () => {
      const modifieldSelectos = __modal.querySelector('.selecto__common.hidden')
      if (modifieldSelectos) modifieldSelectos.classList.remove('hidden')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_horario input') &&
        !e.target.matches('section#modal_editar_horario textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_horario .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_horario .datepicker input')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
