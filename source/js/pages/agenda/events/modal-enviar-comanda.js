/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const MODAL_ENVIAR_COMANDA = require('../modals/modal-enviar-comanda')
const COMANDAS_REQUEST = require('../requests/comandas')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_enviar_comanda')

  const __criarComandaButton = __modal.querySelector('.comandas .botao #criar__comanda')
  const __enviarButton = __modal.querySelector('.modal__button #enviar')

  const __comandasSlide = SYMBOL().retrieve('slide_items_comandas_enviar_comanda')

  // Methods

  methods.activeClickOnEnviarButton = () => {
    __enviarButton.addEventListener('click', async () => {
      const modalData = MODAL_ENVIAR_COMANDA().exportServicosFromHorario()
      const idComanda = __comandasSlide.value().id_comanda

      try {
        __enviarButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().importHorario(idComanda, modalData)
        MODAL_ENVIAR_COMANDA().hideModal()
      } catch (e) { console.log(e) }

      __enviarButton.removeAttribute('disabled')
    })
  }

  methods.activeClickOnCriarComandaButton = () => {
    __criarComandaButton.addEventListener('click', async () => {
      const modalData = MODAL_ENVIAR_COMANDA().generateNewComandaData()

      try {
        __criarComandaButton.setAttribute('disabled', true)
        await COMANDAS_REQUEST().addComanda(modalData)
        MODAL_ENVIAR_COMANDA().hideModal()
      } catch (e) { console.log(e) }

      __criarComandaButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
