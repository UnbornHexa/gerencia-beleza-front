/* Requires */

const HORARIOS_REQUEST = require('../requests/horarios')
const MODAL_DELETAR = require('../modals/modal-deletar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_horario .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_horario .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idHorario = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await HORARIOS_REQUEST().delHorario(idHorario)
        MODAL_DELETAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
