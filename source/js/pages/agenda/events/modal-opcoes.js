/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')

const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_ENVIAR_COMANDA = require('../modals/modal-enviar-comanda')
const MODAL_OPCOES = require('../modals/modal-opcoes')

const HORARIOS_REQUEST = require('../requests/horarios')
const COMANDAS_REQUEST = require('../requests/comandas')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_opcoes_horario')

  const __idHorarioDiv = __modal.querySelector('.content__horario .horario__id')
  const __clienteHorarioDiv = __modal.querySelector('.content__horario .horario__cliente')
  const __funcionarioHorarioDiv = __modal.querySelector('.content__horario .horario__funcionario')
  const __telefoneClienteDiv = __modal.querySelector('.content__horario .horario__telefone')
  const __periodoHorarioDiv = __modal.querySelector('.content__horario .horario__periodo')

  const __deletarOption = __modal.querySelector('.content__opcoes .opcao__deletar')
  const __editarOption = __modal.querySelector('.content__opcoes .opcao__editar')
  const __visualizarOption = __modal.querySelector('.content__opcoes .opcao__visualizar')
  const __whatsappOption = __modal.querySelector('.content__opcoes .opcao__enviar__whatsapp')
  const __enviarComandaOption = __modal.querySelector('.content__opcoes .opcao__enviar__comanda')

  const __marcadoTag = __modal.querySelector('.tags .tag__orange')
  const __concluidoTag = __modal.querySelector('.tags .tag__green')
  const __faltouTag = __modal.querySelector('.tags .tag__red')

  // Methods

  methods.activeClickOnMenuDeletar = () => {
    __deletarOption.addEventListener('click', () => {
      const idHorario = __idHorarioDiv.innerText
      const periodoHorario = __periodoHorarioDiv.innerText
      const clienteHorario = __clienteHorarioDiv.innerText

      MODAL_OPCOES().hideModal()
      MODAL_DELETAR().showModal()

      const inputIdModal = document.querySelector('section#modal_deletar_horario .id input')
      const headerDataModal = document.querySelector('section#modal_deletar_horario .modal__header p')

      inputIdModal.value = idHorario
      headerDataModal.innerText = `${periodoHorario} ( ${clienteHorario} )`
    })
  }

  methods.activeClickOnMenuVisualizar = () => {
    __visualizarOption.addEventListener('click', async () => {
      const idHorario = __idHorarioDiv.innerText

      MODAL_OPCOES().hideModal()
      MODAL_VISUALIZAR().showModal()

      try {
        const horario = await HORARIOS_REQUEST().getHorarioById(idHorario)
        MODAL_VISUALIZAR().importDataToModal(horario)
      } catch (e) { console.log(e) }
    })
  }

  methods.activeClickOnMenuEditar = () => {
    __editarOption.addEventListener('click', async () => {
      const idHorario = __idHorarioDiv.innerText

      MODAL_OPCOES().hideModal()
      MODAL_EDITAR().showModal()

      try {
        const horario = await HORARIOS_REQUEST().getHorarioById(idHorario)
        MODAL_EDITAR().importDataToModal(horario)
      } catch (e) { console.log(e) }
    })
  }

  // Additional Methods

  methods.activeClickOnMenuEnviarComanda = () => {
    __enviarComandaOption.addEventListener('click', async () => {
      const idHorario = __idHorarioDiv.innerText
      const funcionarioHorario = __funcionarioHorarioDiv.innerText

      MODAL_OPCOES().hideModal()
      MODAL_ENVIAR_COMANDA().showModal()

      try {
        const horario = await HORARIOS_REQUEST().getHorarioOnlyClienteAndServicos(idHorario)
        const idCliente = (horario.id_cliente) ? horario.id_cliente._id : ''

        const comandas = await COMANDAS_REQUEST().getComandas(idCliente)
        const funcionario = funcionarioHorario

        MODAL_ENVIAR_COMANDA().importHorarioToModal(horario)
        MODAL_ENVIAR_COMANDA().importComandasAbertasToModal(comandas, funcionario)
      } catch (e) { console.log(e) }
    })
  }

  methods.activeClickOnMenuWhatsapp = () => {
    __whatsappOption.addEventListener('click', async () => {
      const cliente = __clienteHorarioDiv.innerText
      const telefone = __telefoneClienteDiv.innerText.replace(/[-|(|)|\s]/g, '')
      const periodo = __periodoHorarioDiv.innerText.split(' - ')[0]

      if (!telefone) {
        MESSAGEBOX().alert('O cliente não possui telefone cadastrado')
        return
      }

      if (window.screen.availWidth > 900) {
        MESSAGEBOX().alert('Para utilizar esse recurso em computadores você precisa ter o Whatsapp instalado.')
      }

      const mensagem = `Olá, ${cliente}. Posso confirmar seu horário hoje às ${periodo}?`
      const linkWhatsapp = `whatsapp://send?phone=+55${telefone}&text=${mensagem}`
      window.location.assign(linkWhatsapp)
    })
  }

  // Change Estadia

  methods.activeClickOnMenuEstadiaTag = () => {
    const editEstadiaHorario = async (_estadia) => {
      const idHorario = __idHorarioDiv.innerText
      const obj = { estadia: _estadia }

      try {
        const editHorarioOk = await HORARIOS_REQUEST().editHorario(idHorario, obj)
        if (!editHorarioOk) return

        MODAL_OPCOES().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
      } catch (e) { console.log(e) }
    }

    __marcadoTag.addEventListener('click', () => { editEstadiaHorario('Marcado') })
    __concluidoTag.addEventListener('click', () => { editEstadiaHorario('Concluido') })
    __faltouTag.addEventListener('click', () => { editEstadiaHorario('Faltou') })
  }

  return methods
}

module.exports = Module
