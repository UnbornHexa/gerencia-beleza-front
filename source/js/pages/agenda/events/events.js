/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const SECTION_MAIN = require('./section_main')
const MODAL_ADICIONAR = require('./modal-adicionar')
const MODAL_EDITAR = require('./modal-editar')
const MODAL_DELETAR = require('./modal-deletar')
const MODAL_OPCOES = require('./modal-opcoes')
const MODAL_ENVIAR_COMANDA = require('./modal-enviar-comanda')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    SECTION_MAIN().activeClickOnAdicionarButton()

    MODAL_ADICIONAR().activeClickOnAdicionarButton()
    MODAL_ADICIONAR().activeClickOnAdicionarServicoButton()

    MODAL_EDITAR().activeClickOnEditarButton()
    MODAL_EDITAR().activeClickOnAdicionarServicoButton()
    MODAL_EDITAR().activeChangeOnModal()

    MODAL_DELETAR().activeClickOnDeletarButton()

    MODAL_OPCOES().activeClickOnMenuDeletar()
    MODAL_OPCOES().activeClickOnMenuVisualizar()
    MODAL_OPCOES().activeClickOnMenuEditar()
    MODAL_OPCOES().activeClickOnMenuEnviarComanda()
    MODAL_OPCOES().activeClickOnMenuWhatsapp()
    MODAL_OPCOES().activeClickOnMenuEstadiaTag()

    MODAL_ENVIAR_COMANDA().activeClickOnCriarComandaButton()
    MODAL_ENVIAR_COMANDA().activeClickOnEnviarButton()

    TOGGLE().activeClickOnToggle()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
