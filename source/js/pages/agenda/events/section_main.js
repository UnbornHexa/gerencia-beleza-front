/* Requires */

const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section.agenda__top button#adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      MODAL_ADICIONAR().showModal()
    })
  }

  return methods
}

module.exports = Module
