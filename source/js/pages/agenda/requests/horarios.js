/* Requires */

const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __idUsuario = TOKEN().getIdUsuarioFromToken()
  const __url = `${HOSTS().url.api_plataforma}/horarios`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getHorarios = async (_date, _idsFuncionarios) => {
    if (!_idsFuncionarios) _idsFuncionarios = __idUsuario

    const url = `${__url}/${__idEmpresa}?data=${_date}&id_usuario=${_idsFuncionarios}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getHorarioById = async (_idHorario) => {
    const url = `${__url}/${__idEmpresa}/${_idHorario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.addHorario = async (_data) => {
    const url = `${__url}/${__idEmpresa}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, __headersWithToken, _response => {
        if (_response.status === 201) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editHorario = async (_idHorario, _data) => {
    const url = `${__url}/${__idEmpresa}/${_idHorario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PUT', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.delHorario = async (_idHorario) => {
    const url = `${__url}/${__idEmpresa}/${_idHorario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'DELETE', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getHorarioOnlyClienteAndServicos = async (_idHorario) => {
    const url = `${__url}/only-cliente-and-servicos/${__idEmpresa}/${_idHorario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
