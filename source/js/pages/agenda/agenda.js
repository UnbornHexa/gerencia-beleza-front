/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_PERMISSIONS = require('./helpers/check-permissoes')
const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_SELECTO_SERVICOS = require('./helpers/servicos-selecto')
const HELPER_SELECTO_CLIENTES = require('./helpers/clientes-selecto')
const HELPER_SELECTO_FUNCIONARIOS = require('./helpers/funcionarios-selecto')
const HELPER_COMPONENTS_COMMUNICATOR = require('./helpers/components-communicator')

const AGENDA = require('./extendeds/agenda')
const DATE_SELECTOR = require('./extendeds/date-selector')
const SLIDE_ITEMS_COMANDAS = require('./extendeds/slide-items-comandas')
const STORAGE_SERVICOS = require('./extendeds/storage-servicos')
const MODIFIELD_SELECTO = require('./extendeds/modifield-selecto')
const SELECTO = require('./extendeds/selecto')
const SELECTO_MULTIPLE = require('./extendeds/selecto-multiple')
const DATEPICKER = require('./extendeds/datepicker')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('agenda')
  ASIDE_PICTURE().load()

  DATE_SELECTOR().init()
  AGENDA().init(DATE_SELECTOR().getDate())

  SELECTO_MULTIPLE().init('section.agenda__top div.seletor .funcionario .selecto__multiple', 'selecto_multiple_funcionarios')

  SELECTO().init('section#modal_adicionar_horario .informacoes__cliente .selecto__common', 'selecto_clientes_adicionar')
  SELECTO().init('section#modal_editar_horario .informacoes__cliente .selecto__common', 'selecto_clientes_editar')
  SELECTO().init('section#modal_adicionar_horario .informacoes__funcionario .selecto__common', 'selecto_funcionarios_adicionar')
  SELECTO().init('section#modal_editar_horario .informacoes__funcionario .selecto__common', 'selecto_funcionarios_editar')

  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="1"]', 'selecto_servicos_adicionar_1')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="2"]', 'selecto_servicos_adicionar_2')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="3"]', 'selecto_servicos_adicionar_3')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="4"]', 'selecto_servicos_adicionar_4')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="5"]', 'selecto_servicos_adicionar_5')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="6"]', 'selecto_servicos_adicionar_6')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="7"]', 'selecto_servicos_adicionar_7')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="8"]', 'selecto_servicos_adicionar_8')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="9"]', 'selecto_servicos_adicionar_9')
  MODIFIELD_SELECTO().init('section#modal_adicionar_horario .servicos__agrupador .selecto__common[data-id="10"]', 'selecto_servicos_adicionar_10')

  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="1"]', 'selecto_servicos_editar_1')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="2"]', 'selecto_servicos_editar_2')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="3"]', 'selecto_servicos_editar_3')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="4"]', 'selecto_servicos_editar_4')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="5"]', 'selecto_servicos_editar_5')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="6"]', 'selecto_servicos_editar_6')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="7"]', 'selecto_servicos_editar_7')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="8"]', 'selecto_servicos_editar_8')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="9"]', 'selecto_servicos_editar_9')
  MODIFIELD_SELECTO().init('section#modal_editar_horario .servicos__agrupador .selecto__common[data-id="10"]', 'selecto_servicos_editar_10')

  SLIDE_ITEMS_COMANDAS().init('section#modal_enviar_comanda .comandas .slide__items', 'slide_items_comandas_enviar_comanda')
  STORAGE_SERVICOS().init('storage_servicos_enviar_comanda')

  DATEPICKER().init('section#modal_adicionar_horario .servicos__data .datepicker', 'datapicker_adicionar')
  DATEPICKER().init('section#modal_editar_horario .servicos__data .datepicker', 'datapicker_editar')

  HELPER_PERMISSIONS().checkAccessToOthersAgendas()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
  HELPER_SELECTO_SERVICOS().updateOptions()
  HELPER_SELECTO_CLIENTES().updateOptions()
  HELPER_SELECTO_FUNCIONARIOS().updateOptions()

  EVENTS().activeEvents()
}
start()
