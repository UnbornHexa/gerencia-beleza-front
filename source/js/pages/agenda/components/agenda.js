class Agenda {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      schedules: [],
      date: {
        year: 0,
        month: 0,
        numberOfDays: 0
      }
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () {
    const year = this.__state.date.year
    const month = this.__state.date.month
    let html = ''

    for (let index = 1; index <= this.__state.date.numberOfDays; index++) {
      const weekDay = new Date(year, month - 1, index).getDay()
      const dayName = this._getDayName(weekDay + 1)

      html += `
      <div class="agenda__dia" data-dia="${index}">
        <div class="dia__header">${dayName} - ${index}</div>
        <div class="dia__body"></div>
      </div>
      `

      this.__component.innerHTML = html
    }

    if (this.__state.date.year === this._getCurrentDate().year &&
    this.__state.date.month === this._getCurrentDate().month) {
      this.scrollTo(this._getCurrentDate().day)
      this.scrollDayAsToday(this._getCurrentDate().day)
    } else {
      this.scrollTo(1)
    }
  }

  _resetAgenda () {
    this.__component.innerHTML = ''
  }

  // Set Agenda

  setDate (_year, _month) {
    if (!_year && !_month) return

    this.__state.date.year = Number(_year)
    this.__state.date.month = Number(_month)
    this._calcDaysOfDate()
  }

  scrollDayAsToday (_day) {
    const dayDiv = this.__component.querySelector(`.agenda__dia[data-dia="${_day}"] .dia__header`)
    dayDiv.classList.add('hoje')
  }

  scrollTo (_day) {
    const pointX = this.__component.querySelector(`.agenda__dia[data-dia="${_day}"]`).offsetLeft
    this.__component.scrollTo({ top: 0, left: pointX, behavior: 'smooth' })
  }

  // Schedules

  importSchedules (_schedules) {
    this._resetAgenda()
    this.render()

    if (!_schedules) return

    for (const index in _schedules) {
      const schedule = _schedules[index]
      const day = Number(schedule.data.split('-')[2])
      this._addScheduleOnDay(day, schedule)
    }
  }

  _addScheduleOnDay (_day, _data) {
    const dayDiv = this.__component.querySelector(`.agenda__dia[data-dia="${_day}"] .dia__body`)
    let classe = ''
    let nomeCliente = ''
    let nomeFuncionario = ''
    let telefoneCliente = ''

    nomeFuncionario = (_data.id_usuario === null) ? 'Funcionário deletado' : _data.id_usuario.nome
    nomeCliente = (_data.id_cliente === null) ? 'Cliente deletado' : _data.id_cliente.nome
    telefoneCliente = (_data.id_cliente === null) ? '' : _data.id_cliente.contato.telefone1

    if (_data.estadia === 'Marcado') classe = 'orange'
    if (_data.estadia === 'Concluido') classe = 'green'
    if (_data.estadia === 'Faltou') classe = 'red'

    dayDiv.innerHTML += `
    <div class="horario">
      <div class="horario__id" hidden>${_data._id}</div>
      <div class="horario__telefone" hidden>${telefoneCliente}</div>
      <div class="horario__periodo">${_data.hora_inicial} - ${_data.hora_final}</div>
      <div class="horario__funcionario">${nomeFuncionario}</div>
      <div class="horario__cliente">${nomeCliente}</div>
      <div class="horario__estadia ${classe}">${_data.estadia}</div>
    </div>
    `
  }

  // Date Functions

  _calcDaysOfDate () {
    const year = this.__state.date.year
    const month = this.__state.date.month
    const numberOfDays = new Date(year, month, 0).getDate()
    this.__state.date.numberOfDays = numberOfDays
  }

  _getMonthName (_month) {
    if (!_month) return

    const monthsName = [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro'
    ]

    return monthsName[_month - 1]
  }

  _getDayName (_day) {
    if (!_day) return

    const daysName = [
      'Domingo',
      'Segunda',
      'Terça',
      'Quarta',
      'Quinta',
      'Sexta',
      'Sábado'
    ]

    return daysName[_day - 1]
  }

  _getCurrentDate () {
    return {
      day: new Date().getDate(),
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear()
    }
  }

  // Events

  activeOnDrag () {
    let isDown = false
    let startX
    let scrollLeft

    this.__component.addEventListener('mousedown', (e) => {
      isDown = true

      startX = e.pageX - this.__component.offsetLeft
      scrollLeft = this.__component.scrollLeft
    })

    this.__component.addEventListener('mouseleave', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mouseup', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mousemove', (e) => {
      if (!isDown) return
      e.preventDefault()

      const x = e.pageX - this.__component.offsetLeft
      const walk = (x - startX) * 2
      this.__component.scrollLeft = scrollLeft - walk
      this.__component.classList.add('drag')
    })
  }
}

module.exports = Agenda
