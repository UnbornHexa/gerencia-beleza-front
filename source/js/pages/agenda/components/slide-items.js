class SlideItems {
  constructor (_id) {
    this.__id = _id
    this.__component = document.querySelector(_id)
    this.__state = {
      items: [],
      selected: {}
    }
  }

  // State

  getState () {
    return this.__state
  }

  // Build Component

  render () { }

  reset () {
    this.__component.innerHTML = ''
    this.getState().items = []
    this.getState().selected = {}
  }

  // Items

  addItem (_item) {
    if (!_item) return

    this.getState().items.push(_item)
    this.render()
  }

  selectItem (_item) {
    this.resetSelection()

    const idComanda = _item.querySelector('.comanda__id').innerText
    this.getState().selected = { id_comanda: idComanda }

    _item.classList.add('active')
  }

  resetSelection () {
    this.getState().selected = {}

    const items = this.__component.querySelectorAll('.comanda')
    for (const item of items) {
      if (item) item.classList.remove('active')
    }
  }

  // Utils

  value () {
    return this.getState().selected
  }

  // Events

  activeOnDrag () {
    let isDown = false
    let startX
    let scrollLeft

    this.__component.addEventListener('mousedown', (e) => {
      isDown = true

      startX = e.pageX - this.__component.offsetLeft
      scrollLeft = this.__component.scrollLeft
    })

    this.__component.addEventListener('mouseleave', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mouseup', () => {
      isDown = false
      this.__component.classList.remove('drag')
    })

    this.__component.addEventListener('mousemove', (e) => {
      if (!isDown) return
      e.preventDefault()

      const x = e.pageX - this.__component.offsetLeft
      const walk = (x - startX) * 2
      this.__component.scrollLeft = scrollLeft - walk
      this.__component.classList.add('drag')
    })
  }
}

module.exports = SlideItems
