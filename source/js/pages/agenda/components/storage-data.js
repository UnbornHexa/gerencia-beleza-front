class StorageData {
  constructor () {
    this.__state = {
      value: []
    }
  }

  reset () {
    this.__state.value = []
  }

  setValue (_value) {
    if (!_value) return

    this.__state.value = _value
  }

  getValue () {
    return this.__state.value
  }
}

module.exports = StorageData
