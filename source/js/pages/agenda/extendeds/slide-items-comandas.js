/* Requires */

const SLIDE_ITEMS_COMPONENT = require('../components/slide-items')
const MODAL_ENVIAR_COMANDA = require('../modals/modal-enviar-comanda')

const FORMAT = require('../../../global/helpers/format')
const CONVERT = require('../../../global/helpers/convert')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class SlideItemsExtended extends SLIDE_ITEMS_COMPONENT {
  render () {
    let html = ''

    for (const index in this.__state.items) {
      const item = this.__state.items[index]

      const cliente = (item.cliente) ? item.cliente.nome : 'Cliente Deletado'

      html += `
      <div class="comanda">
        <div class="comanda__id" hidden>${item.id}</div>
        <div class="comanda__numero">Comanda ${item.nome}</div>
        <div class="comanda__data">${CONVERT().dateBR(item.data)}</div>
        <div class="comanda__cliente">${cliente}</div>
        <div class="comanda__funcionario">${item.funcionario}</div>
        <div class="comanda__total">${FORMAT().money(item.valor_total)}</div>
      </div>
      `
    }

    this.__component.innerHTML = html
  }

  importItems (_comandas, _funcionario) {
    for (const index in _comandas) {
      const comanda = _comandas[index]

      const item = {
        id: comanda._id,
        nome: comanda.nome,
        cliente: comanda.id_cliente,
        funcionario: _funcionario,
        data: comanda.data,
        valor_total: comanda.valor_total
      }
      this.addItem(item)
    }
  }

  activeClickOnItem () {
    document.addEventListener('click', (e) => {
      if (!e.target.matches(`${this.__id} .comanda`)) return

      this.selectItem(e.target)
      MODAL_ENVIAR_COMANDA().activeButtonEnviar()
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new SlideItemsExtended(_id)

    component.activeOnDrag()
    component.activeClickOnItem()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
