/* Requires */

const DATE_SELECTOR_COMPONENT = require('../../../global/components/date-selector')
const SYMBOL = require('../../../global/helpers/symbols')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Extended Class */

class DateSelectorExtended extends DATE_SELECTOR_COMPONENT {
  activeClickOnNextArrow () {
    document.addEventListener('click', (e) => {
      const arrowNext = this.__component.querySelector('.seta__avancar')
      if (e.target !== arrowNext) return

      this._nextMonth()
      HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
    })
  }

  activeClickOnPreviousArrow () {
    document.addEventListener('click', (e) => {
      const arrowPrevious = this.__component.querySelector('.seta__voltar')
      if (e.target !== arrowPrevious) return

      this._previousMonth()
      HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new DateSelectorExtended('section.top div.date__selector')

    component.init()
    component.render()

    component.activeClickOnNextArrow()
    component.activeClickOnPreviousArrow()

    SYMBOL().subscribe('date_selector_principal', component)
  }

  methods.getDate = () => {
    const component = SYMBOL().retrieve('date_selector_principal')
    const date = component.getState().selectedDate

    return {
      year: date.year,
      month: date.month
    }
  }

  return methods
}

module.exports = Module
