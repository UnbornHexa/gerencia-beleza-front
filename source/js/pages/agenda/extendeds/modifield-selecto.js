/* Requires */

const SELECTO_COMPONENT = require('../../../global/components/selecto')
const SYMBOL = require('../../../global/helpers/symbols')

/* Extended Class */

class ModifiedSelecto extends SELECTO_COMPONENT {
  render () {
    this.__component.innerHTML = `
    <div class="selected"></div>
    <div class="dropdown">
      <div class="dropdown__busca input">
        <input type="text" placeholder="Busque...">
      </div>
      <div class="dropdown__opcoes"></div>
      <div class="alert__empty"></div>
    </div>
    <div class="remove">
      <i class="icon-times"></i>
    </div>
    `
  }

  activeClickOnRemoveButton () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.remove')
      if (e.target !== matchElement) return
      if (e.target.parentElement.getAttribute('data-id') === '1') return

      this.resetSelected()
      e.target.parentElement.classList.add('hidden')
    })
  }

  hidden () {
    this.__component.classList.add('hidden')
  }

  show () {
    this.__component.classList.remove('hidden')
  }

  setOptionsWithEstados () {}
  setOptionsWithFormaPagamento () {}
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new ModifiedSelecto(_id)

    component.render()

    component.activeClickOnSelecto()
    component.activeHiddenOnLostFocus()
    component.activeClickOnOption()
    component.activeKeyupOnSearchbar()
    component.activeClickOnRemoveButton()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
