/* Requires */

const STORAGE_DATA_COMPONENT = require('../components/storage-data')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_key) => {
    const component = new STORAGE_DATA_COMPONENT()
    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
