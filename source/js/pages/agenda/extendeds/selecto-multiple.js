/* Requires */

const SELECTO_MULTIPLE_COMPONENT = require('../../../global/components/selecto-multiple')
const SYMBOL = require('../../../global/helpers/symbols')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Extended Class */

class SelectoMultiple extends SELECTO_MULTIPLE_COMPONENT {
  _loadOptions () {
    this._cleanOptions()

    const optionsDiv = this.__component.querySelector('.dropdown .dropdown__opcoes')
    const selectedDiv = this.__component.querySelector('.selected')
    let options = ''
    let selecteds = ''

    for (const index in this.__state.options) {
      const currentOption = this.__state.options[index]
      const checkedClass = (currentOption.checked) ? 'checked' : ''

      const curentOptionText = (currentOption.text.split(' ').length > 2)
        ? `${currentOption.text.split(' ')[0]} ${currentOption.text.split(' ')[1]}`
        : currentOption.text

      options += `<option class="${checkedClass}" value="${currentOption.value}">${currentOption.text}</option>`

      if (checkedClass) selecteds += `<span>${curentOptionText}</span>`
    }

    optionsDiv.innerHTML = options
    selectedDiv.innerHTML = selecteds
  }

  activeClickOnOption () {
    document.addEventListener('click', (e) => {
      const matchElement = this.__component.querySelector('.dropdown .dropdown__opcoes')
      if (e.target.parentElement !== matchElement) return

      this.selectOption(e.target.value)
      this._resetSearchbar()
      this._loadOptions()

      HELPER_COMPONENTS_COMMUNICATOR().updateAgenda()
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_id, _key) => {
    const component = new SelectoMultiple(_id)

    component.render()

    component.activeClickOnSelecto()
    component.activeHiddenOnLostFocus()
    component.activeClickOnOption()
    component.activeKeyupOnSearchbar()

    SYMBOL().subscribe(_key, component)
  }

  return methods
}

module.exports = Module
