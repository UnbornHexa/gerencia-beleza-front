/* Requires */

const AGENDA_COMPONENT = require('../components/agenda')
const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_OPCOES = require('../modals/modal-opcoes')

/* Extended Class */

class AgendaExtended extends AGENDA_COMPONENT {
  activeMenuOnClick () {
    document.addEventListener('click', (e) => {
      if (!e.target.matches(`${this.__id} .agenda__dia .dia__body .horario`)) return

      const id = e.target.querySelector('.horario__id').innerText
      const telefone = e.target.querySelector('.horario__telefone').innerText
      const periodo = e.target.querySelector('.horario__periodo').innerText
      const funcionario = e.target.querySelector('.horario__funcionario').innerText
      const cliente = e.target.querySelector('.horario__cliente').innerText
      const estadia = e.target.querySelector('.horario__estadia').innerText

      const obj = { id, telefone, periodo, funcionario, cliente, estadia }

      MODAL_OPCOES().showModal()
      MODAL_OPCOES().importDataToModal(obj)
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = (_date) => {
    const component = new AgendaExtended('section.agenda')

    component.setDate(_date.year, _date.month)
    component.render()

    component.activeOnDrag()
    component.activeMenuOnClick()

    SYMBOL().subscribe('agenda_principal', component)
  }

  return methods
}

module.exports = Module
