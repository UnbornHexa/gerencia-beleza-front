/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_horario')

  const __clienteCollapsible = __modal.querySelector('.collapsible .cliente span')
  const __funcionarioCollapsible = __modal.querySelector('.collapsible .funcionario span')
  const __observacaoCollapsible = __modal.querySelector('.collapsible .observacao span')
  const __dataCollapsible = __modal.querySelector('.collapsible .data__registro span')
  const __horaInicialCollapsible = __modal.querySelector('.collapsible .hora__inicio span')
  const __horaFinalCollapsible = __modal.querySelector('.collapsible .hora__fim span')
  const __servicosCollapsible = __modal.querySelector('.collapsible .servicos span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __clienteCollapsible.innerHTML = '...'
    __funcionarioCollapsible.innerHTML = '...'
    __observacaoCollapsible.innerHTML = '...'
    __dataCollapsible.innerHTML = '...'
    __horaInicialCollapsible.innerHTML = '...'
    __horaFinalCollapsible.innerHTML = '...'
    __servicosCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    const cliente = (_data.id_cliente === null) ? 'Cliente deletado' : _data.id_cliente.nome
    const funcionario = (_data.id_usuario === null) ? 'Funcionário deletado' : _data.id_usuario.nome
    const servicos = _data.servicos.map(_servico => _servico._id.nome)

    __clienteCollapsible.innerText = cliente || ''
    __funcionarioCollapsible.innerText = funcionario || ''
    __observacaoCollapsible.innerText = _data.observacao || ''
    __dataCollapsible.innerText = CONVERT().dateBR(_data.data) || ''
    __horaInicialCollapsible.innerText = _data.hora_inicial || ''
    __horaFinalCollapsible.innerText = _data.hora_final || ''

    __servicosCollapsible.innerText = servicos.toString().replace(/,/g, '\n') || ''
  }

  return methods
}

module.exports = Module
