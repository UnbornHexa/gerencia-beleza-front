/* Requires */

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_opcoes_horario')

  const __idDiv = __modal.querySelector('.horario .horario__id')
  const __telefoneDiv = __modal.querySelector('.horario .horario__telefone')
  const __periodoDiv = __modal.querySelector('.horario .horario__periodo')
  const __funcionarioDiv = __modal.querySelector('.horario .horario__funcionario')
  const __clienteDiv = __modal.querySelector('.horario .horario__cliente')
  const __estadiaDiv = __modal.querySelector('.horario .horario__estadia')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __idDiv.innerHTML = '...'
    __telefoneDiv.innerHTML = '...'
    __periodoDiv.innerHTML = '...'
    __funcionarioDiv.innerHTML = '...'
    __clienteDiv.innerHTML = '...'
    __estadiaDiv.innerHTML = '...'

    __estadiaDiv.classList.remove('orange')
    __estadiaDiv.classList.remove('green')
    __estadiaDiv.classList.remove('red')
  }

  // Data

  methods.importDataToModal = (_data) => {
    let classe = 'orange'

    if (_data.estadia === 'Concluido') classe = 'green'
    if (_data.estadia === 'Faltou') classe = 'red'
    __estadiaDiv.classList.add(classe)

    __idDiv.innerText = _data.id
    __telefoneDiv.innerText = _data.telefone
    __periodoDiv.innerText = _data.periodo
    __funcionarioDiv.innerText = _data.funcionario
    __clienteDiv.innerText = _data.cliente
    __estadiaDiv.innerText = _data.estadia
  }

  return methods
}

module.exports = Module
