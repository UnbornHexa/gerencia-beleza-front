/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_enviar_comanda')

  const __idClienteHiddenDiv = __modal.querySelector('.modal__hidden .hidden__id__cliente')

  const __comandasSlide = SYMBOL().retrieve('slide_items_comandas_enviar_comanda')
  const __servicosStorage = SYMBOL().retrieve('storage_servicos_enviar_comanda')

  const __criarComandaButton = __modal.querySelector('.comandas .botao #criar__comanda')
  const __enviarButton = __modal.querySelector('.modal__button #enviar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  methods.resetForm = () => {
    __comandasSlide.reset()
    __servicosStorage.reset()

    __idClienteHiddenDiv.innerText = ''

    __criarComandaButton.setAttribute('disabled', true)
    __enviarButton.setAttribute('disabled', true)
  }

  // Buttons

  methods.activeButtonEnviar = () => {
    __enviarButton.removeAttribute('disabled')
    __criarComandaButton.setAttribute('disabled', true)
  }

  methods.activeButtonCriarComanda = () => {
    __criarComandaButton.removeAttribute('disabled')
    __enviarButton.setAttribute('disabled', true)
  }

  // Data

  methods.importHorarioToModal = (_horario) => {
    __servicosStorage.reset()

    __idClienteHiddenDiv.innerText = _horario.id_cliente._id

    const itemsServico = _horario.servicos.map(_servico => {
      return {
        id_servico: _servico._id,
        id_usuario: _horario.id_usuario,
        quantidade: 1,
        valor_unitario: _servico._id.valor,
        valor_desconto: 0,
        valor_subtotal: _servico._id.valor
      }
    })
    __servicosStorage.setValue(itemsServico)
  }

  methods.importComandasAbertasToModal = (_comandas, _funcionario) => {
    __comandasSlide.importItems(_comandas, _funcionario)

    if (!_comandas.length) methods.activeButtonCriarComanda()
  }

  methods.exportServicosFromHorario = () => {
    return __servicosStorage.getValue()
  }

  methods.generateNewComandaData = () => {
    const comanda = {}
    comanda.itens = {}

    comanda.data = methods._getCurrentDate()
    comanda.id_cliente = __idClienteHiddenDiv.innerText
    comanda.itens.servicos = __servicosStorage.getValue()
    comanda.itens.produtos = []

    return comanda
  }

  // Auxiliar Functions

  methods._getCurrentDate = () => {
    const newDate = new Date()

    const day = newDate.getDate().toString().padStart(2, '0')
    const month = (newDate.getMonth() + 1).toString().padStart(2, '0')
    const year = newDate.getFullYear().toString()

    return `${year}-${month}-${day}`
  }

  return methods
}

module.exports = Module
