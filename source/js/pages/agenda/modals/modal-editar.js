/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar_horario')

  const __idInput = __modal.querySelector('.informacoes__id input')
  const __observacaoInput = __modal.querySelector('.informacoes__observacao textarea')
  const __horaInicialInput = __modal.querySelector('.horario__inicial input')
  const __horaFinalInput = __modal.querySelector('.horario__final input')
  const __dataDatepicker = SYMBOL().retrieve('datapicker_editar')

  const __clientesSelecto = SYMBOL().retrieve('selecto_clientes_editar')
  const __funcionariosSelecto = SYMBOL().retrieve('selecto_funcionarios_editar')
  const __servicosSelecto = [
    SYMBOL().retrieve('selecto_servicos_editar_1'),
    SYMBOL().retrieve('selecto_servicos_editar_2'),
    SYMBOL().retrieve('selecto_servicos_editar_3'),
    SYMBOL().retrieve('selecto_servicos_editar_4'),
    SYMBOL().retrieve('selecto_servicos_editar_5'),
    SYMBOL().retrieve('selecto_servicos_editar_6'),
    SYMBOL().retrieve('selecto_servicos_editar_7'),
    SYMBOL().retrieve('selecto_servicos_editar_8'),
    SYMBOL().retrieve('selecto_servicos_editar_9'),
    SYMBOL().retrieve('selecto_servicos_editar_10')
  ]

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__dataDatepicker.value() && __horaInicialInput.value && __horaFinalInput.value && !__clientesSelecto.isEmpty() &&
    !__funcionariosSelecto.isEmpty() && !__servicosSelecto[0].isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.validateFields = () => {
    const horaInicialOk = FORM_VALIDATION().time(__horaInicialInput.value)
    const horaFinalOk = FORM_VALIDATION().time(__horaFinalInput.value)
    if (horaFinalOk && horaInicialOk) return true

    MESSAGEBOX().alert('Preencha um horário inicial e final válidos')
    return false
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __observacaoInput.value = ''
    __horaInicialInput.value = ''
    __horaFinalInput.value = ''
    __dataDatepicker.clear()

    __clientesSelecto.resetSelected()
    __funcionariosSelecto.resetSelected()

    for (const index in __servicosSelecto) {
      const selecto = __servicosSelecto[index]
      selecto.resetSelected()
      selecto.hidden()
    }
    __servicosSelecto[0].show()

    __editarButton.setAttribute('disabled', true)
  }

  // Data

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __dataDatepicker.selectDate(_data.data || '')
    __horaInicialInput.value = _data.hora_inicial || ''
    __horaFinalInput.value = _data.hora_final || ''
    __observacaoInput.value = _data.observacao || ''

    __clientesSelecto.selectOption(_data.id_cliente._id)
    __funcionariosSelecto.selectOption(_data.id_usuario._id)

    for (const index in _data.servicos) {
      const servico = _data.servicos[index]._id._id
      const selecto = __servicosSelecto[index]
      selecto.selectOption(servico)
      selecto.show()
    }
  }

  methods.exportDataFromModal = () => {
    const horario = {}

    const servicos = __servicosSelecto
      .map(_selecto => { return { _id: _selecto.value() } })
      .filter(_values => { if (_values._id !== undefined) return _values })

    // required
    horario.data = __dataDatepicker.value() || ''
    horario.hora_inicial = __horaInicialInput.value || ''
    horario.hora_final = __horaFinalInput.value || ''

    horario.id_cliente = __clientesSelecto.value() || ''
    horario.id_usuario = __funcionariosSelecto.value() || ''
    horario.servicos = servicos

    // optional
    if (__observacaoInput.value) horario.observacao = __observacaoInput.value || ''

    return horario
  }

  return methods
}

module.exports = Module
