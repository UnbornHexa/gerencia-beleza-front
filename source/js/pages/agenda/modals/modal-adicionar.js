/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const DATE = require('../../../global/helpers/date')
const FORM_VALIDATION = require('../../../global/helpers/form-validation')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const TOKEN = require('../../../global/helpers/token')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_horario')

  const __observacaoInput = __modal.querySelector('.informacoes__observacao textarea')
  const __horaInicialInput = __modal.querySelector('.horario__inicial input')
  const __horaFinalInput = __modal.querySelector('.horario__final input')
  const __dataDatepicker = SYMBOL().retrieve('datapicker_adicionar')

  const __clientesSelecto = SYMBOL().retrieve('selecto_clientes_adicionar')
  const __funcionariosSelecto = SYMBOL().retrieve('selecto_funcionarios_adicionar')
  const __servicosSelecto = [
    SYMBOL().retrieve('selecto_servicos_adicionar_1'),
    SYMBOL().retrieve('selecto_servicos_adicionar_2'),
    SYMBOL().retrieve('selecto_servicos_adicionar_3'),
    SYMBOL().retrieve('selecto_servicos_adicionar_4'),
    SYMBOL().retrieve('selecto_servicos_adicionar_5'),
    SYMBOL().retrieve('selecto_servicos_adicionar_6'),
    SYMBOL().retrieve('selecto_servicos_adicionar_7'),
    SYMBOL().retrieve('selecto_servicos_adicionar_8'),
    SYMBOL().retrieve('selecto_servicos_adicionar_9'),
    SYMBOL().retrieve('selecto_servicos_adicionar_10')
  ]

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__dataDatepicker.value() && __horaInicialInput.value && __horaFinalInput.value && !__clientesSelecto.isEmpty() &&
    !__funcionariosSelecto.isEmpty() && !__servicosSelecto[0].isEmpty()) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.validateFields = () => {
    const horaInicialOk = FORM_VALIDATION().time(__horaInicialInput.value)
    const horaFinalOk = FORM_VALIDATION().time(__horaFinalInput.value)
    if (horaFinalOk && horaInicialOk) return true

    MESSAGEBOX().alert('Preencha um horário inicial e final válidos')
    return false
  }

  methods.resetForm = () => {
    __observacaoInput.value = ''
    __horaInicialInput.value = ''
    __horaFinalInput.value = ''

    __dataDatepicker.clear()
    __dataDatepicker.selectDate(DATE().today())

    __clientesSelecto.resetSelected()
    __funcionariosSelecto.resetSelected()

    for (const index in __servicosSelecto) {
      const selecto = __servicosSelecto[index]
      selecto.resetSelected()
      selecto.hidden()
    }
    __servicosSelecto[0].show()

    __funcionariosSelecto.selectOption(TOKEN().getIdUsuarioFromToken())
  }

  // Data

  methods.exportDataFromModal = () => {
    const horario = {}

    const servicos = __servicosSelecto
      .map(_selecto => { return { _id: _selecto.value() } })
      .filter(_values => { if (_values._id !== undefined) return _values })

    // required
    horario.data = __dataDatepicker.value() || ''
    horario.hora_inicial = __horaInicialInput.value || ''
    horario.hora_final = __horaFinalInput.value || ''

    horario.id_cliente = __clientesSelecto.value() || ''
    horario.id_usuario = __funcionariosSelecto.value() || ''
    horario.servicos = servicos

    // optional
    if (__observacaoInput.value) horario.observacao = __observacaoInput.value || ''

    return horario
  }

  return methods
}

module.exports = Module
