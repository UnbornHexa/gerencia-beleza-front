/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_EMPRESA_PICTURE = require('./helpers/empresa-picture')
const SECTION_MAIN = require('./helpers/section-main')

const FORMULARIO_PAGAR = require('./sections/formulario-pagamento')

const LISTA_PAGAMENTOS = require('./extendeds/lista-pagamentos')
const SELECTO_ESTADO = require('./extendeds/selecto-estado')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  if (PERMISSION().checkAdmin() === false) {
    TOKEN().delToken()
    window.location.assign('/entrar')
  }

  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('empresa')
  ASIDE_PICTURE().load()
  HELPER_EMPRESA_PICTURE().load()

  LISTA_PAGAMENTOS().init()
  SELECTO_ESTADO().init('section#modal_editar .endereco__estado .selecto__common', 'selecto_estado_editar')
  FORMULARIO_PAGAR().buildFormPagamento()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  SECTION_MAIN().updateItems()
  SECTION_MAIN().updateImage()
  LISTA_PAGAMENTOS().updateItems()

  EVENTS().activeEvents()
}
start()
