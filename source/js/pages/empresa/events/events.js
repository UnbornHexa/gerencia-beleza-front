/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const SECTION_MAIN = require('./section-main')
const MODAL_ASSINAR = require('./modal-assinar')
const MODAL_EDITAR = require('./modal-editar')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    TOGGLE().activeClickOnToggle()

    SECTION_MAIN().activeClickOnAssinarButton()
    SECTION_MAIN().activeClickOnVisualizarButton()
    SECTION_MAIN().activeClickOnEditarButton()
    SECTION_MAIN().activeClickOnSuporteButton()
    SECTION_MAIN().activeClickOnAlterarLogoChange()

    MODAL_EDITAR().activeKeyupOnCep()
    MODAL_EDITAR().activeClickOnEditarButton()
    MODAL_EDITAR().activeChangeOnModal()

    MODAL_ASSINAR().activeClickOnAssinarButton()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
