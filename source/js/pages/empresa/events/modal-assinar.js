const Module = () => {
  const methods = {}

  // Internal Variables

  const __assinarButton = document.querySelector('section#modal_assinar .modal__button button')

  // Methods

  methods.activeClickOnAssinarButton = () => {
    __assinarButton.addEventListener('click', () => {
      const form = document.querySelector('section.pagamento form')
      form.submit()
    })
  }

  return methods
}

module.exports = Module
