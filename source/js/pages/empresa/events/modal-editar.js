/* Requires */

const EMPRESAS_REQUEST = require('../requests/empresas')
const CEP_REQUEST = require('../../../global/requests/cep')
const MODAL_EDITAR = require('../modals/modal-editar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_editar')

  const __cepInput = __modal.querySelector('.endereco__cep input')

  const __editarButton = __modal.querySelector('.modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_EDITAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await EMPRESAS_REQUEST().editEmpresa(modalData)
        MODAL_EDITAR().hideModal()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Cep

  methods.activeKeyupOnCep = () => {
    __cepInput.addEventListener('keyup', async (e) => {
      const cep = e.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      try {
        const address = await CEP_REQUEST().getAddress(cep)
        MODAL_EDITAR().fillCep(address)
      } catch (e) { console.log(e) }
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar input') &&
        !e.target.matches('section#modal_editar textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
