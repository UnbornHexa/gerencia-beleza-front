/* Requires */

const HELPER_IMAGE_VALIDATOR = require('../helpers/image-validator')
const SECTION_MAIN = require('../helpers/section-main')

const MODAL_ASSINAR = require('../modals/modal-assinar')
const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_SUPORTE = require('../modals/modal-suporte')

const EMPRESAS_REQUEST = require('../requests/empresas')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __empresaImagemInput = document.querySelector('section.empresa .empresa__content .info__image .foto__input')

  const __empresaImagem = document.querySelector('section.empresa .empresa__content .info__image')

  const __assinarButton = document.querySelector('section.assinatura .assinatura__nova #assinar')
  const __visualizarButton = document.querySelector('section.opcoes .button #visualizar_empresa')
  const __editarButton = document.querySelector('section.opcoes .button #editar_empresa')
  const __suporteButton = document.querySelector('section.opcoes .button #suporte_empresa')

  // Methods

  methods.activeClickOnAssinarButton = () => {
    __assinarButton.parentElement.addEventListener('click', () => {
      MODAL_ASSINAR().showModal()
    })
  }

  methods.activeClickOnSuporteButton = () => {
    __suporteButton.parentElement.addEventListener('click', () => {
      MODAL_SUPORTE().showModal()
    })
  }

  methods.activeClickOnVisualizarButton = () => {
    __visualizarButton.parentElement.addEventListener('click', async () => {
      MODAL_VISUALIZAR().showModal()

      try {
        const empresa = await EMPRESAS_REQUEST().getEmpresa()
        MODAL_VISUALIZAR().importDataToModal(empresa)
      } catch (e) { console.log(e) }
    })
  }

  methods.activeClickOnEditarButton = () => {
    __editarButton.parentElement.addEventListener('click', async () => {
      MODAL_EDITAR().showModal()

      try {
        const empresa = await EMPRESAS_REQUEST().getEmpresa()
        MODAL_EDITAR().importDataToModal(empresa)
      } catch (e) { console.log(e) }
    })
  }

  // Image

  methods.activeClickOnAlterarLogoChange = () => {
    __empresaImagem.addEventListener('click', () => {
      __empresaImagemInput.click()
    })

    __empresaImagemInput.addEventListener('change', async () => {
      const imagem = __empresaImagemInput.files[0]
      if (!HELPER_IMAGE_VALIDATOR().checkType(imagem)) return
      if (!HELPER_IMAGE_VALIDATOR().checkMaxSize(imagem)) return

      const imagemFormData = HELPER_IMAGE_VALIDATOR().convertFormData(imagem)

      try {
        document.querySelector('body').style.cursor = 'progress'
        await EMPRESAS_REQUEST().editEmpresaImage(imagemFormData)
        await SECTION_MAIN().updateImage()
      } catch (e) { console.log(e) }

      document.querySelector('body').style.cursor = 'default'
    })
  }

  return methods
}

module.exports = Module
