/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')
const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')

const PAGAMENTOS_REQUEST = require('../requests/pagamentos')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_pagamento) {
    const itemsDiv = this.__component.querySelector('.lista__itens')
    const classe = _pagamento.status.toLowerCase()

    const li =
      `
    <li>
      <span class="item__id" hidden>${_pagamento._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-star"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">Mensalidade ${CONVERT().dateBR(_pagamento.data_vencimento)}</span>
          <span class="item__valor">${FORMAT().money(_pagamento.valor)}</span>
        </div>
      </div>
      <div class="lista__status">
        <span class="item__situacao ${classe}"></span>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__pagamentos')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()

    SYMBOL().subscribe('lista_pagamentos', component)
  }

  methods.updateItems = async () => {
    const listaPagamentos = SYMBOL().retrieve('lista_pagamentos')

    try {
      const pagamentos = await PAGAMENTOS_REQUEST().getPagamentos()
      listaPagamentos.importItems(pagamentos)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
