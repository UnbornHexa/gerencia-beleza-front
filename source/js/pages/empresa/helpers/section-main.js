/* Requires */

const EMPRESAS_REQUEST = require('../requests/empresas')
const SECTION_MAIN = require('../sections/section-main')
const HELPER_ASSINATURA_FEITA = require('../helpers/assinatura-feita')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateItems = async () => {
    try {
      const assinatura = await EMPRESAS_REQUEST().getAssinatura()
      if (!assinatura) return

      HELPER_ASSINATURA_FEITA().checkAssinaturaFeita(assinatura)
      SECTION_MAIN().importDataToPage(assinatura)
    } catch (e) { console.log(e) }
  }

  methods.updateImage = async () => {
    try {
      const empresa = await EMPRESAS_REQUEST().getImagem()
      SECTION_MAIN().importImage(empresa.imagem)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
