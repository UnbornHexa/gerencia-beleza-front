/* Requires */

const NOTIFICACOES_REQUEST = require('../../../global/requests/notificacoes')

/* Module */

const Module = () => {
  const methods = {}

  methods.checkNews = async () => {
    try {
      const count = await NOTIFICACOES_REQUEST().countNotViewed()
      if (count > 0) activeIcon()
    } catch (e) { console.log(e) }
  }

  const activeIcon = () => {
    const notificacoesTop = document.querySelector('section.empresa .content__options a[name="notificacoes"]')
    const notificacoesNavbar = document.querySelector('navbar .navbar__notificacoes a[name="notificacoes"]')
    notificacoesTop.classList.add('active')
    notificacoesNavbar.classList.add('active')
  }

  return methods
}

module.exports = Module
