/* Requires */

const SECTION_MAIN = require('../sections/section-main')
const TOKEN = require('../../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  methods.checkAssinaturaFeita = (_assinatura) => {
    const isExpirado = TOKEN().getAssinaturaStatusFromToken()
    const nomeAssinatura = _assinatura.assinatura.nome

    if (isExpirado || nomeAssinatura === 'Free') return
    SECTION_MAIN().showAssinaturaFeita()
  }

  return methods
}

module.exports = Module
