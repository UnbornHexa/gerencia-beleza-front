const Module = () => {
  const methods = {}

  methods.load = () => {
    const picture = window.localStorage.getItem('imagem-empresa')
    if (typeof (picture) !== 'string' || !picture.includes('https://')) return
    showPicture(picture)
  }

  const showPicture = (_url) => {
    const fotoPerfil = document.querySelector('section.empresa .info__image img')
    fotoPerfil.src = _url
  }

  return methods
}

module.exports = Module
