/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  methods.checkMaxSize = (_imagem) => {
    if (_imagem.size <= 2000000) return true

    MESSAGEBOX().alert('A imagem deve ter no máximo 2mb.')
    return false
  }

  methods.checkType = (_imagem) => {
    const allowedTypes = [
      'image/png',
      'image/jpg',
      'image/jpeg',
      'image/svg',
      'image/webp',
      'image/webm'
    ]

    if (allowedTypes.includes(_imagem.type)) return true

    MESSAGEBOX().alert('A imagem deve estar em um dos seguintes formatos: png|jpg|jpeg|svg|webm|webp')
    return false
  }

  methods.convertFormData = (_imagem) => {
    const formData = new window.FormData()
    formData.append('avatar', _imagem)
    return formData
  }

  return methods
}

module.exports = Module
