/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __empresaImg = document.querySelector('section.empresa .content__info .info__image img')

  const __nomeEmpresaP = document.querySelector('section.empresa .content__info .info__title #nome_empresa')
  const __assinaturaTipoP = document.querySelector('section.empresa .content__info .info__title #status_assinatura')

  const __diasRestantesSpan = document.querySelector('section.assinatura #dias_restantes')

  const __assinaturaNovaDiv = document.querySelector('section.assinatura .assinatura__nova')
  const __assinaturaFeitaDiv = document.querySelector('section.assinatura .assinatura__feita')

  // Methods

  methods.importDataToPage = (_data) => {
    __nomeEmpresaP.innerHTML = _data.nome || ''
    __diasRestantesSpan.innerHTML = methods._subtrairData(_data.assinatura.data_vencimento)
    __assinaturaTipoP.innerHTML = _data.assinatura.nome || ''
  }

  methods.importImage = (_image) => {
    if (!_image) return

    __empresaImg.src = _image
    window.localStorage.setItem('imagem-empresa', _image)
  }

  methods.showAssinaturaFeita = () => {
    __assinaturaNovaDiv.style.display = 'none'
    __assinaturaFeitaDiv.style.display = 'flex'
  }

  // Aux Functions

  methods._subtrairData = (_data) => {
    const dateAssinatura = new Date(_data)
    const dateToday = new Date()

    const diff = Math.abs(dateAssinatura.getTime() - dateToday.getTime())
    const days = Math.ceil(diff / (1000 * 60 * 60 * 24))

    if (days < 0) return 0
    return days
  }

  return methods
}

module.exports = Module
