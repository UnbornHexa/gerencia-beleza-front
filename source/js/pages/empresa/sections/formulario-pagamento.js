/* Requires */

const TOKEN = require('../../../global/helpers/token')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __pagamentoForm = document.querySelector('section.pagamento')

  // Methods

  methods.buildFormPagamento = () => {
    const formulario =
    `
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
      <!--Tipo do botão-->
      <input type="hidden" name="cmd" value="_s-xclick" />
      <input type="hidden" name="hosted_button_id" value="UCT5ZRCG29CWG">
      <!--Vendedor e URL de retorno, cancelamento e notificação-->
      <input type="hidden" name="business" value="app@gerenciabeleza.com" />
      <input type="hidden" name="return" value="https://app.gerenciabeleza.com.br/app/empresa" />
      <input type="hidden" name="cancel" value="https://app.gerenciabeleza.com.br/app/empresa" />
      <input type="hidden" name="notify_url" value="https://app.gerenciabeleza.com.br/app/empresa" />
      <!--Internacionalização e localização da página de pagamento-->
      <input type="hidden" name="charset" value="utf-8" />
      <input type="hidden" name="lc" value="BR" />
      <input type="hidden" name="country_code" value="BR" />
      <input type="hidden" name="currency_code" value="BRL" />
      <!-- ID_Empresa -->
      <input type="hidden" name="custom" value="${TOKEN().getIdEmpresaFromToken()}" />
    </form>
    `

    __pagamentoForm.innerHTML = formulario
  }

  return methods
}

module.exports = Module
