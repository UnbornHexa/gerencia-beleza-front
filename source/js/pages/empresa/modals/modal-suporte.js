/* Requires */

const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_suporte')

  // Methods

  methods.hideModal = () => {
    __modal.style.display = 'none'
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    MAIN().disableMain()
  }

  return methods
}

module.exports = Module
