/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __gorjetasSection = document.querySelector('section.financeiro__info')

  const __gorjetasTotalSpan = __gorjetasSection.querySelector('.info__complemento .complemento__gorjetas span#gorjeta__mes')
  const __gorjetasGallery = __gorjetasSection.querySelector('.info__content .content__funcionarios')

  let __total = 0

  // Methods

  methods.load = (_gorjetas) => {
    methods._resetGallery()

    const gorjetas = methods._calcGorjetas(_gorjetas)
    __gorjetasTotalSpan.innerText = FORMAT().money(gorjetas.total)

    return gorjetas.total
  }

  methods._calcGorjetas = (_gorjetas) => {
    for (const index in _gorjetas) {
      const nomeFuncionario = _gorjetas[index]._id
      const valor = _gorjetas[index].valor

      methods._addItemInGallery(nomeFuncionario, valor)
      __total += valor
    }

    return { total: __total }
  }

  // Gallery

  methods._addItemInGallery = (_funcionario, _valor) => {
    const funcionario = (_funcionario.split(' ').length > 2)
      ? `${_funcionario.split(' ')[0]} ${_funcionario.split(' ')[1]}`
      : _funcionario

    const item =
    `
    <div class="info_funcionario fade">
      <div class="imagem">
        <img src="https://app.gerenciabeleza.com.br/img/plataforma/all/placeholder-avatar.svg">
      </div>
      <div class="nome">
        <span id="nome_funcionario">${funcionario}</span>
      </div>
      <div class="gorjeta">
        <span id="gorjeta_funcionario">${FORMAT().money(_valor)}</span>
        <p>em gorjetas</p>
      </div>
    </div>
    `

    __gorjetasGallery.innerHTML += item
  }

  methods._resetGallery = () => {
    __gorjetasGallery.innerHTML = ''
  }

  return methods
}

module.exports = Module
