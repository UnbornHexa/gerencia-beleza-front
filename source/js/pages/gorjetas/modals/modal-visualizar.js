/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_gorjeta')

  const __funcionarioCollapsible = __modal.querySelector('.collapsible .funcionario span')
  const __dataPagamentoCollapsible = __modal.querySelector('.collapsible .data__pagamento span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')
  const __formaPagamentoCollapsible = __modal.querySelector('.collapsible .forma__pagamento span')
  const __valorCollapsible = __modal.querySelector('.collapsible .valor span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __funcionarioCollapsible.innerHTML = '...'
    __dataPagamentoCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'
    __formaPagamentoCollapsible.innerHTML = '...'
    __valorCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    const formaPagamento = (_data.forma_pagamento === 'dinheiro') ? 'Dinheiro' : ''

    __funcionarioCollapsible.innerText = _data.nome.replace('Gorjeta para ', '') || ''
    __dataPagamentoCollapsible.innerText = CONVERT().dateBR(_data.data_vencimento) || ''
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || ''
    __formaPagamentoCollapsible.innerText = formaPagamento || ''
    __valorCollapsible.innerText = FORMAT().money(_data.valor) || ''
  }

  return methods
}

module.exports = Module
