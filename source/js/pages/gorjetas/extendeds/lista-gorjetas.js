/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')

const FORMAT = require('../../../global/helpers/format')
const CONVERT = require('../../../global/helpers/convert')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const GORJETAS_REQUEST = require('../requests/gorjetas')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_gorjeta) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
    `
    <li>
      <span class="item__id" hidden>${_gorjeta._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-coins"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_gorjeta.id_usuario.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
            </div>

          </span>
          <span class="item__valor">${FORMAT().money(_gorjeta.valor)} de gorjeta</span>
        </div>
      </div>
      <div class="lista__data">
        <span class="item__data__vencimento">pago em ${CONVERT().dateBR(_gorjeta.data_vencimento)}</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idGorjeta = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const gorjeta = await GORJETAS_REQUEST().getGorjetaById(idGorjeta)
        MODAL_VISUALIZAR().importDataToModal(gorjeta)
      } catch (e) { console.log(e) }
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__gorjetas')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()

    SYMBOL().subscribe('lista_gorjetas', component)
  }

  return methods
}

module.exports = Module
