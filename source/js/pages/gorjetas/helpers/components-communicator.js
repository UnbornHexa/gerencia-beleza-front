/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SECTION_GORJETAS = require('../sections/section-gorjetas')

const GORJETAS_REQUEST = require('../requests/gorjetas')
const RESUMO_REQUEST = require('../requests/resumo')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateValues = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const date = dateSelector.value()
    const yearAndMonth = `${date.year}-${date.month.toString().padStart(2, '0')}`

    await methods._updateGorjetas(yearAndMonth)
    await methods._updateResumo(yearAndMonth)
  }

  methods._updateGorjetas = async (_yearAndMonth) => {
    const listaGorjetas = SYMBOL().retrieve('lista_gorjetas')

    try {
      const gorjetas = await GORJETAS_REQUEST().getGorjetas(_yearAndMonth)
      listaGorjetas.importItems(gorjetas)
    } catch (e) { console.log(e) }
  }

  methods._updateResumo = async (_yearAndMonth) => {
    try {
      const resumo = await RESUMO_REQUEST().getResumoFinanceiro(_yearAndMonth)
      SECTION_GORJETAS().load(resumo.gorjetas)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
