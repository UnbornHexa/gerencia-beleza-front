const Module = () => {
  const methods = {}

  methods.idEmpresa = () => {
    return window.location.pathname.split('/')[2]
  }

  methods.idUsuario = () => {
    return window.location.pathname.split('/')[3]
  }

  methods.senhaToken = () => {
    return window.location.pathname.split('/')[4]
  }

  return methods
}

module.exports = Module
