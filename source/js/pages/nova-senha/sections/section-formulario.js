/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __senhaInput = document.querySelector('section.alterar .alterar__senha input')
  const __confirmarSenhaInput = document.querySelector('section.alterar .alterar__confirmar__senha input')

  // Form

  methods.checkEqualPasswords = () => {
    if (__senhaInput.value === __confirmarSenhaInput.value) return true

    MESSAGEBOX().alert('As senhas devem ser iguais.')
    return false
  }

  methods.checkPasswordLength = () => {
    if (__senhaInput.value.length >= 6) return true

    MESSAGEBOX().alert('A senha deve ter no mínimo 6 dígitos.')
    return false
  }

  methods.resetForm = () => {
    __senhaInput.value = ''
    __confirmarSenhaInput.value = ''
  }

  // Data

  methods.exportDataFromModal = () => {
    const senha = __senhaInput.value
    return { senha }
  }

  return methods
}

module.exports = Module
