/* Requires */

const HOSTS = require('../../../global/data/hosts')
const HTTP = require('../../../global/helpers/http')
const MESSAGEBOX = require('../../../global/modules/messagebox')

const URL_HELPER = require('../helpers/parametros-url')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __url = `${HOSTS().url.api_plataforma}/autenticacao`

  // Methods

  methods.novaSenha = async (_data) => {
    const idEmpresa = URL_HELPER().idEmpresa()
    const idUsuario = URL_HELPER().idUsuario()
    const senhaToken = URL_HELPER().senhaToken()

    const url = `${__url}/nova-senha/${idEmpresa}/${idUsuario}/${senhaToken}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, null, _response => {
        if (_response.status === 201) {
          MESSAGEBOX().alert(_response.body, 'success')
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
