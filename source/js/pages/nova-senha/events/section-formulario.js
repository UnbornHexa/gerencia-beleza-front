/* Requires */

const SECTION_FORMULARIO = require('../sections/section-formulario')
const AUTENTICACAO_REQUEST = require('../requests/autenticacao')
const REDIRECIONAMENTO = require('../helpers/redirecionamento')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __alterarButton = document.querySelector('section.alterar #alterar')

  // Methods

  methods.activeClickOnAlterarButton = () => {
    __alterarButton.addEventListener('click', async () => {
      const modalData = SECTION_FORMULARIO().exportDataFromModal()

      const senhasIguais = SECTION_FORMULARIO().checkEqualPasswords()
      if (senhasIguais === false) return

      const senhaOk = SECTION_FORMULARIO().checkPasswordLength()
      if (senhaOk === false) return

      try {
        __alterarButton.setAttribute('disabled', true)
        await AUTENTICACAO_REQUEST().novaSenha(modalData)
        SECTION_FORMULARIO().resetForm()
        setTimeout(() => { REDIRECIONAMENTO().entrar() }, 5000)
      } catch (e) { console.log(e) }

      __alterarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
