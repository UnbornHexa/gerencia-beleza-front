/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')

const SECTION_FORMULARIO = require('./section-formulario')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()

    SECTION_FORMULARIO().activeClickOnAlterarButton()
  }

  return methods
}

module.exports = Module
