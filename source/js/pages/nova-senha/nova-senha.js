/* Requires */

require('regenerator-runtime/runtime')

const EVENTS = require('./events/events')

/* Start */

const start = async () => {
  EVENTS().activeEvents()
}
start()
