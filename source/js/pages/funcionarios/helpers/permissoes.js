/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const PERMISSOES_REQUEST = require('../requests/permissoes')

/* Module */

const Module = () => {
  const methods = {}

  methods.exportPermissoesToSelectos = async () => {
    const selectoPermissoesAdicionar = SYMBOL().retrieve('selecto_permissoes_adicionar')
    const selectoPermissoesEditar = SYMBOL().retrieve('selecto_permissoes_editar')

    try {
      const permissoes = await PERMISSOES_REQUEST().getNomesPermissoes()

      const options = permissoes.map(_permissao => {
        return { value: _permissao._id, text: _permissao.nome }
      })

      if (options.length <= 1) {
        const html = `
        <p>Não há permissões cadastradas ainda.</p>
        <a href="./permissoes">
          <button type="button">Adicionar Permissão</button>
        </a>
        `
        selectoPermissoesAdicionar._alertEmpty(html)
        selectoPermissoesEditar._alertEmpty(html)
      }

      selectoPermissoesAdicionar.importOptions(options)
      selectoPermissoesEditar.importOptions(options)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
