const Module = () => {
  const methods = {}

  methods.generate = () => {
    const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    const letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    let hash = ''

    for (let index = 0; index < 4; index++) {
      const rand = letters[Math.floor(Math.random() * letters.length)]
      hash += rand
    }

    for (let index = 0; index < 4; index++) {
      const rand = numbers[Math.floor(Math.random() * numbers.length)]
      hash += rand
    }

    return hash
  }

  return methods
}

module.exports = Module
