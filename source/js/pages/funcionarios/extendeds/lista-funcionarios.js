/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')
const FORMAT = require('../../../global/helpers/format')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_PAGAR = require('../modals/modal-pagar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const USUARIOS_REQUEST = require('../requests/usuarios')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_funcionario) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li>
      <span class="item__id" hidden>${_funcionario._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-user-tie"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_funcionario.nome}

            <div class="lista__opcoes">
              <!-- Pagar -->
              <div class="opcoes__pagar">
                <i class="icon-sack-dollar"></i>
                <span>Pagar</span>
              </div>
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__permissao">${_funcionario.id_permissao.nome}</span>
        </div>
      </div>
      <div class="lista__pendente">
        <span class="item__comissao">${FORMAT().money(_funcionario.comissao_pendente)} <p>comissão pendente</p></span>
        <span class="item__gorjeta">${FORMAT().money(_funcionario.gorjeta_pendente)} <p>gorjeta pendente</p></span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idFuncionario = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const funcionario = await USUARIOS_REQUEST().getUsuarioById(idFuncionario)
        MODAL_VISUALIZAR().importDataToModal(funcionario)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuPagar () {
    const match = `${this.__id} .lista__opcoes .opcoes__pagar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_PAGAR().showModal()
      const idFuncionario = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const funcionario = await USUARIOS_REQUEST().getUsuarioById(idFuncionario)
        MODAL_PAGAR().importDataToModal(funcionario)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idFuncionario = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const funcionario = await USUARIOS_REQUEST().getUsuarioById(idFuncionario)
        MODAL_EDITAR().importDataToModal(funcionario)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idFuncionario = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeFuncionario = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_funcionario .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_funcionario .modal__header p')

      inputIdModal.value = idFuncionario
      headerNomeModal.innerText = nomeFuncionario
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__funcionarios')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuPagar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_funcionarios', component)
  }

  methods.updateItems = async () => {
    const listaFuncionarios = SYMBOL().retrieve('lista_funcionarios')

    try {
      const funcionarios = await USUARIOS_REQUEST().getUsuarios()
      listaFuncionarios.importItems(funcionarios)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
