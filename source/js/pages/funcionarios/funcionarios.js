/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_PERMISSOES = require('./helpers/permissoes')

const LISTA_FUNCIONARIOS = require('./extendeds/lista-funcionarios')
const SELECTO_ESTADO = require('./extendeds/selecto-estado')
const SELECTO = require('./extendeds/selecto')
const DATEPICKER = require('./extendeds/datepicker')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('funcionarios')
  ASIDE_PICTURE().load()

  LISTA_FUNCIONARIOS().init()
  SELECTO_ESTADO().init('section#modal_adicionar_funcionario .endereco__estado .selecto__common', 'selecto_estado_adicionar')
  SELECTO_ESTADO().init('section#modal_editar_funcionario .endereco__estado .selecto__common', 'selecto_estado_editar')
  SELECTO().init('section#modal_adicionar_funcionario .informacoes__permissao .selecto__common', 'selecto_permissoes_adicionar')
  SELECTO().init('section#modal_editar_funcionario .informacoes__permissao .selecto__common', 'selecto_permissoes_editar')
  DATEPICKER().init('section#modal_pagar_funcionario .datepicker', 'datapicker_pagar')

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_FUNCIONARIOS().updateItems()
  HELPER_PERMISSOES().exportPermissoesToSelectos()

  EVENTS().activeEvents()
}
start()
