/* Requires */

const MODAL_ADICIONAR = require('../modals/modal-adicionar')
const HELPER_PASSWORD = require('../helpers/password')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __senhaInput = document.querySelector('section#modal_adicionar_funcionario .autenticacao__senha input')

  const __adicionarButton = document.querySelector('.content__adicionar #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', () => {
      MODAL_ADICIONAR().showModal()
      __senhaInput.value = HELPER_PASSWORD().generate()
    })
  }

  return methods
}

module.exports = Module
