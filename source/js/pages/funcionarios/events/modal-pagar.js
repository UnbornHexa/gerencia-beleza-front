/* Requires */

const FORMAT = require('../../../global/helpers/format')
const USUARIOS_REQUEST = require('../requests/usuarios')
const MODAL_PAGAR = require('../modals/modal-pagar')
const LISTA_FUNCIONARIOS = require('../extendeds/lista-funcionarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_pagar_funcionario .id input')
  const __valorComissaoInput = document.querySelector('section#modal_pagar_funcionario .pagar__comissao input')
  const __valorGorjetaInput = document.querySelector('section#modal_pagar_funcionario .pagar__gorjeta input')

  const __comissaoPendenteLabel = document.querySelector('section#modal_pagar_funcionario .comissao__pendente span')
  const __gorjetaPendenteLabel = document.querySelector('section#modal_pagar_funcionario .gorjeta__pendente span')

  const __pagarButton = document.querySelector('section#modal_pagar_funcionario .modal__button #pagar')

  // Methods

  methods.activeKeypressOnComissao = () => {
    __valorComissaoInput.addEventListener('keyup', () => {
      try {
        const valorPago = FORMAT().unformatMoney(__valorComissaoInput.value)
        const valorPendente = __valorComissaoInput.dataset.valorPendente
        const valorApresentado = (valorPendente - valorPago)

        __comissaoPendenteLabel.innerHTML = FORMAT().money(valorApresentado) || 'R$ 0,00'
      } catch (e) { }
    })
  }

  methods.activeKeypressOnGorjeta = () => {
    __valorGorjetaInput.addEventListener('keyup', () => {
      try {
        const valorPago = FORMAT().unformatMoney(__valorGorjetaInput.value)
        const valorPendente = __valorGorjetaInput.dataset.valorPendente
        const valorApresentado = (valorPendente - valorPago)

        __gorjetaPendenteLabel.innerHTML = FORMAT().money(valorApresentado) || 'R$ 0,00'
      } catch (e) { }
    })
  }

  methods.activeClickOnPagarButton = () => {
    __pagarButton.addEventListener('click', async () => {
      const modalData = MODAL_PAGAR().exportDataFromModal()
      const idFuncionario = __idInput.value

      const modalOk = MODAL_PAGAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __pagarButton.setAttribute('disabled', true)
        await USUARIOS_REQUEST().payUsuario(idFuncionario, modalData)
        MODAL_PAGAR().hideModal()
        await LISTA_FUNCIONARIOS().updateItems()
      } catch (e) { console.log(e) }

      __pagarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
