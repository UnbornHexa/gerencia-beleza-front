/* Requires */

const USUARIOS_REQUEST = require('../requests/usuarios')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_FUNCIONARIOS = require('../extendeds/lista-funcionarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_funcionario .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_funcionario .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idFuncionario = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await USUARIOS_REQUEST().delUsuario(idFuncionario)
        MODAL_DELETAR().hideModal()
        await LISTA_FUNCIONARIOS().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
