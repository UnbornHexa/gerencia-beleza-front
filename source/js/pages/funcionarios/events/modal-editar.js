/* Requires */

const USUARIOS_REQUEST = require('../requests/usuarios')
const CEP_REQUEST = require('../../../global/requests/cep')
const MODAL_EDITAR = require('../modals/modal-editar')
const LISTA_FUNCIONARIOS = require('../extendeds/lista-funcionarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_funcionario .informacoes__id input')
  const __cepInput = document.querySelector('section#modal_editar_funcionario .endereco__cep input')

  const __editarButton = document.querySelector('section#modal_editar_funcionario .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idFuncionario = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_EDITAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await USUARIOS_REQUEST().editUsuario(idFuncionario, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_FUNCIONARIOS().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Cep

  methods.activeKeyupOnCep = () => {
    __cepInput.addEventListener('keyup', async (e) => {
      const cep = e.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      try {
        const address = await CEP_REQUEST().getAddress(cep)
        MODAL_EDITAR().fillCep(address)
      } catch (e) { console.log(e) }
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_funcionario input') &&
        !e.target.matches('section#modal_editar_funcionario textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_funcionario .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
