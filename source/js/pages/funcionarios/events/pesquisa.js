/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaFuncionarios = SYMBOL().retrieve('lista_funcionarios')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaFuncionarios.getState().filteredItems = __listaFuncionarios.getState().items
          __listaFuncionarios._loadItemsFromPage(1)
          return
        }

        __listaFuncionarios.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaFuncionarios.getState().items) {
          const item = __listaFuncionarios.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaFuncionarios.getState().filteredItems.push(item)
          }
        }

        __listaFuncionarios._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
