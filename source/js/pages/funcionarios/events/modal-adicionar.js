/* Requires */

const USUARIOS_REQUEST = require('../requests/usuarios')
const CEP_REQUEST = require('../../../global/requests/cep')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')
const LISTA_FUNCIONARIOS = require('../extendeds/lista-funcionarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __cepInput = document.querySelector('section#modal_adicionar_funcionario .endereco__cep input')

  const __adicionarButton = document.querySelector('section#modal_adicionar_funcionario .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      const modalValidatedOk = MODAL_ADICIONAR().validateFields()
      if (modalValidatedOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await USUARIOS_REQUEST().addUsuario(modalData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_FUNCIONARIOS().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  // Cep

  methods.activeKeyupOnCep = () => {
    __cepInput.addEventListener('keyup', async (e) => {
      const cep = e.target.value.replace(/[-.]/g, '')
      if (cep.length !== 8) return

      try {
        const address = await CEP_REQUEST().getAddress(cep)
        MODAL_ADICIONAR().fillCep(address)
      } catch (e) { console.log(e) }
    })
  }

  return methods
}

module.exports = Module
