/* Requires */

const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __url = `${HOSTS().url.api_plataforma}/usuarios`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getUsuarios = async () => {
    const url = `${__url}/${__idEmpresa}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getUsuarioById = async (_idUsuario) => {
    const url = `${__url}/${__idEmpresa}/${_idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.addUsuario = async (_data) => {
    const url = `${__url}/${__idEmpresa}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, __headersWithToken, _response => {
        if (_response.status === 201) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editUsuario = async (_idUsuario, _data) => {
    const url = `${__url}/${__idEmpresa}/${_idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PUT', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.delUsuario = async (_idUsuario) => {
    const url = `${__url}/${__idEmpresa}/${_idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'DELETE', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.payUsuario = async (_idUsuario, _data) => {
    const url = `${__url}/pay/${__idEmpresa}/${_idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PATCH', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
