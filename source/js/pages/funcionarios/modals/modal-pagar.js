/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const FORMAT = require('../../../global/helpers/format')
const DATE = require('../../../global/helpers/date')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_pagar_funcionario')

  const __idInput = __modal.querySelector('.id input')
  const __funcionarioInput = __modal.querySelector('.funcionario input')
  const __dataDatepicker = SYMBOL().retrieve('datapicker_pagar')
  const __valorComissaoInput = __modal.querySelector('.pagar__comissao input')
  const __valorGorjetaInput = __modal.querySelector('.pagar__gorjeta input')

  const __comissaoPendenteLabel = __modal.querySelector('.comissao__pendente span')
  const __gorjetaPendenteInput = __modal.querySelector('.gorjeta__pendente span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__dataDatepicker.value() && __valorComissaoInput.value && __valorGorjetaInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __idInput.value = ''
    __funcionarioInput.value = ''
    __dataDatepicker.clear()
    __valorComissaoInput.value = ''
    __valorGorjetaInput.value = ''

    __comissaoPendenteLabel.innerText = 'R$ 0,00'
    __gorjetaPendenteInput.innerText = 'R$ 0,00'
  }

  // Data

  methods.importDataToModal = (_data) => {
    __idInput.value = _data._id || ''
    __funcionarioInput.value = _data.nome || ''
    __dataDatepicker.selectDate(DATE().today())

    __comissaoPendenteLabel.innerHTML = FORMAT().money(_data.comissao_pendente) || 'R$ 0,00'
    __gorjetaPendenteInput.innerHTML = FORMAT().money(_data.gorjeta_pendente) || 'R$ 0,00'

    __valorComissaoInput.dataset.valorPendente = _data.comissao_pendente || 0
    __valorGorjetaInput.dataset.valorPendente = _data.gorjeta_pendente || 0
  }

  methods.exportDataFromModal = () => {
    const pagamento = {}

    // required
    pagamento.valor_comissao = FORMAT().unformatMoney(__valorComissaoInput.value) || 0
    pagamento.valor_gorjeta = FORMAT().unformatMoney(__valorGorjetaInput.value) || 0
    pagamento.data_pagamento = __dataDatepicker.value() || ''

    return pagamento
  }

  return methods
}

module.exports = Module
