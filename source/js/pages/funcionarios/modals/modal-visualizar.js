/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_visualizar_funcionario')

  const __nomeCollapsible = __modal.querySelector('.collapsible .nome span')
  const __permissaoCollapsible = __modal.querySelector('.collapsible .permissao span')
  const __dataNascimentoCollapsible = __modal.querySelector('.collapsible .data__nascimento span')
  const __dataRegistroCollapsible = __modal.querySelector('.collapsible .data__registro span')
  const __ultimoAcessoCollapsible = __modal.querySelector('.collapsible .data__acesso span')

  const __telefone1Collapsible = __modal.querySelector('.collapsible .telefone span')
  const __telefone2Collapsible = __modal.querySelector('.collapsible .telefone2 span')
  const __emailCollapsible = __modal.querySelector('.collapsible .email span')

  const __cepCollapsible = __modal.querySelector('.collapsible .cep span')
  const __estadoCollapsible = __modal.querySelector('.collapsible .estado span')
  const __cidadeCollapsible = __modal.querySelector('.collapsible .cidade span')
  const __bairroCollapsible = __modal.querySelector('.collapsible .bairro span')
  const __ruaCollapsible = __modal.querySelector('.collapsible .rua span')
  const __numeroCollapsible = __modal.querySelector('.collapsible .numero span')

  const __comissaoCollapsible = __modal.querySelector('.collapsible .comissao span')
  const __gorjetaCollapsible = __modal.querySelector('.collapsible .gorjeta span')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __nomeCollapsible.innerHTML = '...'
    __permissaoCollapsible.innerHTML = '...'
    __dataNascimentoCollapsible.innerHTML = '...'
    __dataRegistroCollapsible.innerHTML = '...'
    __ultimoAcessoCollapsible.innerHTML = '...'

    __telefone1Collapsible.innerHTML = '...'
    __telefone2Collapsible.innerHTML = '...'
    __emailCollapsible.innerHTML = '...'

    __cepCollapsible.innerHTML = '...'
    __estadoCollapsible.innerHTML = '...'
    __cidadeCollapsible.innerHTML = '...'
    __bairroCollapsible.innerHTML = '...'
    __ruaCollapsible.innerHTML = '...'
    __numeroCollapsible.innerHTML = '...'

    __comissaoCollapsible.innerHTML = '...'
    __gorjetaCollapsible.innerHTML = '...'

    methods._shrinkCollapsibles()
  }

  methods._shrinkCollapsibles = () => {
    const collapsiblesDiv = __modal.querySelectorAll('.collapsible')
    const maxLength = collapsiblesDiv.length - 1

    for (let index = 0; index <= maxLength; index++) {
      const component = collapsiblesDiv[index]
      component.classList.remove('active')
      if (index === 0) component.classList.add('active')
    }
  }

  // Data

  methods.importDataToModal = (_data) => {
    __nomeCollapsible.innerText = _data.nome || '...'
    __permissaoCollapsible.innerText = _data.id_permissao.nome || '...'
    __dataNascimentoCollapsible.innerText = CONVERT().dateBR(_data.data_nascimento) || '...'
    __dataRegistroCollapsible.innerText = CONVERT().dateBR(_data.data_registro) || '...'
    __ultimoAcessoCollapsible.innerText = CONVERT().dateBR(_data.ultimo_acesso) || '...'
    __emailCollapsible.innerText = _data.email || '...'

    if (Object.prototype.hasOwnProperty.call(_data, 'contato')) {
      __telefone1Collapsible.innerText = _data.contato.telefone1 || '...'
      __telefone2Collapsible.innerText = _data.contato.telefone2 || '...'
    }

    if (Object.prototype.hasOwnProperty.call(_data, 'endereco')) {
      __cepCollapsible.innerText = _data.endereco.cep || '...'
      __estadoCollapsible.innerText = _data.endereco.estado || '...'
      __cidadeCollapsible.innerText = _data.endereco.cidade || '...'
      __bairroCollapsible.innerText = _data.endereco.bairro || '...'
      __ruaCollapsible.innerText = _data.endereco.rua || '...'
      __numeroCollapsible.innerText = _data.endereco.numero || '...'
    }

    __comissaoCollapsible.innerText = FORMAT().money(_data.comissao_pendente) || 'R$ 0,00'
    __gorjetaCollapsible.innerText = FORMAT().money(_data.gorjeta_pendente) || 'R$ 0,00'
  }

  return methods
}

module.exports = Module
