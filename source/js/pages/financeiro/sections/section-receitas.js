/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __receitasSection = document.querySelector('section.financeiro__grafico .grafico__complemento')

  const __receitasTotalSpan = __receitasSection.querySelector('.complemento__receitas .valor span#receita__mes')

  // Methods

  methods.load = (_receitas) => {
    const total = methods._calcReceitas(_receitas)
    __receitasTotalSpan.innerText = FORMAT().money(total)

    return total
  }

  methods._calcReceitas = (_receitas) => {
    return _receitas.reduce((_total, _current) => {
      return _total + _current.valor
    }, 0)
  }

  return methods
}

module.exports = Module
