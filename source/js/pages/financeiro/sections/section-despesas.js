/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __despesasSection = document.querySelector('section.financeiro__grafico .grafico__complemento')

  const __despesasTotalSpan = __despesasSection.querySelector('.complemento__despesas .valor span#despesa__mes')

  // Methods

  methods.load = (_despesas) => {
    const total = methods._calcDespesas(_despesas)
    __despesasTotalSpan.innerText = FORMAT().money(total)

    return total
  }

  methods._calcDespesas = (_despesas) => {
    return _despesas.reduce((_total, _current) => {
      return _total + _current.valor
    }, 0)
  }

  return methods
}

module.exports = Module
