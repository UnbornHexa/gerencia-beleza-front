/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __gorjetasSection = document.querySelector('section.financeiro__pagos')

  const __gorjetasTotalSpan = __gorjetasSection.querySelector('.pagos__gorjetas .valor span#gorjeta__mes')

  // Methods

  methods.load = (_gorjetas) => {
    const total = methods._calcGorjetas(_gorjetas)
    __gorjetasTotalSpan.innerText = FORMAT().money(total)

    return total
  }

  methods._calcGorjetas = (_gorjetas) => {
    return _gorjetas.reduce((_total, _current) => {
      return _total + _current.valor
    }, 0)
  }

  return methods
}

module.exports = Module
