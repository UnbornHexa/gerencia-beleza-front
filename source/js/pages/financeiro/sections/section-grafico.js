/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __graficoSection = document.querySelector('section.financeiro__grafico .grafico__content .grafico')
  const __receitasSVG = __graficoSection.querySelector('svg circle#donut_receita')
  const __despesasSVG = __graficoSection.querySelector('svg circle#donut_despesa')
  const __comissoesSVG = __graficoSection.querySelector('svg circle#donut_comissao')
  const __lucroText = __graficoSection.querySelector('svg text#lucro_financeiro')

  const __porcentagemSection = document.querySelector('section.financeiro__grafico .grafico__content .info')
  const __receitasSpan = __porcentagemSection.querySelector('.info__receita span#receita__porcentagem')
  const __despesasSpan = __porcentagemSection.querySelector('.info__despesa span#despesa__porcentagem')
  const __comissoesSpan = __porcentagemSection.querySelector('.info__comissao span#comissao__porcentagem')

  // Methods

  methods.build = (_receitas, _despesas, _comissoes) => {
    const receitas = Number(_receitas) || 0
    const despesas = Number(_despesas) || 0
    const comissoes = Number(_comissoes) || 0
    const lucro = receitas - (despesas + comissoes)

    const receitasPercentage = methods._calcPercentage(receitas, receitas, despesas, comissoes)
    const despesasPercentage = methods._calcPercentage(despesas, receitas, despesas, comissoes)
    const comissoesPercentage = methods._calcPercentage(comissoes, receitas, despesas, comissoes)

    const somaDespesasComissoes = comissoesPercentage + despesasPercentage
    const somaTotal = somaDespesasComissoes + receitasPercentage

    __receitasSpan.innerText = `${receitasPercentage}%`
    __despesasSpan.innerText = `${despesasPercentage}%`
    __comissoesSpan.innerText = `${comissoesPercentage}%`

    __receitasSVG.setAttribute('stroke-dasharray', `${somaTotal} 100`)
    __despesasSVG.setAttribute('stroke-dasharray', `${somaDespesasComissoes} 100`)
    __comissoesSVG.setAttribute('stroke-dasharray', `${comissoesPercentage} 100`)
    __lucroText.textContent = FORMAT().money(lucro)
  }

  methods._calcPercentage = (_valor, _receitas, _despesas, _comissoes) => {
    if (_valor === 0) return 0

    const percentage = (_valor * 100) / (_receitas + _despesas + _comissoes)
    return Number(percentage.toFixed(2))
  }

  return methods
}

module.exports = Module
