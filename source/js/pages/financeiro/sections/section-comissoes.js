/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __comissoesSection = document.querySelector('section.financeiro__pagos')

  const __comissoesTotalSpan = __comissoesSection.querySelector('.pagos__comissoes .valor span#comissao__mes')

  // Methods

  methods.load = (_comissoes) => {
    const total = methods._calcComissoes(_comissoes)
    __comissoesTotalSpan.innerText = FORMAT().money(total)

    return total
  }

  methods._calcComissoes = (_comissoes) => {
    return _comissoes.reduce((_total, _current) => {
      return _total + _current.valor
    }, 0)
  }

  return methods
}

module.exports = Module
