/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    TOGGLE().activeClickOnToggle()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
