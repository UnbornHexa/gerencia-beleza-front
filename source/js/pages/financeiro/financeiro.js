/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const SECTION_FINANCEIRO = require('./helpers/section-financeiro')

const DATE_SELECTOR = require('./extendeds/date-selector')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('financeiro')
  ASIDE_PICTURE().load()

  DATE_SELECTOR().init()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  SECTION_FINANCEIRO().updateValues()

  EVENTS().activeEvents()
}
start()
