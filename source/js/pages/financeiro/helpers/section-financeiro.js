/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')
const RESUMO_REQUEST = require('../requests/resumo')

const SECTION_RECEITAS = require('../sections/section-receitas')
const SECTION_DESPESAS = require('../sections/section-despesas')
const SECTION_COMISSOES = require('../sections/section-comissoes')
const SECTION_GORJETAS = require('../sections/section-gorjetas')
const SECTION_GRAFICO = require('../sections/section-grafico')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateValues = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const date = dateSelector.value()
    const yearAndMonth = `${date.year}-${date.month.toString().padStart(2, '0')}`

    try {
      const resumo = await RESUMO_REQUEST().getResumoFinanceiro(yearAndMonth)

      const totalReceitas = SECTION_RECEITAS().load(resumo.receitas)
      const totalDespesas = SECTION_DESPESAS().load(resumo.despesas)
      const totalComissoes = SECTION_COMISSOES().load(resumo.comissoes)
      SECTION_GORJETAS().load(resumo.gorjetas)

      SECTION_GRAFICO().build(totalReceitas, totalDespesas, totalComissoes)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
