/* Requires */

const MODAL_ADICIONAR = require('../modals/modal-adicionar')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('.adicionar__receita #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', () => {
      MODAL_ADICIONAR().showModal()
    })
  }

  return methods
}

module.exports = Module
