/* Requires */

const RECEITAS_REQUEST = require('../requests/receitas')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_receita .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modalData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await RECEITAS_REQUEST().addReceita(modalData)
        MODAL_ADICIONAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateValues()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
