/* Requires */

const RECEITAS_REQUEST = require('../requests/receitas')
const MODAL_EDITAR = require('../modals/modal-editar')

const HELPER_COMPONENTS_COMMUNICATOR = require('../helpers/components-communicator')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_receita .id input')
  const __editarButton = document.querySelector('section#modal_editar_receita .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const idReceita = __idInput.value
      const modalData = MODAL_EDITAR().exportDataFromModal()

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await RECEITAS_REQUEST().editReceita(idReceita, modalData)
        MODAL_EDITAR().hideModal()
        await HELPER_COMPONENTS_COMMUNICATOR().updateValues()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_receita input') &&
        !e.target.matches('section#modal_editar_receita textarea')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_receita .selecto__common')) return
      __editarButton.removeAttribute('disabled')
    })
    document.addEventListener('click', (e) => {
      if (!e.target.matches('section#modal_editar_receita .datepicker input')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
