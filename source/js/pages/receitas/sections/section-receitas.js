/* Requires */

const FORMAT = require('../../../global/helpers/format')
const SECTION_GRAFICO = require('./section-grafico')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __receitasSection = document.querySelector('section.financeiro__grafico')

  const __receitaTotalSpan = __receitasSection.querySelector('.grafico__complemento .valor span#receita__mes')
  const __receitaDinheiroSpan = __receitasSection.querySelector('.info .info__dinheiro span#receita_dinheiro')
  const __receitaDebitoSpan = __receitasSection.querySelector('.info .info__debito span#receita_debito')
  const __receitaCreditoSpan = __receitasSection.querySelector('.info .info__credito span#receita_credito')

  let __dinheiro = 0
  let __debito = 0
  let __credito = 0
  let __total = 0

  // Methods

  methods.load = (_receitas) => {
    const receitas = methods._calcReceitas(_receitas)

    __receitaDinheiroSpan.innerText = FORMAT().money(receitas.dinheiro)
    __receitaDebitoSpan.innerText = FORMAT().money(receitas.debito)
    __receitaCreditoSpan.innerText = FORMAT().money(receitas.credito)
    __receitaTotalSpan.innerText = FORMAT().money(receitas.total)

    SECTION_GRAFICO().build(receitas.dinheiro, receitas.debito, receitas.credito)

    return receitas.total
  }

  methods._calcReceitas = (_receitas) => {
    for (const index in _receitas) {
      const formaPagamento = _receitas[index]._id
      const valor = _receitas[index].valor

      if (formaPagamento === 'dinheiro') __dinheiro = valor
      else if (formaPagamento === 'debito') __debito = valor
      else if (formaPagamento === 'credito') __credito = valor
    }
    __total = __dinheiro + __debito + __credito

    return {
      dinheiro: __dinheiro,
      debito: __debito,
      credito: __credito,
      total: __total
    }
  }

  return methods
}

module.exports = Module
