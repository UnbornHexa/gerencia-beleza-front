const Module = () => {
  const methods = {}

  // Internal Variables

  const __graficoSection = document.querySelector('section.financeiro__grafico .grafico__content .grafico')
  const __dinheiroSVG = __graficoSection.querySelector('svg circle#donut_dinheiro')
  const __debitoSVG = __graficoSection.querySelector('svg circle#donut_debito')
  const __creditoSVG = __graficoSection.querySelector('svg circle#donut_credito')

  // Methods

  methods.build = (_dinheiro, _debito, _credito) => {
    const dinheiro = Number(_dinheiro) || 0
    const debito = Number(_debito) || 0
    const credito = Number(_credito) || 0

    const dinheiroPercentage = methods._calcPercentage(dinheiro, dinheiro, debito, credito)
    const debitoPercentage = methods._calcPercentage(debito, dinheiro, debito, credito)
    const creditoPercentage = methods._calcPercentage(credito, dinheiro, debito, credito)

    const somaDebitoCredito = debitoPercentage + creditoPercentage
    const somaTotal = dinheiroPercentage + somaDebitoCredito

    __dinheiroSVG.setAttribute('stroke-dasharray', `${somaTotal} 100`)
    __creditoSVG.setAttribute('stroke-dasharray', `${somaDebitoCredito} 100`)
    __debitoSVG.setAttribute('stroke-dasharray', `${debitoPercentage} 100`)
  }

  methods._calcPercentage = (_valor, _dinheiro, _debito, _credito) => {
    if (_valor === 0) return 0

    const percentage = (_valor * 100) / (_dinheiro + _debito + _credito)
    return Number(percentage.toFixed(2))
  }

  return methods
}

module.exports = Module
