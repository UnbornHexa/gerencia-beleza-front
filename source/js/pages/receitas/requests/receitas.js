/* Requires */

const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const HOSTS = require('../../../global/data/hosts')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __url = `${HOSTS().url.api_plataforma}/lancamentos`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getReceitas = async (_date) => {
    const url = `${__url}/${__idEmpresa}/?tipo=Receita&date=${_date}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          _response.body.sort(sortLancamentos)
          resolve(_response.body)
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.getReceitaById = async (_idReceita) => {
    const url = `${__url}/${__idEmpresa}/${_idReceita}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.addReceita = async (_data) => {
    const url = `${__url}/${__idEmpresa}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'POST', _data, __headersWithToken, _response => {
        if (_response.status === 201) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editReceita = async (_idReceita, _data) => {
    const url = `${__url}/${__idEmpresa}/${_idReceita}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PUT', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.delReceita = async (_idReceita) => {
    const url = `${__url}/${__idEmpresa}/${_idReceita}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'DELETE', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  // Sort Functions

  const sortLancamentos = (a, b) => {
    const nomeA = a.nome.replace('Comanda #', '')
    const nomeB = b.nome.replace('Comanda #', '')

    const dataA = a.data_registro
    const dataB = b.data_registro

    if (dataA === dataB) {
      return Number(nomeB) - Number(nomeA)
    }

    return (dataA > dataB) ? -1 : 1
  }

  return methods
}

module.exports = Module
