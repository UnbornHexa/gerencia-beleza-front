/* Requires */

const SYMBOL = require('../../../global/helpers/symbols')

const SECTION_RECEITAS = require('../sections/section-receitas')

const RECEITAS_REQUEST = require('../requests/receitas')
const RESUMO_REQUEST = require('../requests/resumo')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateValues = async () => {
    const dateSelector = SYMBOL().retrieve('date_selector_principal')
    const date = dateSelector.value()
    const yearAndMonth = `${date.year}-${date.month.toString().padStart(2, '0')}`

    await methods._updateReceitas(yearAndMonth)
    await methods._updateResumo(yearAndMonth)
  }

  methods._updateReceitas = async (_yearAndMonth) => {
    const listaReceitas = SYMBOL().retrieve('lista_receitas')

    try {
      const receitas = await RECEITAS_REQUEST().getReceitas(_yearAndMonth)
      listaReceitas.importItems(receitas)
    } catch (e) { console.log(e) }
  }

  methods._updateResumo = async (_yearAndMonth) => {
    try {
      const resumo = await RESUMO_REQUEST().getResumoFinanceiro(_yearAndMonth)
      SECTION_RECEITAS().load(resumo.receitas)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
