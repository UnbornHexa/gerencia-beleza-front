/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')
const FORMAT = require('../../../global/helpers/format')
const CONVERT = require('../../../global/helpers/convert')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const RECEITAS_REQUEST = require('../requests/receitas')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_receita) {
    const itemsDiv = this.__component.querySelector('.lista__itens')
    const formaPagamento = (_receita.forma_pagamento === 'dinheiro') ? 'dinheiro'
      : (_receita.forma_pagamento === 'debito') ? 'débito' : 'crédito'

    const li =
      `
    <li>
      <span class="item__id" hidden>${_receita._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-chart-line"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_receita.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__valor">${FORMAT().money(_receita.valor)} - em ${formaPagamento}</span>
        </div>
      </div>
      <div class="lista__data">
        <span class="item__data__vencimento">pago em ${CONVERT().dateBR(_receita.data_vencimento)}</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idReceita = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const receita = await RECEITAS_REQUEST().getReceitaById(idReceita)
        MODAL_VISUALIZAR().importDataToModal(receita)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idReceita = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const receita = await RECEITAS_REQUEST().getReceitaById(idReceita)
        MODAL_EDITAR().importDataToModal(receita)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idReceita = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeReceita = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_receita .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_receita .modal__header p')

      inputIdModal.value = idReceita
      headerNomeModal.innerText = nomeReceita
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__receitas')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_receitas', component)
  }

  return methods
}

module.exports = Module
