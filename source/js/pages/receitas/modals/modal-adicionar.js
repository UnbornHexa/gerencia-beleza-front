/* Requires */

const DATE = require('../../../global/helpers/date')
const FORMAT = require('../../../global/helpers/format')
const MESSAGEBOX = require('../../../global/modules/messagebox')
const SYMBOL = require('../../../global/helpers/symbols')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_adicionar_receita')

  const __nomeInput = __modal.querySelector('.nome input')
  const __dataDatepicker = SYMBOL().retrieve('datapicker_adicionar')
  const __valorInput = __modal.querySelector('.valor input')

  const __formaPagamentoSelecto = SYMBOL().retrieve('selecto_formaPagamento_adicionar')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.checkRequiredFields = () => {
    if (__nomeInput.value && __dataDatepicker.value() && __valorInput.value &&
      !__formaPagamentoSelecto.isEmpty()) return true

    MESSAGEBOX().alert('Preencha todos os campos obrigatórios.')
    return false
  }

  methods.resetForm = () => {
    __nomeInput.value = ''
    __dataDatepicker.clear()
    __valorInput.value = ''
    __formaPagamentoSelecto.resetSelected()

    methods.importDataToModal()
  }

  // Data

  methods.importDataToModal = () => {
    const currentDate = DATE().today()
    __dataDatepicker.selectDate(currentDate)
  }

  methods.exportDataFromModal = () => {
    const receita = {}

    // required
    receita.nome = __nomeInput.value || ''
    receita.data_vencimento = __dataDatepicker.value() || ''
    receita.forma_pagamento = __formaPagamentoSelecto.value() || ''
    receita.valor = FORMAT().unformatMoney(__valorInput.value) || 0
    receita.tipo = 'Receita'

    return receita
  }

  return methods
}

module.exports = Module
