/* Requires */

const HOSTS = require('../../../global/data/hosts')
const HTTP = require('../../../global/helpers/http')
const TOKEN = require('../../../global/helpers/token')
const MESSAGEBOX = require('../../../global/modules/messagebox')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idEmpresa = TOKEN().getIdEmpresaFromToken()
  const __idUsuario = TOKEN().getIdUsuarioFromToken()
  const __url = `${HOSTS().url.api_plataforma}/usuarios`
  const __headersWithToken = HTTP().getHeadersWithToken()

  // Methods

  methods.getUsuario = async () => {
    const url = `${__url}/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'GET', null, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body[0])
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editUsuario = async (_data) => {
    const url = `${__url}/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PUT', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editUsuarioPassword = async (_data) => {
    const url = `${__url}/edit-senha/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().request(url, 'PATCH', _data, __headersWithToken, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  methods.editUsuarioImage = async (_imagem) => {
    const url = `${__url}/edit-imagem/${__idEmpresa}/${__idUsuario}`

    return new Promise((resolve, reject) => {
      HTTP().sendFile(url, 'PATCH', _imagem, _response => {
        if (_response.status === 200) {
          resolve(_response.body)
          MESSAGEBOX().alert(_response.body, 'success')
          return
        }

        MESSAGEBOX().alert(_response.body, 'error')
        reject(new Error(_response.body))
      })
    })
  }

  return methods
}

module.exports = Module
