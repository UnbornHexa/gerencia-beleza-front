/* Requires */

const THEME = require('../../../global/helpers/theme')

const PERFIL_HELPER = require('../helpers/section-perfil')
const SECTION_MAIN = require('../sections/sessao-perfil')
const HELPER_IMAGE_VALIDATOR = require('../helpers/image-validator')
const HELPER_AVATAR = require('../helpers/avatar')

const MODAL_ALTERAR_SENHA = require('../modals/modal-alterar-senha')
const MODAL_EDITAR = require('../modals/modal-editar')

const USUARIOS_REQUEST = require('../requests/usuarios')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __avatarInput = document.querySelector('section.perfil .info__image .foto__input')

  const __avatarImagem = document.querySelector('section.perfil .info__image')

  const __modoNoturnoToggle = document.querySelector('section.sobre .sobre__noturno .toggle .toggle__label')
  const __modoNoturnoCheckbox = document.querySelector('section.sobre .sobre__noturno .toggle #modo__noturno')

  const __logoutDiv = document.querySelector('section.opcoes .button #logout')
  const __alterarSenhaDiv = document.querySelector('section.opcoes .button #alterar_senha')
  const __editarPerfilDiv = document.querySelector('section.opcoes .button #editar_perfil')

  // Theme Noturno

  methods.activeClickOnToggleModoNoturno = () => {
    __modoNoturnoToggle.addEventListener('click', () => {
      __modoNoturnoCheckbox.checked = !__modoNoturnoCheckbox.checked
      if (__modoNoturnoCheckbox.checked) { THEME().activeDarkTheme() } else { THEME().removeDarkTheme() }
    })
  }

  // Image

  methods.activeClickOnAlterarAvatarChange = () => {
    __avatarImagem.addEventListener('click', () => {
      __avatarInput.click()
    })

    __avatarInput.addEventListener('change', async () => {
      const imagem = __avatarInput.files[0]
      if (!HELPER_IMAGE_VALIDATOR().checkType(imagem)) return
      if (!HELPER_IMAGE_VALIDATOR().checkMaxSize(imagem)) return

      const imagemFormData = HELPER_IMAGE_VALIDATOR().convertFormData(imagem)

      try {
        document.querySelector('body').style.cursor = 'progress'
        await USUARIOS_REQUEST().editUsuarioImage(imagemFormData)
        await PERFIL_HELPER().updateInfo()
        await HELPER_AVATAR().updateImage()
      } catch (e) { console.log(e) }

      document.querySelector('body').style.cursor = 'default'
    })
  }

  // Buttons

  methods.activeClickOnLogout = () => {
    __logoutDiv.parentElement.addEventListener('click', () => {
      SECTION_MAIN().logout()
    })
  }

  methods.activeClickOnAlterarSenha = () => {
    __alterarSenhaDiv.parentElement.addEventListener('click', () => {
      MODAL_ALTERAR_SENHA().showModal()
    })
  }

  methods.activeClickOnEditarPerfil = () => {
    __editarPerfilDiv.parentElement.addEventListener('click', async () => {
      MODAL_EDITAR().showModal()

      try {
        const usuario = await USUARIOS_REQUEST().getUsuario()
        MODAL_EDITAR().importDataToModal(usuario)
      } catch (e) { console.log(e) }
    })
  }

  return methods
}

module.exports = Module
