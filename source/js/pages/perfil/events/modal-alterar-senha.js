/* Requires */

const USUARIOS_REQUEST = require('../requests/usuarios')
const MODAL_ALTERAR_SENHA = require('../modals/modal-alterar-senha')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __alterarButton = document.querySelector('section#modal_alterar_senha .modal__button #alterar')

  // Methods

  methods.activeClickOnAlterarButton = () => {
    __alterarButton.addEventListener('click', async () => {
      const modalData = MODAL_ALTERAR_SENHA().exportDataFromModal()

      const modalOk = MODAL_ALTERAR_SENHA().checkRequiredFields()
      if (modalOk === false) return

      const senhasIguais = MODAL_ALTERAR_SENHA().checkEqualsPassword()
      if (senhasIguais === false) return

      const senhaOk = MODAL_ALTERAR_SENHA().checkPasswordSize()
      if (senhaOk === false) return

      try {
        __alterarButton.setAttribute('disabled', true)
        await USUARIOS_REQUEST().editUsuarioPassword(modalData)
        MODAL_ALTERAR_SENHA().hideModal()
      } catch (e) { console.log(e) }

      __alterarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
