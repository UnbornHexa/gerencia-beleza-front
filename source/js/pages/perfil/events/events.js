/* Requires */

const GLOBAL_EVENTS = require('../../../global/events/events')
const WINDOW = require('../../../global/events/window')
const CHAT = require('../../../global/events/chat')

const SECTION_MAIN = require('./section-main')
const MODAL_ALTERAR_SENHA = require('./modal-alterar-senha')
const MODAL_EDITAR = require('./modal-editar')

const TOGGLE = require('../../../structure/navbar/events/toggle')

/* Module */

const Module = () => {
  const methods = {}

  methods.activeEvents = () => {
    GLOBAL_EVENTS().activeEvents()
    WINDOW().checkIdle()

    TOGGLE().activeClickOnToggle()

    MODAL_ALTERAR_SENHA().activeClickOnAlterarButton()

    MODAL_EDITAR().activeClickOnEditarButton()
    MODAL_EDITAR().activeKeyupOnCep()
    MODAL_EDITAR().activeChangeOnModal()

    SECTION_MAIN().activeClickOnToggleModoNoturno()
    SECTION_MAIN().activeClickOnAlterarAvatarChange()
    SECTION_MAIN().activeClickOnLogout()
    SECTION_MAIN().activeClickOnAlterarSenha()
    SECTION_MAIN().activeClickOnEditarPerfil()

    CHAT().activeChat()
  }

  return methods
}

module.exports = Module
