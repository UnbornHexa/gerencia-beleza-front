/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')
const ASIDE_LINK = require('./sections/aside')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')
const HELPER_TOGGLE_THEME = require('./helpers/toggle-theme')
const HELPER_PROFILE_PICTURE = require('./helpers/profile-picture')
const SECTION_PERFIL = require('./helpers/section-perfil')

const LISTA_PAGAMENTOS = require('./extendeds/lista-pagamentos')
const SELECTO_ESTADO = require('./extendeds/selecto-estado')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_PICTURE().load()
  ASIDE_LINK().activePerfil()
  HELPER_PROFILE_PICTURE().load()

  LISTA_PAGAMENTOS().init()
  SELECTO_ESTADO().init('section#modal_editar_perfil .endereco__estado .selecto__common', 'selecto_estado_editar')
  HELPER_TOGGLE_THEME().checkActivedTheme()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_PAGAMENTOS().updateItems()
  SECTION_PERFIL().updateInfo()

  EVENTS().activeEvents()
}
start()
