/* Requires */

const FORMAT = require('../../../global/helpers/format')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __nomeLabel = document.querySelector('section.perfil #perfil_nome')
  const __permissaoLabel = document.querySelector('section.perfil #perfil_permissao')
  const __comissaoReceberLabel = document.querySelector('section.sobre .receber__comissao span')
  const __gorjetaReceberLabel = document.querySelector('section.sobre .receber__gorjeta span')

  const __avatarImg = document.querySelector('section.perfil .info__image img')

  // Methods

  methods.importUserProfile = (_data) => {
    __nomeLabel.innerText = _data.nome || ''
    __permissaoLabel.innerText = _data.id_permissao.nome || ''
    __comissaoReceberLabel.innerText = FORMAT().money(_data.comissao_pendente) || 'R$ 0,00'
    __gorjetaReceberLabel.innerText = FORMAT().money(_data.gorjeta_pendente) || 'R$ 0,00'

    if (_data.imagem) __avatarImg.src = _data.imagem
  }

  // Logout

  methods.logout = () => {
    window.localStorage.removeItem('token-usuario')
    window.localStorage.removeItem('imagem-avatar')
    window.localStorage.removeItem('imagem-empresa')
    window.location.assign('../entrar')
  }

  return methods
}

module.exports = Module
