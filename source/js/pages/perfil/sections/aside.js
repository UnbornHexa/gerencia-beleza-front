const Module = () => {
  const methods = {}

  // Internal Variables

  const perfilLink = document.querySelector('aside .aside__perfil a')

  // Methods

  methods.activePerfil = () => {
    perfilLink.classList.add('active')
  }

  return methods
}

module.exports = Module
