/* Requires */

const SECTION_MAIN = require('../sections/sessao-perfil')
const USUARIOS_REQUEST = require('../requests/usuarios')

/* Module */

const Module = () => {
  const methods = {}

  methods.updateInfo = async () => {
    try {
      const profile = await USUARIOS_REQUEST().getUsuario()
      SECTION_MAIN().importUserProfile(profile)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
