const Module = () => {
  const methods = {}

  // Internal Variables

  const __modoNoturnoCheckbox = document.querySelector('section.sobre .sobre__noturno .toggle #modo__noturno')

  // Methods

  methods.checkActivedTheme = () => {
    if (window.localStorage.getItem('dark-theme')) __modoNoturnoCheckbox.checked = true
  }

  return methods
}

module.exports = Module
