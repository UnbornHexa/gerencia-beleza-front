/* Requires */

const CONVERT = require('../../../global/helpers/convert')
const FORMAT = require('../../../global/helpers/format')

const LISTA_COMPONENT = require('../../../global/components/lista')
const SYMBOL = require('../../../global/helpers/symbols')

const PAGAMENTOS_REQUEST = require('../requests/lancamentos')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_pagamento) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li class="${_pagamento.tipo.toLowerCase()}">
      <span class="item__id" hidden>${_pagamento._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-sack-dollar comissao"></i>
          <i class="icon-coins gorjeta"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_pagamento.tipo}</span>
          <span class="item__valor">${FORMAT().money(_pagamento.valor)}</span>
        </div>
      </div>
      <div class="lista__data">
        <span class="item__data">recebido em ${CONVERT().dateBR(_pagamento.data_vencimento)}</span>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__pagamentos')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()

    SYMBOL().subscribe('lista_pagamentos', component)
  }

  methods.updateItems = async () => {
    const listaPagamentos = SYMBOL().retrieve('lista_pagamentos')

    try {
      const pagamentos = await PAGAMENTOS_REQUEST().getPagamentos()
      listaPagamentos.importItems(pagamentos)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
