/* Requires */

const MESSAGEBOX = require('../../../global/modules/messagebox')
const MAIN = require('../../../structure/main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __modal = document.querySelector('section#modal_alterar_senha')

  const __senhaAtualInput = __modal.querySelector('.senha input')
  const __senhaNovaInput = __modal.querySelector('.senha__nova input')
  const __senhaConfirmacaoInput = __modal.querySelector('.senha__confirmacao input')

  // Modal

  methods.hideModal = () => {
    __modal.style.display = 'none'
    methods.resetForm()
    MAIN().enableMain()
  }

  methods.showModal = () => {
    __modal.style.display = 'flex'
    methods.resetForm()
    MAIN().disableMain()
  }

  // Form

  methods.resetForm = () => {
    __senhaAtualInput.value = ''
    __senhaNovaInput.value = ''
    __senhaConfirmacaoInput.value = ''
  }

  methods.checkRequiredFields = () => {
    if (__senhaAtualInput.value && __senhaNovaInput.value && __senhaConfirmacaoInput.value) return true

    MESSAGEBOX().alert('Preencha os campos obrigatórios.')
    return false
  }

  methods.checkEqualsPassword = () => {
    if (__senhaNovaInput.value === __senhaConfirmacaoInput.value) return true

    MESSAGEBOX().alert('As senhas devem ser iguais.')
    return false
  }

  methods.checkPasswordSize = () => {
    if (__senhaNovaInput.value.length >= 6) return true

    MESSAGEBOX().alert('A senha deve ter no mínimo 6 caracteres.')
    return false
  }

  // Data

  methods.exportDataFromModal = () => {
    const usuario = {}

    // required
    usuario.senha_atual = __senhaAtualInput.value || ''
    usuario.senha_nova = __senhaNovaInput.value || ''

    return usuario
  }

  return methods
}

module.exports = Module
