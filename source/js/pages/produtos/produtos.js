/* Requires */

require('regenerator-runtime/runtime')

const PERMISSION = require('../../global/helpers/permission')
const TOKEN = require('../../global/helpers/token')

const ASIDE_ACTIVE = require('../../structure/aside/helpers/active-aside')
const ASIDE_PICTURE = require('../../structure/aside/helpers/profile-picture')

const EVENTS = require('./events/events')

const HELPER_NOTIFICACOES = require('./helpers/notificacoes')
const HELPER_AVATAR = require('./helpers/avatar')

const LISTA_PRODUTOS = require('./extendeds/lista-produtos')

/* Start */

const start = async () => {
  TOKEN().redirectIfExpiredToken()

  PERMISSION().checkEmpresaExpired()
  PERMISSION().hideNotAllowedAreas()

  ASIDE_ACTIVE().activeLink('produtos')
  ASIDE_PICTURE().load()

  LISTA_PRODUTOS().init()

  HELPER_AVATAR().updateImage()
  HELPER_NOTIFICACOES().checkNews()
  LISTA_PRODUTOS().updateItems()

  EVENTS().activeEvents()
}
start()
