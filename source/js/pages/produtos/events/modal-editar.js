/* Requires */

const PRODUTOS_REQUEST = require('../requests/produtos')
const MODAL_EDITAR = require('../modals/modal-editar')
const LISTA_PRODUTOS = require('../extendeds/lista-produtos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_editar_produto .id input')
  const __editarButton = document.querySelector('section#modal_editar_produto .modal__button #editar')

  // Methods

  methods.activeClickOnEditarButton = () => {
    __editarButton.addEventListener('click', async () => {
      const modalData = MODAL_EDITAR().exportDataFromModal()
      const idProduto = __idInput.value

      const modalOk = MODAL_EDITAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __editarButton.setAttribute('disabled', true)
        await PRODUTOS_REQUEST().editProduto(idProduto, modalData)
        MODAL_EDITAR().hideModal()
        await LISTA_PRODUTOS().updateItems()
      } catch (e) { console.log(e) }

      __editarButton.removeAttribute('disabled')
    })
  }

  // Active Disabled Button

  methods.activeChangeOnModal = () => {
    document.addEventListener('input', (e) => {
      if (!e.target.matches('section#modal_editar_produto input') &&
        !e.target.matches('section#modal_editar_produto textarea')) return
      __editarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
