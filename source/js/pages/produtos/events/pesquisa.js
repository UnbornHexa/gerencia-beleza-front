/* Requires */

const ACCENT = require('../../../global/helpers/accent')
const SYMBOL = require('../../../global/helpers/symbols')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __listaProdutos = SYMBOL().retrieve('lista_produtos')
  const __pesquisaInput = document.querySelector('section.top .top__pesquisa input')
  let __searchClearTimeout = {}

  // Methods

  methods.activeKeyupOnPesquisa = () => {
    __pesquisaInput.addEventListener('keyup', (e) => {
      clearTimeout(__searchClearTimeout)

      __searchClearTimeout = setTimeout(() => {
        const text = e.target.value

        if (text === '') {
          __listaProdutos.getState().filteredItems = __listaProdutos.getState().items
          __listaProdutos._loadItemsFromPage(1)
          return
        }

        __listaProdutos.getState().filteredItems = []
        const textSanitized = ACCENT().remove(text.toString().toUpperCase())

        for (const index in __listaProdutos.getState().items) {
          const item = __listaProdutos.getState().items[index]
          const itemNameSanitized = ACCENT().remove(item.nome.toString().toUpperCase())

          if (itemNameSanitized.includes(textSanitized)) {
            __listaProdutos.getState().filteredItems.push(item)
          }
        }

        __listaProdutos._loadItemsFromPage(1)
      }, 500)
    })
  }

  return methods
}

module.exports = Module
