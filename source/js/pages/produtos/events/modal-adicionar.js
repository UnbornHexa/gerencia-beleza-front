/* Requires */

const PRODUTOS_REQUEST = require('../requests/produtos')
const MODAL_ADICIONAR = require('../modals/modal-adicionar')

const LISTA_PRODUTOS = require('../extendeds/lista-produtos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __adicionarButton = document.querySelector('section#modal_adicionar_produto .modal__button #adicionar')

  // Methods

  methods.activeClickOnAdicionarButton = () => {
    __adicionarButton.addEventListener('click', async () => {
      const modaData = MODAL_ADICIONAR().exportDataFromModal()

      const modalOk = MODAL_ADICIONAR().checkRequiredFields()
      if (modalOk === false) return

      try {
        __adicionarButton.setAttribute('disabled', true)
        await PRODUTOS_REQUEST().addProduto(modaData)
        MODAL_ADICIONAR().hideModal()
        await LISTA_PRODUTOS().updateItems()
      } catch (e) { console.log(e) }

      __adicionarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
