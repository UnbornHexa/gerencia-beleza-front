/* Requires */

const PRODUTOS_REQUEST = require('../requests/produtos')
const MODAL_DELETAR = require('../modals/modal-deletar')
const LISTA_PRODUTOS = require('../extendeds/lista-produtos')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __idInput = document.querySelector('section#modal_deletar_produto .id input')
  const __deletarButton = document.querySelector('section#modal_deletar_produto .modal__button #deletar')

  // Methods

  methods.activeClickOnDeletarButton = () => {
    __deletarButton.addEventListener('click', async () => {
      const idProduto = __idInput.value

      try {
        __deletarButton.setAttribute('disabled', true)
        await PRODUTOS_REQUEST().delProduto(idProduto)
        MODAL_DELETAR().hideModal()
        await LISTA_PRODUTOS().updateItems()
      } catch (e) { console.log(e) }

      __deletarButton.removeAttribute('disabled')
    })
  }

  return methods
}

module.exports = Module
