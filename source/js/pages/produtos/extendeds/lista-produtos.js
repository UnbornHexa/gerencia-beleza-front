/* Requires */

const LISTA_COMPONENT = require('../../../global/components/lista')
const FORMAT = require('../../../global/helpers/format')
const SYMBOL = require('../../../global/helpers/symbols')

const MODAL_VISUALIZAR = require('../modals/modal-visualizar')
const MODAL_EDITAR = require('../modals/modal-editar')
const MODAL_DELETAR = require('../modals/modal-deletar')
const PRODUTOS_REQUEST = require('../requests/produtos')

/* Extended Class */

class ListaExtended extends LISTA_COMPONENT {
  // Items

  _addItem (_produto) {
    const itemsDiv = this.__component.querySelector('.lista__itens')

    const li =
      `
    <li>
      <span class="item__id" hidden>${_produto._id}</span>
      <div class="lista__content">
        <div class="lista__avatar">
          <i class="icon-flask-potion"></i>
        </div>
        <div class="lista__dados">
          <span class="item__nome">${_produto.nome}

            <div class="lista__opcoes">
              <!-- Visualizar -->
              <div class="opcoes__visualizar">
                <i class="icon-eye"></i>
                <span>Visualizar</span>
              </div>
              <!-- Editar -->
              <div class="opcoes__editar">
                <i class="icon-edit"></i>
                <span>Editar</span>
              </div>
              <!-- Deletar -->
              <div class="opcoes__deletar">
                <i class="icon-trash-alt"></i>
                <span>Deletar</span>
              </div>
            </div>

          </span>
          <span class="item__quantidade">${FORMAT().number(_produto.quantidade)} unidades</span>
        </div>
      </div>
      <div class="lista__preco">
        <span class="item__preco">${FORMAT().money(_produto.valor)} a unidade</span>
      </div>
      <div class="lista__abrir__opcoes">
        <i class="icon-arrow-thin-down"></i>
      </div>
    </li>
    `

    itemsDiv.innerHTML += li
  }

  // Events

  activeClickOnMenuVisualizar () {
    const match = `${this.__id} .lista__opcoes .opcoes__visualizar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_VISUALIZAR().showModal()
      const idProduto = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const produto = await PRODUTOS_REQUEST().getProdutoById(idProduto)
        MODAL_VISUALIZAR().importDataToModal(produto)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuEditar () {
    const match = `${this.__id} .lista__opcoes .opcoes__editar`
    document.addEventListener('click', async (e) => {
      if (!e.target.matches(match)) return

      MODAL_EDITAR().showModal()
      const idProduto = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText

      try {
        const produto = await PRODUTOS_REQUEST().getProdutoById(idProduto)
        MODAL_EDITAR().importDataToModal(produto)
      } catch (e) { console.log(e) }
    })
  }

  activeClickOnMenuDeletar () {
    const match = `${this.__id} .lista__opcoes .opcoes__deletar`
    document.addEventListener('click', (e) => {
      if (!e.target.matches(match)) return

      MODAL_DELETAR().showModal()
      const idProduto = e.target.parentElement.parentElement.parentElement
        .parentElement.parentElement.querySelector('span.item__id').innerText
      const nomeProduto = e.target.parentElement.parentElement.parentElement.querySelector('span.item__nome').innerText

      const inputIdModal = document.querySelector('section#modal_deletar_produto .id input')
      const headerNomeModal = document.querySelector('section#modal_deletar_produto .modal__header p')

      inputIdModal.value = idProduto
      headerNomeModal.innerText = nomeProduto
    })
  }
}

/* Module */

const Module = () => {
  const methods = {}

  methods.init = () => {
    const component = new ListaExtended('section#lista__produtos')

    component.render()

    component.activeClickOnFirsrPage()
    component.activeClickOnPreviousPage()
    component.activeClickOnNextPage()
    component.activeClickOnLastPage()
    component.activeClickOnLi()
    component.activeClickOutOfLi()
    component.activeClickOnMenuVisualizar()
    component.activeClickOnMenuEditar()
    component.activeClickOnMenuDeletar()

    SYMBOL().subscribe('lista_produtos', component)
  }

  methods.updateItems = async () => {
    const listaProdutos = SYMBOL().retrieve('lista_produtos')

    try {
      const produtos = await PRODUTOS_REQUEST().getProdutos()
      listaProdutos.importItems(produtos)
    } catch (e) { console.log(e) }
  }

  return methods
}

module.exports = Module
