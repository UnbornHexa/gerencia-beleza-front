/* Requires */

const MAIN = require('../../main/helpers/block-content')

/* Module */

const Module = () => {
  const methods = {}

  // Internal Variables

  const __toggle = document.querySelector('navbar .navbar__funcionalidades .ferramentas__toggle')

  // Methods

  methods.activeClickOnToggle = () => {
    __toggle.addEventListener('click', () => {
      const asideDiv = document.querySelector('aside')

      __toggle.classList.toggle('show')
      asideDiv.classList.toggle('close')

      if (!asideDiv.classList.contains('close')) {
        MAIN().disableMain()
        return
      }

      MAIN().enableMain()
    })
  }

  return methods
}

module.exports = Module
