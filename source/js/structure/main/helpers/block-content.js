const Module = () => {
  const methods = {}

  // Internal Variables

  const __main = document.querySelector('main')
  const __body = document.body

  // Methods

  methods.disableMain = () => {
    __body.style.overflow = 'hidden'
    __main.style.filter = 'opacity(50%)'
    __main.classList.add('disabled')
  }

  methods.enableMain = () => {
    __body.style.overflow = 'auto'
    __main.style.filter = 'opacity(100%)'
    __main.classList.remove('disabled')
  }

  return methods
}

module.exports = Module
