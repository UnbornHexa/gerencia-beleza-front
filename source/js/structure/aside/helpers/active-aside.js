const Module = () => {
  const methods = {}

  methods.activeLink = _link => {
    const a = document.querySelector(`aside .aside__funcionalidades a[name="${_link}"`)
    a.classList.add('active')
  }

  methods.hideLink = _link => {
    const a = document.querySelector(`aside .aside__funcionalidades a[name="${_link}"`)
    a.style.display = 'none'
  }

  return methods
}

module.exports = Module
