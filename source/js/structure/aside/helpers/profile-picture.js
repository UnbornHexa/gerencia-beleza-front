const Module = () => {
  const methods = {}

  // Internal Variables

  const __asideAvatarImg = document.querySelector('aside .aside__perfil a img')
  const __navbarMobileAvatarImg = document.querySelector('navbar .navbar__perfil a img')

  // Methods

  methods.load = () => {
    const picture = window.localStorage.getItem('imagem-avatar')
    if (typeof (picture) !== 'string' || !picture.includes('https://')) return
    showPicture(picture)
  }

  const showPicture = (_url) => {
    __asideAvatarImg.src = _url
    __navbarMobileAvatarImg.src = _url
  }

  return methods
}

module.exports = Module
