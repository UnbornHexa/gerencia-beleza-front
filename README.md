1. Baixando o Projeto
```
git clone https://gitlab.com/gerencia-beleza-v2/frontend/ssr-app.git
```

2. Instalando as Dependências
```
cd ssr-app
yarn install
```

3. Executando a Aplicação
```
yarn run dev
```
