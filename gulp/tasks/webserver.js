/*************
|  REQUIRES  |
*************/

const gulp = require('gulp')
const gls = require('gulp-live-server')
const browserSync = require('browser-sync').create()

/***********
|  BUNDLE  |
***********/

gulp.task('start-webserver', function () {
  const server = gls.new('../ssr/bin/http.js')
  server.start()

  /* Assets */
  browserSync.init({
    files: '../build/**',
    proxy: 'localhost:54010',
    port: 8080,
    watchOptions: {
      ignoreInitial: true
    }
  })

  /* SSR -> Restart Server After Change Files */
  gulp.watch('../ssr/**').on('change', function () {
    server.start.bind(server)()
  })
})

/**********
|  GERAL  |
**********/

gulp.task('app.webserver', gulp.parallel(
  [
    'start-webserver'
  ]
))
