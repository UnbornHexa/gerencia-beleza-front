/*************
|  REQUIRES  |
*************/

const gulp = require('gulp')
const browserify = require('browserify')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')
const es = require('event-stream')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

const mode = require('gulp-mode')()

/**********
|  CONST  |
**********/

const babelConfiguracoes = {
  minified: true,
  comments: false,
  presets: ['@babel/preset-env']
}

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/js'),
  build: path.join(__dirname, '../../build/js')
}

const files = [
  route.source + '/pages/agenda/agenda.js',
  route.source + '/pages/ajuda/ajuda.js',
  route.source + '/pages/bem-vindo/bem-vindo.js',
  route.source + '/pages/clientes/clientes.js',
  route.source + '/pages/comandas/comandas.js',
  route.source + '/pages/comissoes/comissoes.js',
  route.source + '/pages/despesas/despesas.js',
  route.source + '/pages/entrar/entrar.js',
  route.source + '/pages/empresa/empresa.js',
  route.source + '/pages/financeiro/financeiro.js',
  route.source + '/pages/funcionarios/funcionarios.js',
  route.source + '/pages/gorjetas/gorjetas.js',
  route.source + '/pages/materiais/materiais.js',
  route.source + '/pages/notificacoes/notificacoes.js',
  route.source + '/pages/nova-senha/nova-senha.js',
  route.source + '/pages/perfil/perfil.js',
  route.source + '/pages/permissoes/permissoes.js',
  route.source + '/pages/produtos/produtos.js',
  route.source + '/pages/receitas/receitas.js',
  route.source + '/pages/registrar/registrar.js',
  route.source + '/pages/servicos/servicos.js'
]

/***********
|  BUNDLE  |
***********/

gulp.task('bundle-pages', function (done) {
  const tasks = files.map(entry => {
    const entryName = entry.split('/')[entry.split('/').length - 1]

    return browserify(entry)
      .bundle()
      .pipe(source(entryName))
      .pipe(buffer())
      .pipe(plumber({
        errorHandler: error => {
          notify.onError(errorMessage)(error)
        }
      }))
      .pipe(mode.production(babel(babelConfiguracoes)))
      .pipe(mode.production(uglify()))
      .pipe(gulp.dest(route.build + '/pages'))
  })

  es.merge.apply(null, tasks)
  es.merge(tasks).on('end', done)
})

/**********
|  GERAL  |
**********/

gulp.task('app.js', gulp.parallel(
  [
    'bundle-pages'
  ]
))
