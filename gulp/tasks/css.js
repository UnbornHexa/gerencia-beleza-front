/*************
|  REQUIRES  |
*************/

const gulp = require('gulp')
const uglifyCss = require('gulp-uglifycss')
const cssImport = require('gulp-cssimport')
const autoprefixer = require('gulp-autoprefixer')

const es = require('event-stream')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

const mode = require('gulp-mode')()

/**********
|  CONST  |
**********/

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/css'),
  build: path.join(__dirname, '../../build/css')
}

const files = [
  route.source + '/pages/agenda/agenda.css',
  route.source + '/pages/ajuda/ajuda.css',
  route.source + '/pages/bem-vindo/bem-vindo.css',
  route.source + '/pages/clientes/clientes.css',
  route.source + '/pages/comandas/comandas.css',
  route.source + '/pages/comissoes/comissoes.css',
  route.source + '/pages/despesas/despesas.css',
  route.source + '/pages/entrar/entrar.css',
  route.source + '/pages/empresa/empresa.css',
  route.source + '/pages/erro/erro.css',
  route.source + '/pages/financeiro/financeiro.css',
  route.source + '/pages/funcionarios/funcionarios.css',
  route.source + '/pages/gorjetas/gorjetas.css',
  route.source + '/pages/materiais/materiais.css',
  route.source + '/pages/notificacoes/notificacoes.css',
  route.source + '/pages/nova-senha/nova-senha.css',
  route.source + '/pages/perfil/perfil.css',
  route.source + '/pages/permissoes/permissoes.css',
  route.source + '/pages/produtos/produtos.css',
  route.source + '/pages/receitas/receitas.css',
  route.source + '/pages/registrar/registrar.css',
  route.source + '/pages/servicos/servicos.css'
]

/***********
|  BUNDLE  |
***********/

gulp.task('bundle-pages', function (done) {
  const tasks = files.map(entry => {
    return gulp.src(entry)
      .pipe(plumber({
        errorHandler: error => {
          notify.onError(errorMessage)(error)
        }
      }))
      .pipe(cssImport())
      .pipe(autoprefixer())
      .pipe(mode.production(uglifyCss()))
      .pipe(gulp.dest(route.build))
  })
  es.merge.apply(null, tasks)
  es.merge(tasks).on('end', done)
})

/**********
|  GERAL  |
**********/

gulp.task('app.css', gulp.parallel(
  [
    'bundle-pages'
  ]
))
