/*************
|  REQUIRES  |
*************/

const gulp = require('gulp')
const path = require('path')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const mode = require('gulp-mode')()

const handlebars = require('gulp-compile-handlebars')
const htmlmin = require('gulp-htmlmin')
const rename = require('gulp-rename')

/**********
|  CONST  |
**********/

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/html'),
  build: path.join(__dirname, '../../build/html'),
  config: path.join(__dirname, '../../ssr/config')
}

const optionsHTML = {
  collapseWhitespace: true,
  removeComments: true
}

const version = require(`${route.config}/versionamento.json`)

/***********
|  BUNDLE  |
***********/

function bundlePage (_page) {
  const PATH_PAGE = `${route.source}/pages/${_page}/${_page}.hbs`
  const PATH_PARTIALS = `${route.source}/pages/${_page}/partials`
  const PATH_STRUCTURES = `${route.source}/structures`
  const PATH_OTHER = `${route.source}/other`

  const templateData = {
    versao: {
      css: version.css,
      js: version.js
    }
  }
  const templateOptions = {
    ignorePartials: true,
    batch: [PATH_PARTIALS, PATH_STRUCTURES, PATH_OTHER]
  }

  return function () {
    return gulp.src(PATH_PAGE)
      .pipe(plumber({
        errorHandler: error => {
          notify.onError(errorMessage)(error)
        }
      }))
      .pipe(handlebars(templateData, templateOptions))
      .pipe(rename({ extname: '.html' }))
      .pipe(mode.production(htmlmin(optionsHTML)))
      .pipe(gulp.dest(route.build + '/pages'))
      .on('end', () => { })
  }
}

/**********
|  GERAL  |
**********/

gulp.task('app.html', gulp.series([
  bundlePage('agenda'),
  bundlePage('ajuda'),
  bundlePage('bem-vindo'),
  bundlePage('clientes'),
  bundlePage('comandas'),
  bundlePage('comissoes'),
  bundlePage('despesas'),
  bundlePage('entrar'),
  bundlePage('empresa'),
  bundlePage('erro'),
  bundlePage('financeiro'),
  bundlePage('funcionarios'),
  bundlePage('gorjetas'),
  bundlePage('materiais'),
  bundlePage('notificacoes'),
  bundlePage('nova-senha'),
  bundlePage('perfil'),
  bundlePage('permissoes'),
  bundlePage('produtos'),
  bundlePage('receitas'),
  bundlePage('registrar'),
  bundlePage('servicos')
]))
