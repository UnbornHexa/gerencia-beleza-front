/*************
|  REQUIRES  |
*************/

const gulp = require('gulp')

const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const path = require('path')

/**********
|  CONST  |
**********/

const errorMessage = 'Error: <%= error.message %>'
const route = {
  source: path.join(__dirname, '../../source/docs'),
  build: path.join(__dirname, '../../build/docs')
}

/***********
|  BUNDLE  |
***********/

gulp.task('bundle-docs', function () {
  return gulp.src(route.source + '/**')
    .pipe(plumber({
      errorHandler: error => {
        notify.onError(errorMessage)(error)
      }
    }))
    .pipe(gulp.dest(route.build))
})

/**********
|  GERAL  |
**********/

gulp.task('app.docs', gulp.parallel(
  [
    'bundle-docs'
  ]
))
