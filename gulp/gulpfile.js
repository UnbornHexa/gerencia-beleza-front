/*****************
|  DEPENDENCIAS  |
*****************/

const gulp = require('gulp')

require('./tasks/clean')
require('./tasks/docs')
require('./tasks/html')
require('./tasks/css')
require('./tasks/js')
require('./tasks/webserver')

/************
|  DEFAULT  |
************/

gulp.task('default', gulp.series(
  function () {
    console.log()
    console.log('+-----------------+')
    console.log('|  COMANDOS GULP  |')
    console.log('+-----------------+')
    console.log('|   gulp build    |')
    console.log('|   gulp watch    |')
    console.log('+-----------------+')
    console.log()
  }
))

/**********
|  BUILD  |
**********/

gulp.task('build', gulp.series(
  [
    'app.clean',
    'app.html',
    'app.docs',
    'app.css',
    'app.js'
  ]
))

/**********
|  WATCH  |
**********/

gulp.task('monit', function () {
  /* HTML */
  gulp.watch('../source/html/**/**/**').on('change', gulp.series('app.html'))

  /* JS */
  gulp.watch('../source/js/**/**/**/**').on('change', gulp.series('app.js'))

  /* CSS */
  gulp.watch('../source/css/**/**').on('change', gulp.series('app.css'))

  /* DOCS */
  gulp.watch('../source/docs/**/**').on('change', gulp.series('app.docs'))
})

gulp.task('watch', gulp.parallel(
  [
    'app.webserver',
    'monit'
  ]
))
